<?php

/*

 * To change this template, choose Tools | Templates

 * and open the template in the editor.

 */
error_reporting(0);


if (!defined('FGC'))
    define('FGC', 1);



if (!defined('DS'))
    define('DS', DIRECTORY_SEPARATOR);

define('PATH_ROOT', dirname(__FILE__) . DS);

include_once PATH_ROOT . 'includes.php';

class App {

    static $instance;

    function __construct($configs) {



	self::$instance = new Application('site', $configs);
    }

    /**

     *

     * @global <type> $application

     * @return Application

     */
    function &getInstance() {

	return self::$instance;
    }

    function halt($message = null) {

	if (isset(self::$instance))
	    self::$instance->halt($message);
	else
	    exit($message);
    }

    function __call($method_name, $arguments) {

	if (!method_exists($this, $method_name)) {

	    return self::$instance->$method_name();
	}
    }

}

$app = new App($configs);

$application = $app->getInstance();

$application->initialize();

$session = $application->getSession();



$filename = $_REQUEST['filename'];

$name = substr($filename, 0, 9);



if ($name !== "airlines/") {

    /*  if (helperSite::findString($filename, 'practice_section/testsession')) {

      $filename = str_replace('testSection/testsession.html', 'testSection/testsession.php', $filename);

      } else */
    if (helperSite::findString($filename, 'testSection/testsection')) {

	$filename = str_replace('testSection/testsection.html', 'testSection/testsection.php', $filename);
    } else if (helperSite::findString($filename, 'IQtest/iqtest')) {
	$filename = str_replace('IQtest/iqtest.html', 'IQtest/iqtest.php', $filename);
    } else if (helperSite::findString($filename, 'testScore/testscore')) {
	$filename = str_replace('testScore/testscore.html', 'testScore/testscore.php', $filename);
    } else if (helperSite::findString($filename, 'IQtest/iqtest')) {
	$filename = str_replace('IQtest/iqtest.html', 'IQtest/iqtest.php', $filename);
    } else if (helperSite::findString($filename, 'math/math')) {
	$filename = str_replace('math/math.html', 'math/math.php', $filename);
    } else if (helperSite::findString($filename, 'mathchild/mathchild')) {
	$filename = str_replace('mathchild/mathchild.html', 'mathchild/mathchild.php', $filename);
    }



    if (file_exists($filename)) {

	ob_start();

	include_once($filename);

	$output = ob_get_clean();
    } else {

	include_once('404.php');
    }
}

echo $output;

//========

return;
?>

