<?php
header('P3P: CP="IDC DSP COR ADM DEVi TAIi PSA PSD IVAi IVDi CONi HIS OUR IND CNT"');
include_once ('header_new.tpl.php');
?>
<style type="text/css">  
    body {
        background-position: 0 -73px;
    }
    #header {
        height: 15px;
    }
    .pre_next{
        padding-top: 0;
    }
    .question_answer > div{
        padding-bottom: 20px;
    }
</style> 
<link type="text/css" rel="stylesheet" href="assets/css/style.css"/>
<!--[if IE 7]>
<style type="text/css">
        .radioquestion{
            margin: 0 2px 0 0;
        }
        .testsecexit a,
        #view_string_time{
            display: block;
        }
</style>
<![endif]-->
<?php
$app = App::getInstance();

$db = $app->getDBO();

include_once (PATH_ADMIN_APPS . 'models' . DS . 'config.model.php');
$model = new configModel();

$number_random_question = $model->getvalueConfig('number_random_question_iqtest')->value;
if (!$number_random_question) {
    $number_random_question = 10;
}
$sql = "SELECT * FROM iqtest" .
        " WHERE published = 1" .
        " ORDER BY RAND()";
$db->setQuery($sql, 0, $number_random_question);
$results = $db->loadObjectList();
//lay so nguoi da tham gia bai thi
$sql_total = "SELECT COUNT(id) AS total FROM iqtest_report";
$db->setQuery($sql_total);
$total_test = $db->loadResult();
//lay ti le phan tram
$sql_total_percent = "SELECT SUM(correct_percent) AS total_per FROM iqtest_report";
$db->setQuery($sql_total_percent);
$total_per = $db->loadResult();

function getPercentage($value1, $value2) {
    if (!$value1 || !$value2)
        return "0%";
    return (round($value1 / $value2, 2)) . "%";
}

if (empty($results)) {
    ?>
    <div class="noquestion">No Exists question</div>
    <?php
} else {
    ?>
    <div id="conent_javacript">
        <script type="text/javascript" src="assets/js/test_section.js"></script>
    </div>
    <?php
    $result = $results[0];
    $answers = json_decode($result->answers);
    $strabc = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

    $session->clear('inputtesttime');
    $session->clear('user_answer');
    $session->clear('iqtest_result_answer');
    $session->clear('number_answer_iqtest');
    $session->clear('number_iqtest_question', array());
    $iqtest_question = array();
    for ($i = 0; $i < count($results); $i++) {
        $iqtest_question[] = $results[$i]->id;
        ?>
        <script type="text/javascript">
            arrQuestionid.push(<?php echo $results[$i]->id; ?>);
        </script>
        <?php
    }
    $session->set('number_iqtest_question', $iqtest_question);

    $record = count($results);

    $timeViewAnswer = ($model->getvalueConfig('time_round_iqtest')->value);
    $timeViewAnswer = ($timeViewAnswer > 0)
            ? $timeViewAnswer
            : 0;
    $timeQuestion = $timeViewAnswer * $number_random_question;

    function convertTime($timeQuestion) {
        if (!$timeQuestion) {
            $stringTime = "00:00:00";
        } else {
            $hour = floor($timeQuestion / 3600);
            $minute = floor(($timeQuestion - $hour * 3600) / 60);
            $second = $timeQuestion - $hour * 3600 - $minute * 60;
            if ($hour < 10) {
                $hour = '0' . $hour;
            }
            if ($minute < 10) {
                $minute = '0' . $minute;
            }
            if ($second < 10) {
                $second = '0' . $second;
            }
            $stringTime = $hour . ':' . $minute . ':' . $second;
        }
        return $stringTime;
    }

    $stringTime = convertTime($timeQuestion);
    ?>
    <div id="wrapper_question" class="fgcmath_test" timeView="<?php echo $timeViewAnswer; ?>" countRecord ="<?php echo $record; ?>">
        <div class="testsectioinstart">
            <div class="testsecbody" style="padding: 10px;">
                <h1>
                    IQ Test
                </h1>
                <p>
                    This test shows you a 3-dimensional cube with unique symbols or markings on each face and then asks you to identify the matching cube.
                </p>
                <br/>
                <br/>
                <br/>
                <p>
                    <span class="colred ftbold">Example 1:</span>
                </p>
                <br/>
                <p style="font-style: italic;">
                    Find the identical cube?


                </p>
                <p style="font-style: italic;">
                    In this case C is the correct answer.
                </p>
                <div  style="border: 1px solid #aaa; text-align: center">
                    <img  src="assets/images/IQimage.png"/>
                </div>
                <p>
                    <span class="colred">
                        Attention! 
                    </span>
                    Attention! All sides of a cube look different. Please mark only solutions with 2 corresponding sides!
                </p>
                <br/>
                <br/>
                <br/>
                <div style="font-size: 18px; text-align: center;">
                    Average Score: <?php echo getPercentage($total_per, $total_test); ?>
                </div>
            </div>
            <br/>
            <br/>
            <div class="testsecclick">
                <a id="testsecstart">&nbsp;</a>
            </div>
        </div>
        <div class="content_game_math_question">
            <div id="fgcloading">
                <img src="assets/images/ajaxLoader.gif"/>
            </div>
            <div class="timeout_game">
                <span id ="view_string_time"></span>
            </div>
            <div class="testsecexit">
                <a href="/IQtest/iqtest.html">&nbsp;</a>
            </div>
            <div class ="content_submit">
                <form class="fgcform_answer_math" id="fgcform_iqtest" method="post" name="answerForm" action="test.php?apps=iqtest&questionid=<?php echo $result->id ?>" AUTOCOMPLETE="OFF">
                    <div class="title_game_math">
                        <span class="numone">Find the identical cube!</span><br>
                        <img  src="<?php echo URL_SITE . 'uploadfiles/' . $result->images ?>" alt="root"/>
                    </div>
                    <div class ="question_answer">
                        <ul class="listanswers">
                            <?php
                            for ($i = 0; $i < 8; $i++) {
                                ?>
                                <li class="npquestion<?php echo $i ?>">
                                    <input class="radioquestion" id="question_<?php echo $i ?>" type="radio" name="answers[]" value="<?php echo $i ?>"/>
                                </li>
                                <?php
                            }
                            ?>
                        </ul>
                    </div>
                    <br><br>
                    <!--end-->
                    <div class="pre_next_iqtest">
                        <span class="next_iqtest">
                            <input type="hidden" id="inputtesttime" value="0" name="inputtesttime"/>
                            <input type="hidden" name="questionid" value="<?php echo $result->id ?>" id="fgcquestionid"/>
                            <input type="submit" name="submit" value="Next"/>
                        </span>
                    </div>
                </form>
            </div>
        </div>
        <img src="assets/images/bg_fsh_title.png" style="display: none"/>
    </div>
    <script type="text/javascript">
        console.log(arrQuestionid)
    </script>
    <?php
}
include_once ('setup-page/footer.tpl.php');
?>