<?php
/*
  Created on : Jul 8, 2014, 8:37:44 AM
  Author: Tran Trong Thang
  Email: trantrongthang1207@gmai.com
 */
?>

<?php
if (!defined('FGC'))
    define('FGC', 1);

if (!defined('DS'))
    define('DS', DIRECTORY_SEPARATOR);
define('PATH_ROOT', dirname(__FILE__) . DS);
include_once PATH_ROOT . 'includes.php';

class App {

    static $instance;

    function __construct($configs) {

        self::$instance = new Application('site', $configs);
    }

    /**
     *
     * @global <type> $application
     * @return Application
     */
    function &getInstance() {
        return self::$instance;
    }

    function halt($message = null) {
        if (isset(self::$instance))
            self::$instance->halt($message);
        else
            exit($message);
    }

    function __call($method_name, $arguments) {
        if (!method_exists($this, $method_name)) {
            return self::$instance->$method_name();
        }
    }

}

$app = new App($configs);
$application = $app->getInstance();
$application->initialize();
$session = $application->getSession();
$request = $application->getRequest();
$offset = $request->getVar('offset', 0);
$db = $application->getDBO();
$sql = "SELECT * FROM math_test" .
        " WHERE type = 'practice' and published = 1" .
        " ORDER BY id asc";
$db->setQuery($sql, $offset, 1);
$result = $db->loadObject();
?>
<script type="text/javascript">
    jQuery(document).ready(function($j) {
        $j(".submit_answer").click(function(e) {
            e.preventDefault();
            $j(".question_answer").slideUp(500);
            $j(".discription_question").slideDown(500);

        })
    })
</script>
<div class="title_game_math">
    <span class="numone"><?php echo $result->content_math ?></span>
</div>
<div class ="question_answer">
    <div>
        <div class="title_question">The Correct Answer is.<span class="red"> *</span></div>
        <form UTOCOMPLETE="OFF">
            <input type="text" name="altitude" value="" AUTOCOMPLETE="OFF"/>
        </form>
    </div>
    <input type="button" name="buttom" value="Submit Answer" class="submit_answer"/>
</div>
<div class="discription_question">
    <?php echo $result->discription_practice ?>
</div>
