<?php
header('P3P: CP="IDC DSP COR ADM DEVi TAIi PSA PSD IVAi IVDi CONi HIS OUR IND CNT"');
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
define('PATH_ROOT', './../');
include_once PATH_ROOT . 'includes.php';

function getPercentage($value1, $value2) {
    if (!$value1 || !$value2)
        return "0%";
    return (round($value1 / $value2, 2) * 100) . "%";
}

class App {

    static $instance;

    function __construct($configs) {

        self::$instance = new Application('site', $configs);
    }

    /**
     *
     * @global <type> $application
     * @return Application
     */
    function &getInstance() {
        return self::$instance;
    }

    function halt($message = null) {
        if (isset(self::$instance))
            self::$instance->halt($message);
        else
            exit($message);
    }

    function __call($method_name, $arguments) {
        if (!method_exists($this, $method_name)) {
            return self::$instance->$method_name();
        }
    }

}

$app = new App($configs);
$application = $app->getInstance();
$application->initialize();
$request = $application->getRequest();
$session = $application->getSession();
$db = $application->getDBO();
$strabc = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
//lay toan bo cau hoi da duoc luu vao session
$allQuestion = $session->get('number_testscore_question', array());

$task = $request->getVar('task', '');
$questionid = $request->getVar('questionid');

//lay thoi gian lam bai test
$inputtesttime = $session->get('inputtesttime');
$inputtesttime+=$request->getVar('inputtesttime');
$inputtesttime = $session->set('inputtesttime', $inputtesttime);

//end
$key = @array_search($questionid, $allQuestion);
$questionidnext = $allQuestion[($key + 1)];
$sql = "SELECT * FROM testscore_questions" .
        " WHERE id = '$questionidnext'";
$db->setQuery($sql);
$result = $db->loadObject();
//get ans
$sql = "SELECT * FROM  testscore_answers" .
        " WHERE question_id = " . $result->id;
$db->setQuery($sql);
$resultsAns = $db->loadObjectList();
//luu cau tra loi cua nguoi dung

$user_answer = array();
$user_answer = $session->get('user_answer');
//luu cau tra loi cau nguoi dung
//var_dump($_REQUEST);
//exit();
$sql = "SELECT * FROM  testscore_answers" .
        " WHERE question_id = " . $questionid;
$db->setQuery($sql);
$resultsAnsUser = $db->loadObjectList();
foreach ($resultsAnsUser as $value) {
    $poss = $_REQUEST['answers' . $value->id][0];
    if ($poss != '') {
        $user_answer[$questionid][$value->id][$poss] = $poss;
    }
}
$session->set('user_answer', $user_answer);

//end
$result->id = $questionidnext;
//neu con co cau hoi
?>
<?php if ($questionidnext != null && $questionidnext > 0) { ?>
    <div class="content_question_next">
        <div id="np-memory">
            <span class="numone">Memorize the symbols on the map</span><br><br>
            <img class="np_position" src="uploadfiles/<?php echo $result->image1; ?>"/>
            <img class="lazy-memory" src="assets/images/loading.gif" data-src="uploadfiles/<?php echo $result->image1; ?>"/>
        </div>
        <form class="fgcform_answer_math" id="fgcform_testscore" method="post" name="answerForm" action="test.php?apps=testscore&questionid=<?php echo $result->id ?>" AUTOCOMPLETE="OFF">
            <div class="title_game_math">
                <span class="numone">Answer the question below!</span><br>
                <img src="uploadfiles/<?php echo $result->image2; ?>"/>
            </div>
            <div class="np_question_answer">
                <?php
                $jj = 0;
                foreach ($resultsAns as $ans) {
                    $jj++;
                    $arr_postions = explode(";", $ans->postions);
                    ?>
                    <div id="np-position<?php echo $ans->id; ?>" class="np-position">
                        <p><?php echo $ans->namepostion; ?> is located:</p>
                        <ul class="listanswers">
                            <?php
                            for ($i = 0; $i < count($arr_postions); $i++) {
                                ?>
                                <li>
                                    <input class="radioquestion" id="question_<?php echo $ans->id . '_' . $i ?>" type="radio" name="answers<?php echo $ans->id ?>[]" value="<?php echo $arr_postions[$i]; ?>"/>
                                    <label for="question_<?php echo $ans->id . '_' . $i ?>">
                                        Position <?php echo $arr_postions[$i]; ?>
                                    </label>
                                </li>
                                <?php
                            }
                            ?>
                        </ul>
                    </div>
                    <?php
                    if ($jj % 4 == 0) {
                        ?>
                        <div class="npclear"></div>
                        <?php
                    }
                    ?>
                <?php } ?>
            </div>
            <div class="pre_next_testscore">
                <span class="next_testscore">
                    <input type="hidden" id="inputtesttime" value="0" name="inputtesttime"/>
                    <input type="hidden" name="questionid" value="<?php echo $result->id ?>" id="fgcquestionid"/>
                    <input type="submit" name="submit" value="Next"/>
                </span>
            </div>
        </form>
    </div>
<?php } ?>
<?php
if ($questionidnext == null || $questionidnext == '') {
//lay cau tra loi va tinh cau dung
    $user_answer = $session->get('user_answer');

    $sql = "SELECT * FROM testscore_questions" .
            " WHERE published = 1 AND id IN(" . implode(",", $allQuestion) . ")";
    $db->setQuery($sql);
    $listQuestion = $db->loadObjectList();
    $correct = 0;
    if (count($listQuestion) > 0) {
        foreach ($listQuestion as $value) {
            //get ans
            $sql = "SELECT * FROM  testscore_answers" .
                    " WHERE question_id = " . $value->id;
            $db->setQuery($sql);
            $resultsAns = $db->loadObjectList();
            foreach ($resultsAns as $ans) {
                $arr_postions = explode(";", $ans->postions);
                for ($i = 0; $i < count($arr_postions); $i++) {
                    if ($user_answer[$value->id][$ans->id][$arr_postions[$i]] == $ans->right_postion) {
                        $correct++;
                    }
                }
            }
            //
        }
    }
//Lay tong so cau hoi
    $sql = "SELECT COUNT(id) AS total FROM testscore_answers" .
            " WHERE  question_id IN(" . implode(",", $allQuestion) . ")";
    $db->setQuery($sql);
    $total_question = $db->loadResult();
//tinh thoi gian

    $timeQuestion = $session->get('inputtesttime');

    function convertTime($timeQuestion) {
        if (!$timeQuestion) {
            $stringTime = "00:00:00";
        } else {
            $hour = floor($timeQuestion / 3600);
            $minute = floor(($timeQuestion - $hour * 3600) / 60);
            $second = $timeQuestion - $hour * 3600 - $minute * 60;
            if ($hour < 10) {
                $hour = '0' . $hour;
            }
            if ($minute < 10) {
                $minute = '0' . $minute;
            }
            if ($second < 10) {
                $second = '0' . $second;
            }
            $stringTime = $hour . ':' . $minute . ':' . $second;
        }
        return $stringTime;
    }

    $stringTime = convertTime($timeQuestion);

//Tinh phan tram cau tra loi dung
    $correct_percent = getPercentage($correct, $total_question);

//luu vao db
    $sql_insert = "INSERT INTO testscore_report(correct_percent) VALUES('" . substr($correct_percent, 0, strlen($correct_percent) - 1) . "')";
    $db->setQuery($sql_insert);
    $db->query();
    ?>
    <style type="text/css">
        .timeout_game,
        .testsecexit{
            display: none;
        }
    </style>
    <div class="result">
        <div class="result_inner result_math_test">
            <div class="result_title">
            </div>
            <div class="result_complate">
                <label>You Scored:</label> <?php echo $correct . "&nbsp;&nbsp;"; ?>out of<?php echo "&nbsp;&nbsp;" . $total_question . "&nbsp;&nbsp;"; ?>Positions
            </div>
            <div class="result_percentage">
                <label>You Scored:<span> <?php echo $correct_percent; ?></span></label>
            </div>
            <div class="resutl_time">
                <label>Time Used:<span> <?php echo $stringTime; ?></span></label>
            </div>
            <div class="result_show">
                <a href="#" class="aresult_show">Click Here to view answers!</a>
            </div>
            <div class="retake_testsec">
                <a href="/testScore/testscore.html">&nbsp;</a>
            </div>
        </div>
    </div>
    <div class="fgcresult_show">
        <?php
        for ($k = 0; $k < count($allQuestion); $k++):
            $sql = "SELECT * FROM testscore_questions" .
                    " WHERE published = 1 AND id = '{$allQuestion[$k]}'";
            $db->setQuery($sql, 0, 1);
            if ($resultQuestion = $db->loadObject()) {
                //get ans
                $sql = "SELECT * FROM  testscore_answers" .
                        " WHERE question_id = " . $resultQuestion->id;
                $db->setQuery($sql);
                $resultsAns = $db->loadObjectList();
                //
                ?>
                <div class="inner">
                    <div class="fgcresult_left">
                        <span>Q.<?php echo $k + 1; ?></span>
                    </div>
                    <div class="fgcresult_right">
                        <div class="fgccontent_question">
                            <img src="uploadfiles/<?php echo $resultQuestion->image2; ?>"/>
                        </div>
                        <div class="fgccontent_answers np_question_answer">
                            <?php
                            $jj = 0;
                            foreach ($resultsAns as $ans) {
                                $jj++;
                                $arr_postions = explode(";", $ans->postions);
                                ?>
                                <div id="np-position<?php echo $ans->id; ?>" class="np-position">
                                    <p><?php echo $ans->namepostion; ?> is located:</p>
                                    <ul class="listanswers">
                                        <?php
                                        for ($i = 0; $i < count($arr_postions); $i++) {
                                            ?>
                                            <li>
                                                <input  class="radioquestion" id="question_<?php echo $ans->id . '_' . $i ?>" type="radio" <?php
                                                if ($user_answer[$resultQuestion->id][$ans->id][$arr_postions[$i]] != '') {
                                                    echo 'checked="true"';
                                                }
                                                ?> name="answers<?php echo $ans->id; ?>[]" value="<?php echo $i ?>"/>
                                                <label for="question_<?php echo $ans->id . '_' . $i ?>">
                                                    Position <?php echo $arr_postions[$i]; ?>
                                                </label>
                                                <?php
                                                if ($user_answer[$resultQuestion->id][$ans->id][$arr_postions[$i]] == $arr_postions[$i] && $user_answer[$resultQuestion->id][$ans->id][$arr_postions[$i]] != null) {
                                                    if ($user_answer[$resultQuestion->id][$ans->id][$arr_postions[$i]] == $ans->right_postion) {
                                                        ?>

                                                        <img src = "assets/images/icon-correct.png" alt = "corect"/>
                                                    <?php } else { ?>
                                                        <img alt="incorrect" src="assets/images/icon-incorrect.png"/> 
                                                    <?php } ?>
                                                <?php } ?>
                                                <?php if ($user_answer[$resultQuestion->id][$ans->id][$arr_postions[$i]] != $ans->right_postion && $arr_postions[$i] == $ans->right_postion) { ?>
                                                    <img src = "assets/images/icon-correct.png" alt = "corect"/>  
                                                <?php }
                                                ?>
                                            </li>
                                            <?php
                                        }
                                        ?>
                                    </ul>
                                </div>
                                <?php
                                if ($jj % 4 == 0) {
                                    ?>
                                    <div class="npclear"></div>
                                    <?php
                                }
                                ?>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            <?php } endfor; ?>
    </div>
    <?php
}
?>

