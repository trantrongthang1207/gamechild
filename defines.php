<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
if (!defined('DS'))
    define('DS', DIRECTORY_SEPARATOR);

$SITE_URL = "http://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];

$SITE_URL = substr($SITE_URL, 0, strrpos($SITE_URL, '/') + 1);

define('URL_SITE', $SITE_URL);
define('URL_ROOT_SITE', "http://" . $_SERVER['HTTP_HOST'] . '/');

if (!defined('HOST_NAME'))
    define('HOST_NAME', $_SERVER['HTTP_HOST']);

define('PATH_LIBRARIES', PATH_ROOT . 'libraries' . DS);

define('PATH_LIBRARY_CORE', PATH_LIBRARIES . 'core' . DS);

define('PATH_INCLUDES', PATH_ROOT . 'includes' . DS);

define('PATH_SITE', PATH_ROOT);

define('PATH_APPS', PATH_SITE . 'apps' . DS);

define('PATH_LIBARIES_TABLE', PATH_LIBRARIES . 'site' . DS . 'tables' . DS);

define('PATH_ADMIN_APPS', PATH_SITE . 'admin' . DS . 'apps' . DS);
?>

