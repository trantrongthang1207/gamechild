<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7"/>
<title> Airline Interview Preparation</title>
<link rel="stylesheet"  type="text/css" href="main_style.css">
<meta name="description" content="Compass and aptitude test for pilots attending Emirates, Fly dubai, Qatar Airways, Etihad and Cathay Pacific interview " />
<meta name="keywords" content="Matha test, aptitude test, numerical reasoning test, memory games, online datin, games for math, russian dating online, psychometric tests" />


<meta name="google-site-verification" content="gsOKUvylxGkmHbPZgBCrRblY4dODM4SnlDp2U92HkmY" />
<script type='text/javascript'>var STATIC_BASE = 'http://www.dragndropbuilder.com/';</script><script type='text/javascript' src='http://ajax.googleapis.com/ajax/libs/prototype/1.6.1/prototype.js' ></script>
<script type='text/javascript' src='http://www.dragndropbuilder.com/weebly/images/common/effects-1.8.2.js' ></script>
<script type='text/javascript' src='http://www.dragndropbuilder.com/weebly/images/common/weebly.js' ></script>
<script type='text/javascript' src='http://www.dragndropbuilder.com/weebly/images/common/lightbox202.js?8' ></script>
<script type='text/javascript' src='http://www.dragndropbuilder.com/weebly/libraries/flyout_menus.js?11'></script>
<script type='text/javascript'>function initFlyouts(){initPublishedFlyoutMenus([{"id":"189489064820642642","title":"HOME","url":"index.html"},{"id":"736976607200237226","title":"VIEW JOBS","url":"view-jobs.html"},{"id":"142396558144269964","title":"POST JOBS","url":"post-jobs.html"},{"id":"531590364546131745","title":"CAREER SERVICE","url":"career-service.html"},{"id":"549293792484800302","title":"FORUM","url":"forum.html"},{"id":"483580970137485058","title":"AIRLINE INTERVIEWS","url":"airline-interviews.html"},{"id":"249101591614218051","title":"CONTACT","url":"contact.html"},{"id":"872439001532575136","title":"ADVERTISE","url":"advertise.html"}],'482685949480805340','<li class=\'weebly-nav-more\'><a href=\"#\">more...</a></li>','')}if (Prototype.Browser.IE) window.onload=initFlyouts; else document.observe('dom:loaded', initFlyouts);</script>

<link rel='stylesheet' href='http://www.dragndropbuilder.com/weebly/images/common/common.css?6' type='text/css' />
<link rel='stylesheet' type='text/css' href='/files/main_style.css?311542948879864901' title='weebly-theme-css' />
<link rel='stylesheet' type='text/css' href='/files/fgc_memory_game.css' />
<script type='text/javascript' src='/files/jquery-1.4.4.min.js'></script>
<style type='text/css'>
#weebly_page_content_container div.paragraph, #weebly_page_content_container p, #weebly_page_content_container .product-description, .blog-sidebar div.paragraph, .blog-sidebar p{}
#weebly_page_content_container h2, #weebly_page_content_container .product-title, .blog-sidebar h2{}
#weebly_site_title{}
</style>

<script language="JavaScript1.2">
</script>
<script language=JavaScript>
<!--

//Disable right mouse click Script
//By Maximus (maximus@nsimail.com) w/ mods by DynamicDrive
//For full source code, visit http://www.dynamicdrive.com

var message="Function Disabled!";

///////////////////////////////////
function clickIE4(){
if (event.button==2){
alert(message);
return false;
}
}

function clickNS4(e){
if (document.layers||document.getElementById&&!document.all){
if (e.which==2||e.which==3){
alert(message);
return false;
}
}
}

if (document.layers){
document.captureEvents(Event.MOUSEDOWN);
document.onmousedown=clickNS4;
}
else if (document.all&&!document.getElementById){
document.onmousedown=clickIE4;
}

document.oncontextmenu=new Function("alert(message);return false")

// -->
</script>
<style type='text/css'>
.weebly_header {
	background-image: url(/uploads/2/7/7/3/2773200/header_images/1307720201.jpg) !important;
	background-position: 0 0 !important;
}
</style>
</head>
<body class="weeblypage-compass-and-pilot-aptitude-tests" >
  <div id="wrapper">
    <div id="main">
      <div id="header">
        <div id="sitename"><span style="visibility:hidden;">Latest Pilot Jobs provides the latest aviation vacancies around the world.</span></div>
      </div>
      <div id="content-wrapper">
        <div id="content">
          <div id="header-image">
             <div class="weebly_header"></div>
          </div>
          <div id="navigation_wrapper">
             <div id="navigation">

<ul>

  <li id='pg189489064820642642'><a href="/index.html">HOME</a></li><li id='pg736976607200237226'><a href="/view-jobs.html">VIEW JOBS</a></li><li id='pg142396558144269964'><a href="/post-jobs.html">POST JOBS</a><div class='weebly-menu-wrap' style='display:none'><ul class='weebly-menu'><li id='weebly-nav-460098646949959169'><a href='/post-jobs-free.html'><span class='weebly-menu-title'>POST JOBS (FREE)</span></a></li></ul></div></li><li id='pg531590364546131745'><a href="/career-service.html">CAREER SERVICE</a><div class='weebly-menu-wrap' style='display:none'><ul class='weebly-menu'><li id='weebly-nav-972892814533230970'><a href='/curriculum-vitae-example.html'><span class='weebly-menu-title'>CURRICULUM VITAE EXAMPLE</span></a></li><li id='weebly-nav-917014896331562272'><a href='/cover-letter-example.html'><span class='weebly-menu-title'>COVER LETTER EXAMPLE</span></a></li><li id='weebly-nav-482685949480805340' class='weebly-nav-current'><a href='/compass-and-pilot-aptitude-tests.html'><span class='weebly-menu-title'>COMPASS AND PILOT APTITUDE TESTS</span></a></li></ul></div></li><li id='pg549293792484800302'><a href="/forum.html">FORUM</a></li><li id='pg483580970137485058'><a href="/airline-interviews.html">AIRLINE INTERVIEWS</a><div class='weebly-menu-wrap' style='display:none'><ul class='weebly-menu'><li id='weebly-nav-397209795974668892'><a href='/ryanair-pilot-interview.html'><span class='weebly-menu-title'>RYANAIR PILOT INTERVIEW</span><span class='weebly-menu-more'>&gt;</span></a><div class='weebly-menu-wrap' style='display:none'><ul class='weebly-menu'><li id='weebly-nav-580946236111109084'><a href='/ryanair-cabin-crew-interview.html'><span class='weebly-menu-title'>RYANAIR CABIN CREW INTERVIEW</span></a></li></ul></div></li><li id='weebly-nav-413529251147065739'><a href='/air-baltic-pilot-interview.html'><span class='weebly-menu-title'>AIR BALTIC PILOT INTERVIEW</span></a></li><li id='weebly-nav-147267322884794737'><a href='/jet2-pilot-interview.html'><span class='weebly-menu-title'>JET2 PILOT INTERVIEW</span></a></li><li id='weebly-nav-226603599304160351'><a href='/qatar-airways-pilot-interview.html'><span class='weebly-menu-title'>QATAR AIRWAYS PILOT INTERVIEW</span><span class='weebly-menu-more'>&gt;</span></a><div class='weebly-menu-wrap' style='display:none'><ul class='weebly-menu'><li id='weebly-nav-849907033963972681'><a href='/qatar-airways-cabin-crew-interview.html'><span class='weebly-menu-title'>QATAR AIRWAYS CABIN CREW INTERVIEW</span></a></li></ul></div></li><li id='weebly-nav-982701575946384761'><a href='/emirates-pilot-interview.html'><span class='weebly-menu-title'>EMIRATES PILOT INTERVIEW</span><span class='weebly-menu-more'>&gt;</span></a><div class='weebly-menu-wrap' style='display:none'><ul class='weebly-menu'><li id='weebly-nav-874175490891591614'><a href='/emirates-cabin-crew-interview.html'><span class='weebly-menu-title'>EMIRATES CABIN CREW INTERVIEW</span></a></li></ul></div></li><li id='weebly-nav-683794288985817084'><a href='/etihad-airways-pilot-interview.html'><span class='weebly-menu-title'>ETIHAD AIRWAYS PILOT INTERVIEW</span><span class='weebly-menu-more'>&gt;</span></a><div class='weebly-menu-wrap' style='display:none'><ul class='weebly-menu'><li id='weebly-nav-536623863604119145'><a href='/etihad-airways-cabin-crew-interview.html'><span class='weebly-menu-title'>ETIHAD AIRWAYS CABIN CREW INTERVIEW</span></a></li></ul></div></li><li id='weebly-nav-391183931968220129'><a href='/saudi-arabian-airlines-pilot-interview.html'><span class='weebly-menu-title'>SAUDI ARABIAN AIRLINES PILOT INTERVIEW</span></a></li><li id='weebly-nav-417986795294513568'><a href='/cathay-pacific-pilot-interview.html'><span class='weebly-menu-title'>Cathay Pacific Pilot Interview</span></a></li><li id='weebly-nav-371995124201102482'><a href='/susi-air-pilot-interview.html'><span class='weebly-menu-title'>SUSI AIR PILOT INTERVIEW</span></a></li><li id='weebly-nav-363505544924527393'><a href='/cabin-crew-interview.html'><span class='weebly-menu-title'>CABIN CREW INTERVIEW</span></a></li><li id='weebly-nav-606914475235676024'><a href='/submit-interview.html'><span class='weebly-menu-title'>SUBMIT INTERVIEW</span></a></li></ul></div></li><li id='pg249101591614218051'><a href="/contact.html">CONTACT</a></li><li id='pg872439001532575136'><a href="/advertise.html">ADVERTISE</a></li>
</ul>
            </div>
          </div>
          <div id="content-main">