/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
function Pagination(offset, numOfPage, currentPage)
{
    var pageStart = 0; //parseFloat(currentPage) - parseFloat(offset);
    var pageEnd =  0;//parseFloat(currentPage) + parseFloat(offset);
    var numPage = new String();
    if(numOfPage < 1) return false;
    numPage += '<ul id="pagi">';
    if(currentPage > 1) numPage += '<li class="previous" ><a href="javascript:;" name="page'+(parseFloat(currentPage) - 1)+'">&laquo;</a></li>';
    else numPage += '<li class="previous-off">&laquo;</li>';
    if(currentPage<=2)
    {
        if(currentPage==2){
            pageStart =1 ;
            if((parseFloat(offset) + 1)<=numOfPage){
                pageEnd = parseFloat(offset) + 1;
            }
            else{
                pageEnd = numOfPage;
            }
        }
        else if(currentPage < 2){
            pageStart =1 ;
            if(offset<=numOfPage){
                pageEnd = offset;
            }
            else{
                pageEnd = numOfPage;
            }
        }
        for(i=pageStart;i<=pageEnd;i++){
            if(i == currentPage) numPage += '<li class="active">'+i+'</li>';
            else numPage += '<li><a href="javascript:;" name="page'+i+'" >'+i+'</a></li>';
        }
    }
    if(currentPage > 2 && numOfPage!= offset) numPage += '<li><a href="javascript:;" name="page1">1</a></li><li class="spacing-dot"> ... </li>';
    if((currentPage > 2) && (numOfPage > (parseFloat(currentPage) + parseFloat(offset) + 1)))
    {
        pageStart = currentPage;
        pageEnd =   parseFloat(currentPage) + parseFloat(offset);
        for(i = pageStart; i <= pageEnd; i++){
            if(i == currentPage) numPage += '<li class="active">'+i+'</li>';
            else numPage += '<li><a href="javascript:;" name="page'+i+'" >'+i+'</a></li>';

        }
    }

    if((currentPage > 2) && (numOfPage == (parseFloat(currentPage) + parseFloat(offset) + 1))){

        pageStart = currentPage;
        pageEnd =   numOfPage;
        for(i = pageStart; i <= pageEnd; i++){

            if(i == currentPage) numPage += '<li class="active">'+i+'</li>';
            else numPage += '<li><a href="javascript:;" name="page'+i+'" >'+i+'</a></li>';

        }
    }
    if((currentPage > 2) && (numOfPage <= (parseFloat(currentPage) + parseFloat(offset))))
    {
        if(numOfPage!=offset){
            for(j=offset;j>=0;j--){

                var number = parseFloat(currentPage) + parseFloat(j);
                if(number == numOfPage){
                    var index = parseFloat(offset) - parseFloat(j);
                    pageStart = parseFloat(currentPage) - parseFloat(index);
                    pageEnd =   numOfPage;
                    for(i = pageStart; i <= pageEnd; i++){
                        if(i == currentPage) numPage += '<li class="active">'+i+'</li>';
                        else numPage += '<li><a href="javascript:;" name="page'+i+'" >'+i+'</a></li>';

                    }
                }
            }
        }
        else if(numOfPage==offset){
            pageStart = 1;
            pageEnd =   numOfPage;
            for(i = pageStart; i <= pageEnd; i++){
                if(i == currentPage) numPage += '<li class="active">'+i+'</li>';
                else numPage += '<li><a href="javascript:;" name="page'+i+'" >'+i+'</a></li>';

            }
        }
    }
   
    if(numOfPage > pageEnd) numPage += '<li class="spacing-dot"> ... </li><li class = "currentpage"><a href="javascript:;" name="page'+numOfPage+'">'+numOfPage+'</a></li>';
    if(currentPage < numOfPage) numPage += '<li class="next"><a href="javascript:;" name="page'+(parseFloat(currentPage) + 1)+'" >&raquo;</a></li>';
    else numPage += '<li class="next-off">&raquo;</li>';
    numPage += '</ul>';
    return numPage;
}




