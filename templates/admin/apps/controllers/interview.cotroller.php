<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
include_once (PATH_APPS . 'models' . DS . 'interview.model.php');

class interviewController {

    function __construct() {

    }

    function delete() {
        $app = App::getInstance();
        $request = $app->getRequest();
        $model = new interviewModel();
        $cid = $request->getVar('cid', array(0));
        $record = 0;
        for ($i = 0; $i < count($cid); $i++) {
            if ($model->delete($cid[$i])) {
                $record++;
            }
            $msg = "Deleted ($record) successfull!";
        }
        $url = "index.php?apps=interview&msg={$msg}";
        $app->redirect($url);
    }

    function save() {
        $app = App::getInstance();
        $request = $app->getRequest();
        $model = new interviewModel();
        $records = $_POST;
        if($records['choice']=='free'){
            $records['price'] = 0;
        }
        $db = $app->getDBO();
        $interview = $model->getInterviewByFileName($records['filename']);
        // if ($records['name'] != '' && $records['airline_id'] != 0 && $records['filename'] != 0 && $records['type'] != 0) {
        if ($records['id'] != '' && $records['id'] > 0) {
            $table = new InterviewsTable($db);
            $interviewupdate = $table->load_object($records['id']);
            if (!empty($interview) && !empty($interviewupdate) && $interview->filename != $interviewupdate->filename) {
                $msg = "File is exists!!! Please try choice another file.";
            } else {
                if ($model->insertUpdate($records)) {
                    $msg = "Update interview successfull!";
                } else {
                    $msg = "No update interview";
                }
            }
        } else {

            if (empty($interview)) {
                if ($model->insertUpdate($records)) {
                    $msg = "Insert interview successfull!";
                } else {
                    $msg = "No insert interview";
                }
            } else {
                $msg = "File is exists!!! Please try choice another file.";
            }
        }
//        } else {
//            $msg = "Data not null";
//        }

        $url = "index.php?apps=interview&msg={$msg}";
        $app->redirect($url);
    }

}

?>
