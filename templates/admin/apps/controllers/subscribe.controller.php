<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
include_once (PATH_APPS . 'models' . DS . 'subscribe.model.php');

class subscribeController {

    function __construct() {

    }

    function delete() {
        $app = App::getInstance();
        $model = new subscribeModel();
        $record = $model->delete();
        $msg = "Deleted ($record) successfull!";
        $url = "index.php?apps=subscribe&msg={$msg}";
        $app->redirect($url);
    }

    function save() {
        $app = App::getInstance();
        $request = $app->getRequest();
        $model = new subscribeModel();
        $records = $_POST;
        $records['start_time'] = helper::format_date($records['start_time'], 'Y-m-d');
        $records['end_time'] = helper::format_date($records['end_time'], 'Y-m-d');
  
        if ($records['id'] != '' && $records['id'] > 0) {
            if ($model->insertUpdate($records)) {
                $msg = "Update subscribe successfull!";
            } else {
                $msg = "No update subscribe";
            }
        } else {
            if ($model->insertUpdate($records)) {
                $msg = "Insert subscribe successfull!";
            } else {
                $msg = "No insert subscribe";
            }
        }

        $url = "index.php?apps=subscribe&msg={$msg}";
       $app->redirect($url);
    }

}

?>
