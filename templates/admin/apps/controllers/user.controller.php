<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
include_once (PATH_APPS . 'models' . DS . 'user.model.php');

class userController {

    function __construct() {
        
    }

    function delete() {
        $app = App::getInstance();
        $request = $app->getRequest();
        $model = new userModel();
        $cid = $request->getVar('cid', array(0));
        $count = 0;
        for ($i = 0; $i < count($cid); $i++) {
            if ($model->delete($cid[$i])) {
                $count++;
            }
            $msg = "Deleted ($count) successfull!";
        }
        $url = "index.php?apps=user&msg={$msg}";
        $app->redirect($url);
    }

    function save() {
        $app = App::getInstance();
        $request = $app->getRequest();
        $model = new userModel();
        $records = $_POST;
        if ($records['username'] != '' && $records['email'] != '') {
            $user = $model->getuserbyusername($records['username']);
            $records['username'] = trim($records['username']);
            $records['name'] = trim($records['name']);
            $records['email'] = trim($records['email']);
            if (intval($records['id']) == 0 || $records['id'] == '') {
                if (empty($user)) {
                    if ($records['password'] == $records['verify_password'] && $records['password'] != '') {
                        $records['password'] = md5(trim($records['password']));
                        $records['register_date'] = date('Y-m-d h:i:s');
                        if ($model->insertUpdate($records)) {
                            $msg = "Insert user successfull";
                        } else {
                            $msg = "No insert user";
                        }
                    } else {
                        $msg = "Pasword null or password and verify password do not match";
                    }
                } else {
                    $msg = "Username exists!";
                }
            } else {
                $app = App::getInstance();
                $db = $app->getDBO();
                $table = new UsersTable($db);
                $userupdate = $table->load_object($records['id']);
                if (!empty($user) && !empty($userupdate) && $user->username != $userupdate->username) {
                    $msg = "Username exists!";
                } else {
                    unset($records['register_date']);
                    if ($records['password'] == '') {

                        unset($records['password']);
                        if ($model->insertUpdate($records)) {
                            $msg = "Update user successfull";
                        } else {
                            $msg = "No update user";
                        }
                    } else {

                        if ($records['password'] == $records['verify_password']) {
                            $records['password'] = trim(md5($records['password']));
                            if ($model->insertUpdate($records)) {
                                $msg = "Update user successfull";
                            } else {
                                $msg = "No update user";
                            }
                        } else {
                            $msg = "password and verify password do not match";
                        }
                    }
                }
            }
        } else {
            $msg = "username and email not null";
        }
        $url = "index.php?apps=user&msg={$msg}";
        $app->redirect($url);
    }

    function block() {
        $app = App::getInstance();
        $request = $app->getRequest();
        $cid = $request->getVar('cid', 0);
        $block = $request->getVar('block', '0');
        $model = new userModel();
        if ($model->block($cid, $block)) {
            if ($block == 0) {
                echo "1";
            } else {
                echo "0";
            }
        } else {
            if ($block == 0) {
                echo "0";
            } else {
                echo "1";
            }
        }
    }

}

?>
