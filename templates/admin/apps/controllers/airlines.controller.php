<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
include_once (PATH_APPS . 'models' . DS . 'airlines.model.php');

class airlinesController {

    function __construct() {
        
    }

    function delete() {
        $app = App::getInstance();
        $request = $app->getRequest();
        $model = new airlinesModel();
        $cid = $request->getVar('cid', array(0));
        $record = 0;
        for ($i = 0; $i < count($cid); $i++) {
            if ($model->delete($cid[$i])) {
                $record++;
            }
            $msg = "Deleted ($record) successfull!";
        }
        $url = "index.php?apps=airline&msg={$msg}";
        $app->redirect($url);
    }

    function save() {
        $app = App::getInstance();
        $records = $_POST;
        $records['name']= trim($records['name']);
        $string = $records['name'];
        $string = strtolower(str_replace(' ', '-', $string));
        $records['alias']= $string;
        $model = new airlinesModel();
        if ($records['id'] > 0) {
            if ($model->insertUpdate($records)) {
                $msg = "Updated successfull!";
            } else {
                $msg = "Cannot update! Please try again.";
            }
        } else {
            if ($model->insertUpdate($records)) {
                $msg = "Insert airline successfull!";
            } else {
                $msg = "Cannot insert airline! Please try again.";
            }
        }
        $request = $app->getRequest();
        $request->setVar('msg', $msg);
        $url = "index.php?apps=airline&msg={$msg}";
        $app->redirect($url);
    }

    function published() {
        $app = App::getInstance();
        $request = $app->getRequest();
        $cid = $request->getVar('cid', 0);
        $publish = $request->getVar('valuepublish', 0);
        if ($publish == 0) {
            $published = 1;
        } else {
            $published = 0;
        }
        $model = new airlinesModel();
        if ($model->published($cid, $published)) {
            if ($published == 1) {
                echo "1";
            } else {
                echo "0";
            }
        } else {
            if ($published == 0) {
                echo "0";
            } else {
                echo "1";
            }
        }
    }

}

?>
