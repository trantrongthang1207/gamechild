<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
include_once (PATH_APPS . 'models' . DS . 'config.model.php');

class configController {

    function __construct() {

    }

    function save() {
        $records = $_POST;
        $records['paypal_testmode'] = (isset($records['paypal_testmode']) && $records['paypal_testmode'] == 'on') ? 1 : 0;
        $records['end_time'] = $records['end_time'] .":". $records['time_unit'];
        if (!empty($records)) {
            $count = 0;
            foreach ($records as $key => $record) {
                $name = trim($key);
                $value = trim($record);
                if ($name != '' && $value != '') {
                    $model = new configModel();
                    if ($name != 'time_unit') {
                        if ($model->setConfig($name, $value)) {
                            $msg = "Update successfull";
                        }
                    }
                } else {
                    $msg = "No update";
                }
            }
        } else {
            $msg = "No update";
        }
        $app = App::getInstance();
        $request = $app->getRequest();
        $url = "index.php?apps=config&msg={$msg}";
        $app->redirect($url);
    }

}

?>
