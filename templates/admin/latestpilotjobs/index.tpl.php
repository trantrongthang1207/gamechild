<?php
global $templateloader;
global $array_type_questions;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <title><?php echo TITLE_SITE_ADMIN; ?></title>
        <link type="text/css" rel="stylesheet" href="<?php echo URL_TEMPLATE ?>css/style.css" />
        <link type="text/css" href="<?php echo URL_TEMPLATE ?>css/ui-lightness/jquery-ui-1.8.5.custom.css" rel="stylesheet" />
	<link type="text/css" href="<?php echo URL_TEMPLATE ?>css/fgc.admin.css" rel="stylesheet"/>
	<?php if ($_REQUEST['apps'] == 'math' || $_REQUEST['apps'] == 'mathchild') { ?>
    	<script type="text/javascript" src="<?php echo URL_TEMPLATE ?>js/jquery-1.11.3.js"></script>
	<?php } else { ?>
    	<link type="text/css" href="<?php echo URL_TEMPLATE ?>css/ui-lightness/jquery.tooltip.css" rel="stylesheet" />
    	<script type="text/javascript" src="<?php echo URL_TEMPLATE ?>js/jquery-1.4.2.min.js"></script>
    	<script type="text/javascript" src="<?php echo URL_TEMPLATE ?>js/jquery-ui-1.8.5.custom.min.js"></script>
    	<script type="text/javascript" src="<?php echo URL_TEMPLATE ?>js/jquery.tooltip.js"></script>
    	<script type="text/javascript" src="<?php echo URL_TEMPLATE ?>js/jquery.validate.js"></script>
	<?php } ?>
	<script type="text/javascript" src="<?php echo URL_TEMPLATE ?>js/fgc.block.user.js"></script>

        <script type="text/javascript">
	    $(document).ready(function () {
		//chage background color of table
		$('table.list_table tr:even').addClass('even');
		$('table.list_table tr:odd').addClass('odd');
		$('table.tb_content_answer tr:even').addClass('even');
		$('table.tb_content_answer tr:odd').addClass('odd');
		$('table.tb_content_question tr:even').addClass('even');
		$('table.tb_content_question tr:odd').addClass('odd');
	    });
        </script>
    </head>
    <body>

        <div id="rwapper">
	    <?php
	    include_once (PATH_ADMIN . 'conditions' . DS . 'login.php');
	    $checklogin = new LoginConditions();
	    ?>
            <div class="header"><?php $templateloader->loadHeader(); ?></div>
            <div class="main_body">
		<?php if ($checklogin->check()): ?>
    		<div id="left">
    		    <div id="title_menu">
    			<div id="conner_left"></div><!--end conner-left-->
    			<div id="conner_right"></div><!--end conner-left-->
    			<h2>Admin Menu</h2>
    		    </div>
    		    <ul class="menu">
    			<li><a href= "index.php?apps=user">User</a></li>
    			<li><a href= "index.php?apps=config">Config</a></li>
    			<li id="testsection" class="<?php if ($_REQUEST['apps'] == 'practicesection' || $_REQUEST['apps'] == 'testsection') echo 'actionmenu' ?>">
    			    <a href="#">Math Test</a>
    			    <ul class="ul_testsection">
    				<li><a href="index.php?apps=testsection">Questions</a></li>
    				<li><a href="index.php?apps=testsection&task=report">List report</a></li>
    				<li><a href="index.php?apps=testsection&task=view&layout=configtime">Config Time</a></li>
    			    </ul>
    			</li>
    			<li id="iqtest" class="<?php if ($_REQUEST['apps'] == 'iqtest') echo 'actionmenu' ?>">
    			    <a href="#">IQ test</a>
    			    <ul class="ul_iqtest">
    				<li><a href="index.php?apps=iqtest">Question IQ test</a></li>
    				<li><a href="index.php?apps=iqtest&task=report">List report</a></li>
    				<li><a href="index.php?apps=iqtest&task=view&layout=configtime">Config Time</a></li>
    			    </ul>
    			</li>
    			<li id="testscore" class="<?php if ($_REQUEST['apps'] == 'testscore') echo 'actionmenu' ?>">
    			    <a href="#">Short Term Memory Test</a>
    			    <ul class="ul_testscore">
    				<li><a href="index.php?apps=testscore">Questions</a></li>
    				<li><a href="index.php?apps=testscore&task=reports">List report</a></li>
    				<li><a href="index.php?apps=testscore&task=configtime">Config Time</a></li>
    			    </ul>
    			</li>
    			<li id="math" class="<?php if ($_REQUEST['apps'] == 'math') echo 'actionmenu' ?>">
    			    <a href="#">Mental Arithmetic Test</a>
    			    <ul class="ul_math">
    				<li><a href="index.php?apps=math">Questions</a></li>
    				<li><a href="index.php?apps=math&task=reports">List report</a></li>
    				<li><a href="index.php?apps=math&task=configtime">Configs</a></li>
    			    </ul>
    			</li>
			<li id="mathchild" class="<?php if ($_REQUEST['apps'] == 'mathchild') echo 'actionmenu' ?>">
    			    <a href="#">Mental Arithmetic Test Child</a>
    			    <ul class="ul_mathchild">
    				<li><a href="index.php?apps=mathchild">Questions</a></li>
    				<li><a href="index.php?apps=mathchild&task=reports">List report</a></li>
    				<li><a href="index.php?apps=mathchild&task=configtime">Configs</a></li>
    			    </ul>
    			</li>
    		    </ul>
    		</div>
    		<div id="right">
    		    <div id="main">
			    <?php $templateloader->loadApp(); ?>
    		    </div>
    		</div>
		<?php else: ?>
    		<div class="show_login_form">
			<?php $templateloader->loadApp('login'); ?>
    		</div>
		<?php endif; ?>

            </div>
            <div class="footer"><?php $templateloader->loadFooter(); ?></div>
        </div>
    </body>
</html>



