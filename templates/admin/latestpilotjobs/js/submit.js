/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

function submitform(task)
{

    if(typeof task=='undefined'||task==''){
        if(validate()){
            document.adminForm.submit();
        }
    }else{
        document.adminForm.submit();
    }
}

function checked(task)
{
    var total=""
    var arraycid = document.getElementsByName('cid[]');
    for(var i=0; i < arraycid.length; i++){
        if(arraycid[i].checked==true)
        {
            total +=arraycid[i].value + "\n";
        }
    }
    if(total=="" && task !='add' && task !='addpq')
    {
        alert("Please select your choice");
        return false;
    }
    else{
        //        if(task=='add'){
        //            task = 'edit';
        //        }else if(task=='addpq'){
        //            task = 'addpq';
        //        }
        switch (task) {
            case 'add':
                task = 'edit';
                break;
            case 'addpq':
                task = 'addpq';
                break;
            case 'editpq':
                task = 'editpq';
                break;
            case 'editrq':
                task = 'editrq';
                break;
            case 'deleteaq':
                task = 'deleteaq';
                break;
            case 'deleteanswer':
                task = 'deleteanswer';
                break;
            case 'delete':
                task = 'delete';
                break;
            default:
                break;
        }
        var element = document.getElementById('name_task_submit');
        element.setAttribute('value', task);
        submitform(task);
    //return true;
    }
  
}
function alert_name(name){
    switch (name){
        case 'name':
            alert("Name is not null");
            break;
        case 'username':
            alert("Username is not null");
            break;
        case 'user_type':
            alert("User type is not null");
            break;
        case 'airline_id':
            alert("Airline name is not null");
            break;
        case 'filename':
            alert("File name is not null");
            break;
        case 'type':
            alert("Type is not null");
            break;
        case 'userid':
            alert("Username is not null");
            break;
        case 'interview_id':
            alert("Interview is not null");
            break;
        case 'start_time':
            alert("Start time is not null");
            break;
        case 'end_time':
            alert("End time is not null");
            break;
        case 'choice':
            alert("Choice is not null");
            break;
    }
}
function validate(){
    var msg = '';
    var name='';
    var arrayclass = "";
    //var array = new Array();
    for(i=0;i<document.adminForm.elements.length;i++){
        arrayclass = document.adminForm.elements[i].getAttribute('class');
        if(arrayclass!="" && arrayclass !=null){
            arrayclass = arrayclass.split(" ");
            if(inArray(arrayclass,'required')){
                if((document.adminForm.elements[i].value=="") || (document.adminForm.elements[i].value==0)){
                    // array.push(document.adminForm.elements[i].element);
                    name = document.adminForm.elements[i].name;
                    //msg = msg + name+" This is not null\n";
                    document.adminForm.elements[i].focus();
                    if(i==0){
                        document.adminForm.elements[i].style.border='1px solid #7F9DB9';
                        document.adminForm.elements[i].style.border='1px solid red';
                    }else{
                        document.adminForm.elements[i-1].style.border='1px solid #7F9DB9';
                        document.adminForm.elements[i].style.border='1px solid red';
                    }

                    //alert(msg) ;
                    alert_name(name);
                    //var lab = document.adminForm.elements[i].getElementById('fgc_airline');
                    //alert(lab);
                    //                    var content = document.createTextNode(msg);
                    return false;
                }
            }
            if(inArray(arrayclass,'email')){
                field_text = document.adminForm.elements[i].value;
                reg=/^[0-9A-Za-z]+[0-9A-Za-z_]*@[\w\d.]+.\w{2,4}$/;
                if(!reg.test(field_text)){
                    name = document.adminForm.elements[i].name;
                    msg = msg +"Please enter a valid email address.";
                    document.adminForm.elements[i].focus();
                    alert(msg) ;
                    return false;
                }
            }
            if(inArray(arrayclass, 'pass')){
                if(document.adminForm.elements[i-1].value!=document.adminForm.elements[i].value){
                    name = document.adminForm.elements[i].name;
                    msg = msg + "Please enter the same value again";
                    document.adminForm.elements[i].focus();
                    alert(msg);
                    return false;
                }
            }
            if(inArray(arrayclass, 'end_time')){
                var date1 = new Date(document.adminForm.elements[i].value);
                var date2 = new Date(document.adminForm.elements[i-1].value);
                if((date1-date2)<0){
                    msg = msg + "Time end must be greater than time start\n";
                    document.adminForm.elements[i].focus();
                    alert(msg);
                    return false;
                }
            }
            if(inArray(arrayclass, 'number')){
                field_text = document.adminForm.elements[i].value;
                reg=/^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/;
                if(!reg.test(field_text) || (parseFloat(field_text)<0)){
                    name = document.adminForm.elements[i].name;
                    msg = msg + " Wrong format\n";
                    document.adminForm.elements[i].focus();
                    alert(msg) ;
                    return false;
                }
            }
            if(inArray(arrayclass, 'month')){
                field_text = document.adminForm.elements[i].value;
                reg=/^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/;
                if(!reg.test(field_text) ||(parseFloat(field_text)<0) ||(parseFloat(field_text)>12)){
                    name = document.adminForm.elements[i].name;
                    msg = msg +"Wrong format or month must be less than 13 ";
                    document.adminForm.elements[i].focus();
                    alert(msg) ;
                    return false;
                }
            }
        }
    }
    return true;
}
function inArray(array,value){
    for(j=0;j<array.length;j++){
        if(array[j]==value){
            return true
        }
    }
    return false
}
jQuery(document).ready(function($){
    $("#select_choice").change(function(){
        var choice=$("#select_choice option:selected").val();
        if(choice=='free'){
            $("input[name=price]").attr('class', "");
            $("input[name=price]").attr('readonly','readonly');
            $("input[name=price]").attr('value','');
        }else{
            $("input[name=price]").attr('class', "required number");
            $("input[name=price]").attr('readonly','');
        }
    });
    $("#select_option_unit").change(function(){
        var option=$("#select_option_unit option:selected").val();
        $.ajax({
            url:"index.php?apps=config&format=raw&task=ajax_time",
            type: "POST",
            data:'unint_time='+option,
            dataType:"html",
            success: function(data){
                if(data!=''){
                    $(".value_time_config").html(data);
                }
            }
        })
    })
})


