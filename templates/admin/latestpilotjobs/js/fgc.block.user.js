function blockuser(cid, urlsite, urltemplate) {
    (function ($) {
	var action = $('.fgc_block_' + cid).attr('action');
	$.ajax({
	    url: urlsite + "index.php?apps=user&task=block&format=raw&cid=" + cid + '&block=' + action,
	    type: "post",
	    dataType: "html",
	    success: function (data) {
		if (data == '1') {
		    $(".image_block_" + cid).attr('src', urltemplate + "images/publish_x.png");

		} else {
		    $(".image_block_" + cid).attr('src', urltemplate + "images/tick.png");
		}
		$(".fgc_block_" + cid).attr('action', data);

	    }
	});
    })(jQuery);
}
jQuery(document).ready(function () {
    $("#question").click(function () {
	$(".question_type").slideDown();
    })
    $("#management").click(function () {
	$(".management_test").slideDown();
    })
    $("#mathematics").click(function () {
	$(".mathematics_test").slideDown();
    })
    $("#mathseries").click(function () {
	$(".series_test").slideDown();
    })
    $("#mathreasoning").click(function () {
	$(".reasoning_test").slideDown();
    })
    $("#pattern").click(function () {
	$(".pattern_test").slideDown();
    })
    $("#testtext").click(function () {
	$(".test_text").slideDown();
    })
    $("#aviation").click(function () {
	$(".aviation_english").slideDown();
    })
    $("#testsection").click(function () {
	$(".ul_testsection").slideDown();
    })
    $("#iqtest").click(function () {
	$(".ul_iqtest").slideDown();
    })

    $("#testscore").click(function () {
	$(".ul_testscore").slideDown();
    })

    $("#math").click(function () {
	$(".ul_math").slideDown();
    })
    
    $("#mathchild").click(function () {
	$(".ul_mathchild").slideDown();
    })
    
    var Choose = '';
    var answer = 0;
    var strabc = '0,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z';
    var arrChoose = strabc.split(',');
    $("#fgcaddquestion").click(function (e) {
	e.preventDefault();
	answer = answer + 1;
	Choose = arrChoose[answer];
	//alert(Choose)
	var html = '';
	html = html + '<div>';
	html = html + '<label class="">' + Choose + '.</label>';
	html = html + '<input type="radio" name="right_answer[]" value="' + answer + '" />';
	html = html + '<input class="answer_choice" type="text" name="answer[]" /><br />';
	html = html + '</div>';
	$("#fgclistaddquestion").append(html);
    })
})
