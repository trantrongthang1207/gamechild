/* 
 Created on : Aug 14, 2015, 2:42:23 PM
 Author     : Tran Trong Thang
 Email      : trantrongthang1207@gmail.com
 Skype      : trantrongthang1207
 */
var $strabc = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
jQuery(document).ready(function ($) {
    $('#addoperation').click(function (evt) {
	evt.preventDefault();
	var operation = $('#operations option:selected').val();
	$('.operationlists').append('<div class="ui-droppable tv-drop"><span></span> ' + renderSelectbox(operation) + '</div>');
    })
    $(".math_answers").mask('000000000000000000000000000000000000000000000000', {
	translation: {
	    '0': {
		pattern: /[-+=x()/0-9]/, optional: true
	    }
	}
    });
    $(".input_question_content").mask('000000000000000000000000000000000000000000000000');
    $('.suggest_answer').mask('000000000000000000000000000000000000000000000000', {
	translation: {
	    '0': {
		pattern: /[;0-9]/, optional: true
	    }
	}
    });
    $(".math_answers").keyup(function (e) {
	if (e.keyCode == 32) {
	    $(this).val($.trim($(this).val()));
	    alert("Please dont user space");
	}
    })
    $(document).on('click', '.ui-droppable.tv-drop span, span.tvdelete', function () {
	$(this).parent().remove();
    });
    function renderSelectbox(operation) {
	if (operation == 'N') {
	    var selectbox = '<select name="question_contents[]" size="1" class="operations">';
	} else {
	    var selectbox = '<select name="question_content[]" size="1" class="operations">';
	}
	var selected = '';
	if (operation == '?') {
	    selected = 'selected="selected"';
	    selectbox += '<option ' + selected + ' value="?">&nbsp;?&nbsp;</option>';
	} else {
	    selected = '';
	    selectbox += '<option ' + selected + ' value="?">&nbsp;?&nbsp;</option>';
	}

	if (operation == '+') {
	    selected = 'selected="selected"';
	    selectbox += '<option ' + selected + ' value="+">&nbsp;+&nbsp;</option>';
	} else {
	    selected = '';
	    selectbox += '<option ' + selected + ' value="+">&nbsp;+&nbsp;</option>';
	}

	if (operation == '-') {
	    selected = 'selected="selected"';
	    selectbox += '<option ' + selected + ' value="-">&nbsp;-&nbsp;</option>';
	} else {
	    selected = '';
	    selectbox += '<option ' + selected + ' value="-">&nbsp;-&nbsp;</option>';
	}

	if (operation == 'x') {
	    selected = 'selected="selected"';
	    selectbox += '<option ' + selected + ' value="x">&nbsp;x&nbsp;</option>';
	} else {
	    selected = '';
	    selectbox += '<option ' + selected + ' value="x">&nbsp;x&nbsp;</option>';
	}

	if (operation == '/') {
	    selected = 'selected="selected"';
	    selectbox += '<option ' + selected + ' value="/">&nbsp;/&nbsp;</option>';
	} else {
	    selected = '';
	    selectbox += '<option ' + selected + ' value="/">&nbsp;/&nbsp;</option>';
	}

	if (operation == '(') {
	    selected = 'selected="selected"';
	    selectbox += '<option ' + selected + ' value="(">&nbsp;(&nbsp;</option>';
	} else {
	    selected = '';
	    selectbox += '<option ' + selected + ' value="(">&nbsp;(&nbsp;</option>';
	}

	if (operation == ')') {
	    selected = 'selected="selected"';
	    selectbox += '<option ' + selected + ' value=")">&nbsp;)&nbsp;</option>';
	} else {
	    selected = '';
	    selectbox += '<option ' + selected + ' value=")">&nbsp;)&nbsp;</option>';
	}
	if (operation == 'N') {
	    selected = 'selected="selected"';
	    selectbox += '<option ' + selected + ' value="N">&nbsp;0&nbsp;</option>';
	} else {
	    selected = '';
	    selectbox += '<option ' + selected + ' value="N">&nbsp;0&nbsp;</option>';
	}

	selectbox += '</select>';
	if (operation == 'N') {
	    selectbox += '<input type="text" name="question_content[]" class="input_question_content">';
	}

	return selectbox;
    }
    $(document).on('change', '.operationlists select', function () {
	var operation = $(this).val();
	if (operation == 'N') {
	    $(this).attr('name', 'question_contents[]');
	    $(this).after('<input type="text" name="question_content[]" class="input_question_content">');
	} else {
	    $(this).attr('name', 'question_content[]');
	    $(this).parent().find('.input_question_content').remove();
	}
    });

    $('#addanswer').click(function (evt) {
	evt.preventDefault();
	var lenghtLi = $('.listanswers li').length;
	//alert(lenghtLi+$strabc[lenghtLi]);
	var htmlLi = '<li>';
	htmlLi += '<span>';
	htmlLi += $strabc[lenghtLi];
	htmlLi += '</span>';
	htmlLi += '<input type="checkbox" name="right_answer" value="<?php echo $i ?>" class="right_answer"/>';
	htmlLi += '<input type="text" name="answers[]" class="math_answers"/>';
	htmlLi += '<span class="tvdelete">';
	htmlLi += '</span>';
	htmlLi += '</li>';
	$(".listanswers").append(htmlLi);
    })


    $('#submitformmathchild').click(function (evt) {
	evt.preventDefault();
	if ($('.suggest_answer').val() == '') {
	    alert("Please number");
	    return false;
	}
	$('#frm_user_edit').submit()
    })
})
