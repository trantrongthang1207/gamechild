function published(cid,urlsite,urltemplate){
    (function($) {
        var action=$('.fgc_block_'+cid).attr('action');
        $.ajax({
            url:urlsite+"index.php?apps=airline&task=published&format=raw&cid="+cid+'&valuepublish='+action,
            type:"post",
            dataType:"html",
            success:function(data){
                if(data=='0'){
                    $(".image_block_"+cid).attr('src', urltemplate+"images/publish_x.png");

                }else{
                    $(".image_block_"+cid).attr('src', urltemplate+"images/tick.png");
                }
                $(".fgc_block_"+cid).attr('action',data);

            }
        });
    })(jQuery);
}
function published2(cid,urlsite,urltemplate){
    (function($) {
        var action=$('.fgc_block_'+cid).attr('action');
        $.ajax({
            url:urlsite+"index.php?apps=questionsgame&task=published&format=raw&cid="+cid+'&valuepublish='+action,
            type:"post",
            dataType:"html",
            success:function(data){
                if(data=='0'){             
                    $(".image_block_"+cid).attr('src', urltemplate+"images/publish_x.png");

                }else{
                    $(".image_block_"+cid).attr('src', urltemplate+"images/tick.png");
                }
                $(".fgc_block_"+cid).attr('action',data);

            }
        });
    })(jQuery);
}
function publishedManagement(cid,urlsite,urltemplate,apps){
    (function($) {
        var action=$('.fgc_block_'+cid).attr('action');
        $.ajax({
            url:urlsite+"index.php?apps="+apps+"&task=published&format=raw&cid="+cid+'&valuepublish='+action,
            type:"post",
            dataType:"html",
            success:function(data){
                if(parseInt(data) == 0){
                    $(".image_block_"+cid).attr('src', urltemplate+"images/publish_x.png");

                }else{
                    $(".image_block_"+cid).attr('src', urltemplate+"images/tick.png");
                }
                $(".fgc_block_"+cid).attr('action',data);

            }
        });
    })(jQuery);
}

