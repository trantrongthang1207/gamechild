jQuery(document).ready(function($) {

    $(".list_table :checkbox[name=checkAll]").click(function(e) {

        if ($(this).attr('checked')) {
            //check
            $(".list_table input:checkbox[name='cid[]']").attr('checked','checked');
        }
        else {
            //uncheck
            $(".list_table input:checkbox[name='cid[]']").attr('checked','');
        }
    })
    $(".list_table input:checkbox[name='cid[]']").change(function() {
        if ($(this).attr('checked')) {
            if( $(".list_table :checked[name='cid[]']").length==$(".list_table input:checkbox[name='cid[]']").length) {
                $(".list_table :checkbox[name=checkAll]").attr('checked','checked');
            }
        }
        else {
            if( $(".list_table :checked[name='cid[]']").length==0) {
                $(".list_table :checkbox[name=checkAll]").attr('checked','');
            }
        }
    })
    $(".addrow_bk").click(function(){
        var i = 0;
        i = $(".tb_content_answer tr:last td:first").text();
        i = parseInt(i) + 1;
        var $content = "<tr><td>"+ i + "</td><td><label>Image answer:</label><br/><input type='file' name='image_answer[]'/><input type='hidden' name='id_ans[]' value=''/></td><td><label>Choose:</label><select id='select_choice' class='required_not' name='right_answer[]'><option value='0'>-Select your choice-</option><option value='1'>right</option><option value='0'>wrong</option></select></td><td><a class='remove_row'>del</a></td></tr>";
        $(".tb_content_answer").append($content);
    })
    $(".addrow_").click(function(){
        var i = 0;
        var class_tr = "";
        i = $(".tb_content_answer tr:last td:first").text();
        i = parseInt(i) + 1;

        if( i % 2 ==0){
            class_tr = "odd";
        }else{
            class_tr = "even";
        }
        //var $content = "<tr class='"+class_tr + "'><td>" + i + "</td><td><input type='file' name='image_answer[]'></td><td><input type='text' name='descriptions_answers[]'></td><td><input type='checkbox' name='check[]'></td><td><a class='remove_row'>Remove</a></td></tr>";
        var $content = "<tr class='" + class_tr + "'><td>" + i +  "</td><td><input type='file' name='image_answer[]'/></td><td><input type='text' name='description_answers[]'></td><td><select id='select_choice' class='required_not' name='right_answer[]'><option value='0'>-Select your choice-</option><option value='1'>right</option><option value='0'>wrong</option></select></td><td><a class='remove_row'>Remove</a></td></tr>";
        $(".tb_content_answer").append($content);
    })
    $("a.remove_row").click(function(){
        $(this).parent().parent().html(" ");
    })
})
function del_pq(cid, urlsite){
    (function($){
        var url_action = $(".del_answer_"+cid).attr('action');
        $.ajax({
            url: urlsite + url_action,
            type: "post",
            success: function(data){
                alert(data);
                //$(".del_answer_"+cid).parent().parent().slideUp();
                //$(".del_answer_"+cid).parent().parent().html(" ");
            }
        })
    })(jQuery)
}
function del_rq(cid, urlsite){
    (function($){
        var url_action = $(".del_answer_rq"+cid).attr('action');
        $.ajax({
            url: urlsite + url_action,
            type: "post",
            success: function(data){
                $(".del_answer_rq"+cid).parent().parent().html(" ");
            }
        })
    })(jQuery)
}







