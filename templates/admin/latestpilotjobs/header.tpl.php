<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<div id="banner">
    <h1 id="title"><?php echo TITLE_SITE_ADMIN;?></h1>
    <?php
    $app = App::getInstance();
    $session = $app->getSession();
    $username = $session->get('username', '', 'adminlogin');
    $checklogin = new LoginConditions();
    if($checklogin->check()):
    ?>

    <p class="welcome">
        <span>Hi: <strong class="customer_username"><?php echo $username; ?></strong>!</span>

        <a class="logout" href="index.php?apps=login&task=logout">Logout</a>
    </p>
    <?php endif;?>
</div><!--end banner-->