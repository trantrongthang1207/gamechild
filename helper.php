<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class helperSite {

    function __construct() {

    }

    function checkPaypal($userid, $filename) {
        $app = App::getInstance();
        $db = $app->getDBO();
        $interview_id = self::getInterviewIdByFilename($filename);
        $curentTime = date("Y-m-d");
        if ($interview_id > 0) {
            $query = "SELECT sub.* FROM interviews inter,subscribe_interviews sub" .
                    " WHERE sub.interview_id=inter.id AND sub.userid=" . $db->Quote($userid) .
                    " AND sub.start_time <= " . $db->Quote($curentTime) . " AND sub.end_time >=" . $db->Quote($curentTime) .
                    " AND inter.id=" . $db->Quote($interview_id);
            $db->setQuery($query);
            if ($result = $db->loadObjectList())
                return 2;
            else
                return 1;
        }else {
            return 0;
        }
    }

    function getObjectInterviewByFilename($filename) {
        $app = App::getInstance();
        $db = $app->getDBO();
        $query = "SELECT * FROM interviews WHERE filename=" . $db->Quote($filename) . " LIMIT 1";
        $db->setQuery($query);
        $result = $db->loadObject();
        if (!empty($result)) {
            return $result;
        } else {
            return 0;
        }
    }

    function getAirlineById($id) {
        $app = App::getInstance();
        $db = $app->getDBO();
        $query = "SELECT * FROM airlines WHERE id=" . $db->Quote($id);
        $db->setQuery($query);
        $result = $db->loadObject();
        if (!empty($result)) {
            return $result;
        } else {
            return array();
        }
    }

    function getAirlines() {
        $app = App::getInstance();
        $db = $app->getDBO();
        $query = "SELECT * FROM airlines where published = 1";
        $db->setQuery($query);
        $result = $db->loadObjectList();
        if (!empty($result)) {
            return $result;
        } else {
            return array();
        }
    }

    function getAirlineIDByName($airline_name) {
        $app = App::getInstance();
        $db = $app->getDBO();
        $query = "SELECT * FROM airlines where published = 1 and name =" . $airline_name;
        $db->setQuery($query);
        $result = $db->loadObjectList();
        if (!empty($result)) {
            return $result;
        } else {
            return array();
        }
    }

    function getInteviewsByAirlineId($airline_id) {
        $app = App::getInstance();
        $db = $app->getDBO();
        $query = "SELECT * FROM interviews WHERE airline_id='$airline_id'";
        $db->setQuery($query);
        if ($result = $db->loadObjectList()) {
            return $result;
        } else {
            return array();
        }
    }

    function getInterviewIdByFilename($filename) {
        $app = App::getInstance();
        $db = $app->getDBO();
        $query = "SELECT id FROM interviews WHERE filename=" . $db->Quote($filename) . " LIMIT 1";
        $db->setQuery($query);
        $result = $db->loadObject();
        if (!empty($result)) {
            return $result->id;
        } else {
            return 0;
        }
    }

    function getContent($filename) {
        $app = App::getInstance();
        $db = $app->getDBO();
        $query = "SELECT content_free FROM interviews WHERE filename=" . $db->Quote($filename) . " LIMIT 1";
        $db->setQuery($query);
        $result = $db->loadObject();
        if (!empty($result)) {
            return $result->content_free;
        } else {
            return '';
        }
    }

    function getEndTime($start_time, $value_time, $unit_time='') {
        $time = new DateTime($start_time);
        $date = $time->format('d');
        $month = $time->format('m');
        $year = $time->format('Y');
        switch ($unit_time) {
            case "d":
                $timesend = date('Y-m-d', mktime(0, 0, 0, $month, $date + $value_time, $year));
                break;
            case "m":
                $timesend = date('Y-m-d', mktime(0, 0, 0, $month + $value_time, $date, $year));
                break;
            case "y":
                $timesend = date('Y-m-d', mktime(0, 0, 0, $month, $date, $year + $value_time));
                break;
            default :
                $timesend = date('Y-m-d', mktime(0, 0, 0, $month, $date + $value_time, $year));
                break;
        }
        return $timesend;
    }

    function getNameTime($unit_time) {
        switch ($unit_time) {
            case "d":
                $nametime = "Day";
                break;
            case "m":
                $nametime = "Month";
                break;
            case "y":
                $nametime = "Year";
                break;
        }
        return $nametime;
    }

    function verify_valid_email($emailtocheck) {
        $eregicheck = "^([-!#\$%&'*+./0-9=?A-Z^_`a-z{|}~])+@([-!#\$%&'*+/0-9=?A-Z^_`a-z{|}~]+\\.)+[a-zA-Z]{2,4}\$";
        return eregi($eregicheck, $emailtocheck);
    }

    function sendMail($sendto, $message, $subject='Object send mail', $namesend='', $from='', $bcc='') {
        $subject = nl2br($subject);
        $message = nl2br($message);
        if ($bcc != "") {
            $headers = "Bcc: " . $bcc . "\n";
        }
        $headers = "MIME-Version: 1.0\n";
        $headers .= "Content-type: text/html; charset=iso-utf-8\n";
        $headers .= "X-Priority: 3\n";
        $headers .= "X-MSMail-Priority: Normal\n";
        $headers .= "X-Mailer: PHP/" . "MIME-Version: 1.0\n";
        if ($namesend != '') {
            $headers .= "From: $namesend<" . $from . ">\n";
        }
        $headers .= "Content-Type: text/html\n";
        if (@mail($sendto, $subject, $message, $headers))
            return true;
        else
            return false;
    }

    function findString($string, $stringSearh, $offset=0, $end=0) {
        $lensub = strlen($stringSearh);
        if ($end == 0) {
            $end = strlen($string);
        }

        for ($i = $offset; $i < $end; $i++) {
            $substring = substr($string, $i, $lensub);
            if ($substring == $stringSearh) {
                return true;
                break;
            }
        }
        return false;
    }

    function getResult($number1, $cal, $number2) {
        switch ($cal) {
            case '+':
                $number = $number1 + $number2;
                break;
            case '-':
                $number = $number1 - $number2;
                break;
            case '*':
                $number = $number1 * $number2;
                break;
            case '/':
                $number = $number1 / $number2;
                break;
            default :
                $number = 0;
        }
        if (strpos($number, '.')) {
            return number_format($number, 2, '.', '');
        } else {
            return $number;
        }
    }

}

?>
