<?php

class UsersTable extends Table {

    var $id = NULL;
    var $name = NULL;
    var $username = NULL;
    var $email = NULL;
    var $password = NULL;
    var $user_type = NULL;
    var $block = NULL;
    var $send_email = NULL;
    var $register_date = NULL;
    var $last_visit_date = NULL;
    var $checked_out = NULL;
    var $checked_out_time = NULL;

    function __construct(&$db) {

        parent::__construct('users', 'id', $db);
    }

    function __destruct() {

        parent::__destruct();
    }

}

?>