<?php

class Template extends RootObject {

    var $templatePath = NULL;
    var $templateFile = NULL;
    var $html = '';
    var $data = NULL;

    function __construct() {

        parent::__construct();
    }

    function __destruct() {

        parent::__destruct();
    }

    function setData(&$data) {
     
        if (is_object($data))
            $this->data = clone $data;

        else
            $this->data = $data;
        
    }

    function setVar($varName, &$value) {

        if (is_object($value))
            $this->data[$varName] = clone $value;

        else
            $this->data[$varName] = $value;
    }

    function setTemplatePath($templatePath) {
       
        $this->templatePath = $templatePath;
    }

    function setTemplateFile($templateFile) {

        $this->templateFile = $templateFile;
    }

    function loadTemplate($templateFile=NULL) {

        if (empty($templateFile))
            $templateFile = $this->templateFile;

        if (file_exists($this->templagePath . $templateFile))
            return file_get_contents($this->templagePath . $templateFile);
        else
            return NULL;
    }

    function render() {
        
        $templateFilePath = $this->templatePath  . $this->templateFile;
       
        $this->html = '';

        if (file_exists($templateFilePath)) {
         
            ob_start();
           // print_r($this->data);
            if ($this->data) {

                foreach ($this->data as $varname => $var) {

                    ${$varname} = $var;

                }
            }
            // print_r($this->data);
            include($templateFilePath);

            $this->html = ob_get_clean();
        }
        else
            $this->errorMsg[] = "templateFile " . $templateFilePath . " is not existed";
        
        return $this->html;
    }

    function parseTemplate($templateFile='') {

        if ($templateFile) {

            $this->templateFile = $templateFile;
        }

        return $this->render();
    }

    function getOutput() {

        return $this->html;
    }

    function getHTML() {

        return $this->html;
    }

    function output() {

        echo $this->html;
    }

}

?>