<?php

class SiteTemplate extends Template {

    var $name = 'default';
    var $type = 'site';

    function __construct($templateName, $templatePath=PATH_TEMPLATES) {

        $this->name = $templateName;
        
        $this->setTemplatePath($templatePath . $templateName . DS);

        $this->setTemplateFile('index.tpl.php');

        parent::__construct();
    }

    function __destruct() {

        parent::__destruct();
    }

    function loadHead() {

        /* @var $application Application */
        global $application;

        $setting = $application->getSetting();

        $title = $setting->getTitle();

        $keywords = $setting->getKeywords();

        $head = "<title>$title</tilte>";
        $head.="<meta name='description' content='$description' />" .
                "<meta name='keywords' content='$keywords' />" .
                "<meta name='author' content='$author' />";

        echo $head;
    }

    function loadApp() {

        $app = App::getInstance();

        $setting = $app->getSetting();

        $router = $app->getRouter();

        $executed_app = $setting->getApp($router->getPageId());

        $class = $executed_app['params']['class'];

        echo "<div class='page $class'>" . $app->getOutput() . "</div>";
    }

    function loadBlock($blockName) {

        $html = '';

        $app = App::getInstance();

        $router = $app->getRouter();

        if ($widgets = $this->getWidgets($router->getPageId(), $blockName)) {

            $setting = $app->getSetting();

            foreach ($widgets as $widget_id) {

                $widget = $setting->getWidget($widget_id);

                $widgetExecuteFile = PATH_WIDGETS . $widget['name'] . DS . $widget['name'] . ".php";
                
                if (PATH_ROOT!=PATH_SITE && !file_exists($widgetExecuteFile)) {

                    define('PATH_WIDGETS', PATH_ROOT.'widgets');
                    $widgetExecuteFile = PATH_WIDGETS . $widget['name'] . DS . $widget['name'] . ".php";
                }

                if (file_exists($widgetExecuteFile)) {

                    ob_start();

                    addClassPath(PATH_WIDGETS . $widget['name'] . DS . 'helpers');

                    include($widgetExecuteFile);

                    $class = $widget['params']['class'];

                    $html .= "<div class='widget $class'>" . ob_get_clean() . "</div>";
                }
                if (PATH_ROOT!=PATH_SITE) {
                    define('PATH_WIDGETS', PATH_SITE.'widgets');
                }
            }
        }

        echo $html;
    }

    function setTemplateName($templateName) {

        $this->name = $templateName;

        $this->setTemplatePath($templatePath . $templateName . DS);
    }

    function getPath($name) {

        if (is_dir(PATH_TEMPLATES . $this->name . DS . $name))
            return PATH_TEMPLATES . $this->name . DS . $name;
        else
            return '';
    }

    function getCssPath() {
        return $this->getPath('css');
    }

    function getJsPath() {
        return $this->getPath('js');
    }

    function getImagepath() {
        return $this->getPath('images');
    }

    function loadCss($fileName) {
        $url = $this->getTemplateUrl() . "css/$fileName";
        echo "<link rel='stylesheet' type='text/css' href='$url' />";
    }

    function loadJs($fileName) {
        $url = $this->getTemplateUrl() . "css/$fileName";
        echo "<script type='javascript/text' src='$url'></script>";
    }

    /**
     *
     * @staticvar <type> $pageWidgets
     * @param <type> $page_id
     * @param <type> $blockName
     * @return <type> 
     */
    function getWidgets($page_id, $blockName) {

        static $pageWidgets;

        $return = array();

        if (!isset($pageWidgets[$page_id])) {

            $app = App::getInstance();

            $setting = $app->getSetting();

            $pageWidgets[$page_id] = $setting->getWidgets($page_id);
        }

        if (!empty($pageWidgets[$page_id])) {

            $templateSetting = $this->getSetting();

            if ($templateWidgets = $templateSetting->getWidget($blockName)) {

                foreach ($templateWidgets as $widget_id) {
                    if (in_array($widget_id, $pageWidgets[$page_id]))
                        $return[] = $widget_id;
                }
            }
        }

        return $return;
    }

    /**
     *
     * @staticvar <type> $setting
     * @return TemplateSetting 
     */
    function getSetting() {
        static $setting;

        if (empty($setting)) {
            $app = App::getInstance();
            $setting = $app->getSetting();

            $setting = new TemplateSetting($setting->getTemplate());
        }
        return $setting;
    }

    function hasWidget($blockName) {

        $app = App::getInstance();

        $router = $app->getRouter();

        if ($widgets = $this->getWidgets($router->getPageId(), $blockName)) {

            return true;
        } else {
            return false;
        }
    }

    function getTemplateUrl() {
        static $url_template;
        if (empty($url_template))
            $url_template = URL_SITE . 'templates/' . $this->name."/";
        return $url_template;
    }

}

?>