<?php

class AppTemplate extends Template {

    var $layout = 'default';
    var $type = 'app';
    var $name='df';
    var $view='fd';

    function __construct($name, $view, $layout='default') {

        $this->name=$name;

        $this->view=$view;

        $this->layout = $layout;

        $this->setTemplatePath(PATH_APPS.$this->name . DS.'views'.DS);

        $this->setTemplateFile($this->view."_".$this->layout.'.tpl.php');

        parent::__construct();
    }

    function __destruct() {

        parent::__destruct();
    }


}

?>