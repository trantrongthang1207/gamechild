<?php

class WidgetTemplate extends Template {

    var $layout = 'default';
    var $type = 'widget';
    var $name='';

    function __construct($name, $layout='default') {
      
        $this->name=$name;
        
        $this->layout = $layout;

        $this->setTemplatePath(PATH_WIDGETS.$this->name . DS.'tpl'.DS);
      
        $this->setTemplateFile($this->layout.'.tpl.php');

        parent::__construct();
    }

    function __destruct() {

        parent::__destruct();
    }

    
}

?>