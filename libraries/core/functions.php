<?php

/**
 * Redirect
 * @param string $url
 */
function redirect($url, $internal=false) {
    if ($internal) {
        $url = URL_SITE . $url;
    }
    header('Location:' . $url);
    exit();
}

function jsRedirect($url, $internal=false) {
    if ($internal) {
        $url = URL_SITE . $url;
    }

    echo '<script type="text/javascript">
                window.location = "' . $url . '";
            </script>';
}

/**
 * This function try to automatically load a class from 'classes' directory .
 * Never call this function yourself.
 *
 * @param string 	$class
 * @access 	public
 * @return  boolean

 */
function __autoload($class) {

    if (class_exists($class)) {
        return;
    }
    global $classPaths;
    require_once PATH_LIBRARY_CORE . "file" . DS . "path.class.php";

    /* @var $application Application */
    if ($pos = strpos($class, 'Table')) {
        $class = substr($class, 0, $pos);      
    }
    $class_file_name = strtolower($class) . ".class.php";
   
    if ($file = Path::find($classPaths, $class_file_name)) {
        require_once($file);
        return true;
    }
    return false;
}

function addClassPath($path) {
    global $classPaths;
    if (is_array($path) && !empty($path)) {
        foreach ($path as $item) {
            if (!in_array($item, $classPaths))
                $classPaths[] = $item;
        }
    }
    elseif (!in_array($path, $classPaths)) {
        $classPaths[] = $path;
    }
}

function addClassPath2($path, $level=0) {

    global $classPaths;
    if (is_array($path) && !empty($path)) {
        foreach ($path as $item) {

            if ($item != '.' & $item !== '..' & is_dir($item) & !in_array($item, $classPaths)) {

                $classPaths[] = $item;
                if ($level != 0 & $childDirs = scandir($item)) {
                    $basePath = dirname($item);
                    foreach ($childDirs as $dir) {
                        $dir = $basePath . DS . $dir;
                        if ($dir != '.' & $dir !== '..' & is_dir($dir) & !in_array($dir, $classPaths)) {
                            addClassPath($dir, $level - 1);
                        }
                    }
                }
            }
        }
    } elseif ($path != '.' & $path !== '..' & file_exists($path) & !in_array($path, $classPaths)) {
        $classPaths[] = $path;
        if ($level != 0 & $childDirs = scandir($path)) {
            $basePath = dirname($path);
            foreach ($childDirs as $dir) {
                $dir = $basePath . DS . $dir;
                if ($dir != '.' & $dir !== '..' & is_dir($dir) & !in_array($dir, $classPaths)) {
                    addClassPath($dir, $level - 1);
                }
            }
        }
    }
}
addClassPath(PATH_LIBRARIES.'site'.DS.'tables');
?>