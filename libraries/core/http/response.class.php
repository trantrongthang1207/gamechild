<?php

class Response extends RootObject {

    function __construct() {
        
    }

    function __destruct() {

        parent::__destruct();
    }

    function redirect($url) {
        header('Location:' . $url);
        exit();
    }

    function url($url) {
        if (strpos($url, 'http://') === false)
            $url = URL_SITE . $url;
        $uri = new Uri($url);
        $app = App::getInstance();
        if (!$uri->getParam('pageId', '0')) {
            $request = $app->getRequest();
          
            if ($pageId = $request->getVar('pageId', '')) {
                $uri->setParam('pageId', $pageId);
            } elseif ($appName = $uri->getParam('app', '')) {
                $seting = $app->getSetting();
                $pageId = $seting->getPageId($appName);
                $uri->setParam('pageId', $pageId);
            }
        }
        if ($uri->getParam('customId', null) === null) {
            $customId = Request::getVar('customId', null);
            if ($customId !== null) {
                $uri->setParam('customId', $customId);
            }
        }

        return $uri->build();
    }

}

?>