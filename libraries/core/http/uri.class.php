<?php

class Uri extends RootObject {

    var $uri = '';
    var $scheme = '';
    var $host = '';
    var $user = NULL;
    var $pass = NULL;
    var $path = NULL;
    var $query = NULL;
    var $fragment = NULL;
    var $params = array();
    var $separator='&';

    function __construct($uri=NULL) {

        if ($uri) {

            $this->uri = $uri;
        }

        $this->parse();
    }

    function __destruct() {

        parent::__destruct();
    }

    function _parseUri() {

        if ($this->uri) {

            $temp = parse_url($this->uri);

            $this->setProperties($temp);
        }
    }

    function _parseQuery() {

        if ($this->query)
            parse_str($this->query, $this->params);
    }

    function _buildUri() {

        $this->uri = NULL;

        $this->uri = $this->scheme . "://" . $this->host . $this->path . "?" . $this->query;

        return $this->uri;
    }

    function _buildQuery($separator="&") {

        $this->query = NULL;

        //if ($this->params)
        //if ($separator)
      
        $this->query = http_build_query($this->params, '', $separator);

        //else
        //$this->query=http_build_query($this->params);
    }

    function getParam($paramName, $default=NULL) {

        if (isset($this->params[$paramName]))
            return $this->params[$paramName];

        else
            return $default;
    }

    function setParam($paramName, $value) {

        $this->params[$paramName] = $value;
    }

    function clearParam($paramName) {

        if (isset($this->params[$paramName]))
            unset($this->params[$paramName]);
    }
    function clearParams() {
        $this->params=array();
    }
    function getParams() {

        return $this->Params;
    }

    function getUri() {

        return $this->uri;
    }

    function getQuery() {

        return $this->query;
    }

    function parse() {

        if ($this->uri) {

            $this->_parseUri();

            $this->_parseQuery();
        }
    }

    function build($separator='&') {

        $this->_buildQuery($separator);

        return $this->_buildUri();
    }
    function setParams($params, $clear=false) {
        if ($clear) $this->clearParams ();
        if (!empty($params)) {
            foreach ($params as $name=>$value) {
                $this->setParam($name, $value);
            }
        }
    }
    function setScheme($value) {
        $this->set('scheme',$value);
    }
    function setHost($value) {
        $this->set('host',$value);
    }
    function setPath($value) {
        $this->set('path', $value);
    }
    
}

?>