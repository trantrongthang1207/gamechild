<?php

class Database extends RootObject {

    var $name = '';
    var $_sql = '';
    var $_errorNum = 0;
    var $_errorMsg = '';
    var $_resource = '';
    var $_cursor = null;
    var $_debug = 0;
    var $_limit = 0;
    var $_offset = 0;
    var $_ticker = 0;
    var $_log = null;
    var $_nullDate = null;
    var $_nameQuote = null;
    var $_quoted = null;
    var $_hasQuoted = null;

    function __construct() {

        $this->_ticker = 0;
        $this->_errorNum = 0;
        $this->_log = array();
        $this->_quoted = array();
        $this->_hasQuoted = false;

        // Register faked "destructor" in PHP4 to close all connections we might have made
        if (version_compare(PHP_VERSION, '5') == -1) {
            register_shutdown_function(array(&$this, '__destruct'));
        }
    }

    function &getInstance($configs, $database_type='mysql') {

        static $instances;

        $database_type = strtolower($database_type);

        if (!isset($instances)) {
            $instances = array();
        }

        $instance = null;

        if (empty($instances[$database_type])) {

            $dbClass = ucfirst($database_type);

            $instance = new $dbClass($configs);

            if (!is_object($instance))
                return 'Unable to load Database Driver:' . $database_type;

            if ($error = $instance->getErrorMsg()) {
                return $error;
            } else {
                $instances[$database_type] = & $instance;
            }
        }


        return $instances[$database_type];
    }

    function __destruct() {
        return true;
    }

    function connected() {
        return false;
    }

    function debug($level) {
        $this->_debug = intval($level);
    }

    function getErrorNum() {
        return $this->_errorNum;
    }

    function getErrorMsg($escaped = false) {
        if ($escaped) {
            return addslashes($this->_errorMsg);
        } else {
            return $this->_errorMsg;
        }
    }

    function getEscaped($text, $extra = false) {
        return;
    }

    function getLog() {
        return $this->_log;
    }

    function getTicker() {
        return $this->_ticker;
    }

    function nameQuote($s) {
        // Only quote if the name is not using dot-notation
        if (strpos($s, '.') === false) {
            $q = $this->_nameQuote;
            if (strlen($q) == 1) {
                return $q . $s . $q;
            } else {
                return $q{0} . $s . $q{1};
            }
        } else {
            return $s;
        }
    }

    function Quote($text, $escaped = true) {

        return '\'' . ($escaped ? $this->getEscaped($text) : $text) . '\'';
    }

    function addQuoted($quoted) {
        if (is_string($quoted)) {
            $this->_quoted[] = $quoted;
        } else {
            $this->_quoted = array_merge($this->_quoted, (array) $quoted);
        }
        $this->_hasQuoted = true;
    }

    function isQuoted($fieldName) {
        if ($this->_hasQuoted) {
            return in_array($fieldName, $this->_quoted);
        } else {
            return true;
        }
    }

    function getNullDate() {
        return $this->_nullDate;
    }

    function setQuery($sql, $offset = 0, $limit = 0) {
        $this->_sql = $sql;
   
        $this->_limit = (int) $limit;
        $this->_offset = (int) $offset;
    }

    function getQuery() {
        return $this->_sql;
    }

    function query($sql=null, $offset=0, $limit=0) {

        return;
    }

    function getNumRows($cur=null) {
        return;
    }

    /**
     * Get the affected rows by the most recent query
     *
     * @abstract
     * @access public
     * @return int The number of affected rows in the previous operation
     */
    function getAffectedRows() {
        return;
    }

    function loadResult($row=0, $field=null) {
        return;
    }

    function loadResultArray($numinarray = 0) {
        return;
    }

    function loadAssoc() {
        return;
    }

    function loadAssocList($key='') {
        return;
    }

    function loadObject() {
        return;
    }

    function loadObjectList($key='') {
        return;
    }

    function loadRow() {
        return;
    }

    function loadRowList($key='') {
        return;
    }

    function stderr($showSQL = false) {
        if ($this->_errorNum != 0) {
            return "DB function failed with error number $this->_errorNum"
            . "<br /><font color=\"red\">$this->_errorMsg</font>"
            . ($showSQL ? "<br />SQL = <pre>$this->_sql</pre>" : '');
        } else {
            return "DB function reports no errors";
        }
    }

    function insertid() {
        return;
    }

}
