<?php

/**
 * MySQL database driver
 */
class Mysql extends Database {

    var $name = 'mysql';
    var $_nullDate = '0000-00-00 00:00:00';
    var $_nameQuote = '`';

    function __construct($configs) {
        $host = $configs['host'];
        $user = $configs['username'];
        $password = $configs['password'];
        $database = $configs['database'];
        // perform a number of fatality checks, then return gracefully
        if (!function_exists('mysql_connect')) {
            $this->_errorNum = 1;
            $this->_errorMsg = 'The MySQL adapter "mysql" is not available.';
            return;
        }

        if (!($this->_resource = @mysql_connect($host, $user, $password, true))) {
            $this->_errorNum = 2;
            $this->_errorMsg = 'Could not connect to MySQL';

            return;
        }

        // finalize initialization
        parent::__construct();

        // select the database
        if ($database) {
            $this->select($database);
        }

    }

    function __destruct() {
        $return = false;
        if (is_resource($this->_resource)) {
            $return = mysql_close($this->_resource);
        }
        return $return;
    }

    function connected() {
        if (is_resource($this->_resource)) {
            return mysql_ping($this->_resource);
        }
        return false;
    }

    function select($database) {
        if (!$database) {
            return false;
        }

        if (!mysql_select_db($database, $this->_resource)) {
            $this->_errorNum = 3;
            $this->_errorMsg = 'Could not connect to database';
            return false;
        }

        // if running mysql 5, set sql-mode to mysql40 - thereby circumventing strict mode problems
        if (strpos($this->getVersion(), '5') === 0) {
            $this->setQuery("SET sql_mode = 'MYSQL40'");
            $this->query();
        }

        return true;
    }

    /**
     * Determines UTF support
     *
     * @access	public
     * @return boolean True - UTF is supported
     */
    function hasUTF() {
        $verParts = explode('.', $this->getVersion());
        return ($verParts[0] == 5 || ($verParts[0] == 4 && $verParts[1] == 1 && (int) $verParts[2] >= 2));
    }

    /**
     * Custom settings for UTF support
     *
     * @access	public
     */
    function setUTF() {
        mysql_query("SET NAMES 'utf8'", $this->_resource);
    }

    /**
     * Get a database escaped string
     *
     * @param	string	The string to be escaped
     * @param	boolean	Optional parameter to provide extra escaping
     * @return	string
     * @access	public
     * @abstract
     */
    function getEscaped($text, $extra = false) {
        $result = mysql_real_escape_string($text, $this->_resource);
        if ($extra) {
            $result = addcslashes($result, '%_');
        }
        return $result;
    }
    /**
     *
     * @param string $sql
     * @param int $offset
     * @param int $limit
     * @return resource
     */
    function query($sql=null, $offset=0, $limit=0) {

        if (!is_resource($this->_resource)) {

            return false;
        }

        if (!empty($sql)) {
            $this->setQuery($sql, $offset, $limit);
        }
        $sql = $this->_sql;
        
        if ($this->_limit > 0 || $this->_offset > 0) {
            $sql .= ' LIMIT ' . $this->_offset . ', ' . $this->_limit;
            //echo $sql;
        }
        if ($this->_debug) {
            $this->_ticker++;
            $this->_log[] = $this->_sql;
        }
        $this->_errorNum = 0;
        $this->_errorMsg = '';

        $this->_cursor = mysql_query($sql, $this->_resource);
        if (!$this->_cursor) {
            $this->_errorNum = mysql_errno($this->_resource);
            $this->_errorMsg = mysql_error($this->_resource) . " SQL=$sql";

            if ($this->_debug) {

            }
            return false;
        }
        return $this->_cursor;
    }

    function getNumRows($cur=null) {
        return mysql_num_rows($cur ? $cur : $this->_cursor);
    }

    /**
     * Description
     *
     * @access	public
     * @return int The number of affected rows in the previous operation
     */
    function getAffectedRows() {
        return mysql_affected_rows($this->_resource);
    }

    function loadResult($field=null) {

        if (!($cur = $this->query())) {
            return null;
        }

        $ret = null;
        if ($field !== null) {

            if ($row=$this->loadAssoc() && array_key_exists($field, $row))
                $ret = $row[$field];
        }
        else {

            if ($row=$this->loadRow())
                $ret = $row[0];
        }

        mysql_free_result($cur);
        return $ret;
    }

    function loadResultArray($numinarray = 0) {
        if (!($cur = $this->query())) {
            return null;
        }
        $array = array();
        while ($row = mysql_fetch_row($cur)) {
            $array[] = $row[$numinarray];
        }
        mysql_free_result($cur);
        return $array;
    }

    function loadAssoc() {
        if (!($cur = $this->query())) {
            return null;
        }
        $ret = null;
        if ($array = mysql_fetch_assoc($cur)) {
            $ret = $array;
        }
        mysql_free_result($cur);
        return $ret;
    }

    function loadAssocList($key='') {
        if (!($cur = $this->query())) {
            return null;
        }
        $array = array();
        while ($row = mysql_fetch_assoc($cur)) {
            if ($key) {
                $array[$row[$key]] = $row;
            } else {
                $array[] = $row;
            }
        }
        mysql_free_result($cur);
        return $array;
    }

    function loadObject() {
        if (!($cur = $this->query())) {

            return null;
        }
        $ret = null;
        if ($object = mysql_fetch_object($cur)) {
            $ret = $object;
        }
        mysql_free_result($cur);
        return $ret;
    }

    function loadObjectList($key='') {

        if (!($cur = $this->query())) {
            return null;
        }
        $array = array();
        while ($row = mysql_fetch_object($cur)) {
            if ($key) {
                $array[$row->$key] = $row;
            } else {
                $array[] = $row;
            }
        }
        mysql_free_result($cur);

        return $array;
    }

    function loadRow() {

        if (!($cur = $this->query())) {

            return null;
        }

        $ret = null;
        if ($row = mysql_fetch_row($cur)) {
            $ret = $row;
        }
        mysql_free_result($cur);
        return $ret;
    }

    function loadRowList($key=null) {
        if (!($cur = $this->query())) {
            return null;
        }
        $array = array();
        while ($row = mysql_fetch_row($cur)) {
            if ($key !== null) {
                $array[$row[$key]] = $row;
            } else {
                $array[] = $row;
            }
        }
        mysql_free_result($cur);
        return $array;
    }

    function getVersion() {
        return mysql_get_server_info($this->_resource);
    }

    function insertid() {
        return mysql_insert_id($this->_resource);
    }

    /**
     * Inserts a row into a table based on an objects properties
     *
     * @access	public
     * @param	string	The name of the table
     * @param	object	An object whose properties match table fields
     * @param	string	The name of the primary key. If provided the object property is updated.
     */
    function insertObject($table, &$object, $keyName = NULL) {
        $fmtsql = 'INSERT INTO ' . $this->nameQuote($table) . ' ( %s ) VALUES ( %s ) ';
        $fields = array();
        foreach (get_object_vars($object) as $k => $v) {
            if (is_array($v) or is_object($v) or $v === NULL) {
                continue;
            }
            if ($k[0] == '_') { // internal field
                continue;
            }
            $fields[] = $this->nameQuote($k);
            $values[] = $this->isQuoted($k) ? $this->Quote($v) : (int) $v;
        }

        $this->setQuery(sprintf($fmtsql, implode(",", $fields), implode(",", $values)));
        if (!$this->query()) {
            return false;
        }
        $id = $this->insertid();
        if ($keyName && $id) {
            $object->$keyName = $id;
        }
        return true;
    }

    /**
     * Description
     *
     * @access public
     * @param [type] $updateNulls
     */
    function updateObject($table, &$object, $keyName, $updateNulls=true) {
        $fmtsql = 'UPDATE ' . $this->nameQuote($table) . ' SET %s WHERE %s';
        $tmp = array();
        foreach (get_object_vars($object) as $k => $v) {
            if (is_array($v) or is_object($v) or $k[0] == '_') { // internal or NA field
                continue;
            }
            if ($k == $keyName) { // PK not to be updated
                $where = $keyName . '=' . $this->Quote($v);
                continue;
            }
            if ($v === null) {
                if ($updateNulls) {
                    $val = 'NULL';
                } else {
                    continue;
                }
            } else {
                $val = $this->isQuoted($k) ? $this->Quote($v) : (int) $v;
            }
            $tmp[] = $this->nameQuote($k) . '=' . $val;
        }
        $this->setQuery(sprintf($fmtsql, implode(",", $tmp), $where));
        
        return $this->query();
    }
    

}
