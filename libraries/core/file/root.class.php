<?php

/**

 */
class RootObject {

    var $_errors = array();

    function RootObject() {
        $args = func_get_args();
        call_user_func_array(array(&$this, '__construct'), $args);
    }

    function __construct() {

    }

    function __destruct() {

    }

    function __set($property, $value) {

        // if (isset($this->$property))

        $this->$property = $value;
    }

    function __get($property) {

        return $this->$property;
    }

    function __isset($property) {

    }

    function __unset($property) {

    }

    function __call($methodname, $arguments) {

        if (!method_exists($this, $method_name)) {

            $this->set('error', get_class($this) . " :: " . $methodname . " is not existed!");
        }
    }

    function get($property, $default=null) {
        if (isset($this->$property)) {
            return $this->$property;
        }
        return $default;
    }

    function getProperties($public = true) {
        $vars = get_object_vars($this);

        if ($public) {
            foreach ($vars as $key => $value) {
                if ('_' == substr($key, 0, 1)) {
                    unset($vars[$key]);
                }
            }
        }

        return $vars;
    }

    function getError($i = null, $toString = true) {
        // Find the error
        if ($i === null) {
            // Default, return the last message
            $error = end($this->_errors);
        } else
        if (!array_key_exists($i, $this->_errors)) {
            // If $i has been specified but does not exist, return false
            return false;
        } else {
            $error = $this->_errors[$i];
        }

        // Check if only the string is requested
        if (JError::isError($error) && $toString) {
            return $error->toString();
        }

        return $error;
    }

    function getErrors() {
        return $this->_errors;
    }

    function set($property, $value = null) {
        $previous = isset($this->$property) ? $this->$property : null;
        $this->$property = $value;
       
        return $previous;
    }

    //Set an arrray or an object to properties
    function setProperties($properties) {
        $properties = (array) $properties; //cast to an array

        if (is_array($properties)) {
            foreach ($properties as $k => $v) {
                $this->$k = $v;
            }

            return true;
        }

        return false;
    }

    function setError($error) {
        array_push($this->_errors, $error);
    }

    function toString() {
        return get_class($this);
    }

    function render($file, $default='default') {

        ob_start();
        $html = "";
        if (file_exists($file)) {
            include_once($file);  // Include the file
        } else if (file_exists($default)) {
            include_once($default);  // Include the file
        }
        $html = ob_get_contents(); // Get the contents of the buffer
        ob_end_clean();                // End buffering and discard

        return $html;
    }

    function getTime() {

        return time();
    }

    function hasProperty($property) {

        return property_exists($this, $property);
    }
    function debug() {
        echo get_class($this);
    }
}
