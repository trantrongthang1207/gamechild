<?php

/*
  Document   : file.class.php
  Created on : March 4, 2010, 11:47:51 PM
  Author     : Khanh Huy
  Email      : khanhhuyna@gmail.com
  Description:
  A File handling class
 */

if (!defined('DS'))
    define('DS', DIRECTORY_SEPARATOR);

require_once 'path.class.php';

class File extends RootObject {
    /**
     *
     * @param <type> $file
     * @return <type>
     */
    function getExt($file) {
        return substr($file, strrpos($file, '.') + 1);
    }
    /**
     *
     * @param <type> $file
     * @return <type>
     */

    function stripExt($file) {
        return preg_replace('#\.[^.]*$#', '', $file);
    }
    /**
     *
     * @param <type> $file
     * @return <type>
     */
    function makeSafe($file) {
        $regex = array('#(\.){2,}#', '#[^A-Za-z0-9\.\_\- ]#', '#^\.#');
        return preg_replace($regex, '', $file);
    }
    /**
     *
     * @param <type> $src
     * @param <type> $dest
     * @param <type> $path
     * @return <type>
     */
    function copy($src, $dest, $path = null) {
         if (self::exists($src))
             return copy($src, $dst);
         else
             return false;
    }
    /**
     *
     * @param <type> $file
     * @return <type>
     */
    function delete($file) {
        return unlink($file);
    }

    /**
     *
     * @param <type> $src
     * @param <type> $dest
     * @param <type> $path
     */

    function move($src, $dest, $path = '') {

    }
    /**
     *
     * @param <type> $filename
     * @param <type> $incpath
     * @param <type> $amount
     * @param <type> $chunksize
     * @param <type> $offset
     */
    function read($filename, $incpath = false, $amount = 0, $chunksize = 8192, $offset = 0) {

    }
    /**
     *
     * @param <type> $file
     * @param <type> $buffer
     */
    function write($file, $buffer) {

    }

    /**
     *
     * @param <type> $upload_fieldname
     * @param <type> $dest
     * @return <type>
     */
    function upload($upload_fieldname, $dest) {
        if ($_FILES[$upload_fieldname]['error'] == UPLOAD_ERR_OK) {
            return move_uploaded_file($_FILES[$upload_fieldname]['tmp_name'], $dest);
            echo "here";
        }
        else {
            return false;
        }
    }
    /**
     *
     * @param <type> $file
     * @return <type>
     */
    function exists($file) {
        return is_file(Path::clean($file));
    }
    /**
     *
     * @param <type> $file
     * @return <type>
     */
    function getName($file) {
        $slash = strrpos($file, DS);
        if ($slash !== false) {
            return substr($file, $slash + 1);
        } else {
            return $file;
        }
    }

}
