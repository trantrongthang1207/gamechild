<?php

class storedSession_table extends table {
		
	var $sessionid=NULL;
	
	var $userid=NULL;
	
	var $username='guest';
	
	var $usertype='guest';
	
	var $data='';
	
	var $startTime='';
	
	var $description='';
	
	
	
	function __construct(&$db) {
	
		parent::__construct('sessions','sessionid',$db);
				
	}
	
	function __destruct() {
	
		parent::__destruct();
		
	}
	
	function load($sessionId=NULL) {
				
		return parent::load($sessionId);
		
	}
	
	function store() {
		
		return parent::store();
		
	}
	
	function getSessionId() {
	
		return $this->sessionid;
		
	}
	
	function getUserId() {
	
		return $this->userid;
		
	}
	
	function getUserType() {
		return $this->usertype;
	}
	
	function getStartTime() {
	
		return $this->startTime;
		
	}
	
	function getData() {
	
		return $this->data;
		
	}
	
	function getDescription() {
	
		return $this->description;
		
	}
	
	function getUserName() {
		
		return $this->username;
		
	}
	
	function setSessionId($sessionId=NULL) {
	
		$this->sessionid=$sessionId;
		
	}
	
	function setUserId($userid=NULL) {
	
		$this->userid=$userid;
	}
	
	function setUserName($username='guest') {
	
		$this->username=$username;
		
	}
	
	function setUserType($usertype='guest') {
		
		$this->usertype=$usertype;
		
	}
	
	function setStartTime($startTime=NULL) {
	
		$this->startTime=$startTime;
		
	}
	
	function setData($data=NULL) {
	
		$this->data=$data;
		
	}
	
	
	
	function setDescription($description='') {
	
		$this->description=$description;
		
	}
		
	function newSession($sessionId=NULL, $userid=NULL, $data='', $description='guest') {
	
		if (sessionId) {
						
			$this->sessionid=$sessionId;
			
			//$this->userid=$userid;
			
			//$this->username="guest";
			
			$this->data=$data;
					
			$this->startTime=$this->getTime();
			
			$this->description=$description;	
			
			/*
			$user=new user($this->_db);
			
			if ($user->load(userid))
				$this->userType=$user->get('usertype');
			else 
				$this->userType=NULL;
			*/
			
			return $this->_db->insertObject('sessions',$this);
			
		}
		else 
			return false;	
	}
	
	
	function deleteSession() {
	
		if (parent::delete($this->sessionid))
		{		
			
			$this->sessionid=NULL;
		
			$this->userid=NULL;
			
			$this->username='guest';
			
			$this->usertype='guest';
			
			$this->data='';
			
			$this->startTime='';
			
			$this->description='';
			
			return true;

		}
		else
		
			return false;
	}
	
	function userLoginCheck($userid) {
		
		$arrCondition=array('userid'=>$userid);
		
		return $this->loadObjectList($arrCondition) ? true : false;
		
			
	}
	
	function loadFromUserid($userId=NULL) {
		
		$db=$this->_db;	
			
		$sql="SELECT * FROM ".$db->nameQuote($userId). " WHERE ".$db->nameQuote('userid')."=".$db->Quote($userId);
		
		$db->setQuery($sql);
				
		if ($result = $db->loadAssoc( )) {
		
			return $this->bind($result);
			
		}
		else
		{
			$this->setError( $db->getErrorMsg() );
			return false;
		}
		
	}
	
	function deleteSessionFromUserId($userId=NULL) {
		
		$db=$this->_db;	
			
		$sql="DELETE FROM ".$db->nameQuote($userId). " WHERE ".$db->nameQuote('userid')."=".$db->Quote($userId);
		
		$db->setQuery($sql);
			
		if ($result = $db->query( )) {
		
			return true;
			
		}
		else
		{
			$this->setError( $db->getErrorMsg() );
			
			return false;
		}
		
	}
	
}
?>