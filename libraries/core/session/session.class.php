<?php

class Session extends RootObject {

    //State active/ destroyed / expired /restart
    var $_state = 'active';
    //Session life in minutes
    var $_expire = 15;
    


    function __construct($store = 'none', $options = array()) {
      
        ini_set('session.save_handler', 'files');

        ini_set('session.use_trans_sid', '1');

        $this->_start();

        $this->_setCounter();

        $this->_setTimers();

        //print_r($this);
    }

    function __destruct() {
        $this->close();
    }

    function getState() {
        return $this->_state;
    }

    function getExpire() {
        return $this->_expire;
    }

    function getSessionName() {
        if ($this->_state === 'destroyed') {
            // @TODO : raise error
            return null;
        }
        return session_name();
    }
    
    function isNew() {
        $counter = $this->get('session.counter');

        if ($counter === 1) {
            return true;
        }
        return false;
    }

    function getId() {
        if ($this->_state === 'destroyed') {
            // @TODO : raise error
            return null;
        }
        return session_id();
    }

    function &get($name, $default = null, $namespace = 'default') {
        $namespace = '__' . $namespace; //add prefix to namespace to avoid collisions

        if ($this->_state !== 'active' && $this->_state !== 'expired') {
            // @TODO :: generated error here
            $error = null;
            return $error;
        }

        if (isset($_SESSION[$namespace][$name])) {
            return $_SESSION[$namespace][$name];
        }
        return $default;
    }

    function set($name, $value, $namespace = 'default') {
        $namespace = '__' . $namespace; //add prefix to namespace to avoid collisions

        if ($this->_state !== 'active') {
            // @TODO :: generated error here
            return null;
        }

        $old = isset($_SESSION[$namespace][$name]) ? $_SESSION[$namespace][$name] : null;

        if (null === $value) {
            unset($_SESSION[$namespace][$name]);
        } else {
            $_SESSION[$namespace][$name] = $value;
        }

        return $old;
    }

    function has($name, $namespace = 'default') {
        $namespace = '__' . $namespace; //add prefix to namespace to avoid collisions

        if ($this->_state !== 'active') {
            // @TODO :: generated error here
            return null;
        }

        return isset($_SESSION[$namespace][$name]);
    }

    function clear($name, $namespace = 'default') {
        $namespace = '__' . $namespace; //add prefix to namespace to avoid collisions

        if ($this->_state !== 'active') {
            // @TODO :: generated error here
            return null;
        }

        $value = null;
        if (isset($_SESSION[$namespace][$name])) {
            $value = $_SESSION[$namespace][$name];
            unset($_SESSION[$namespace][$name]);
        }

        return $value;
    }

    function _start() {
        //  start session if not started
        if ($this->_state == 'restart') {

            session_id($this->_createId());
        }

        session_cache_limiter('none');

        session_start();

        $this->_state = 'active';
        // Send modified header for IE 6.0 Security Policy
        header('P3P: CP="NOI ADM DEV PSAi COM NAV OUR OTRo STP IND DEM"');

        return true;
    }

    function destroy() {
        // session was already destroyed
        if ($this->_state === 'destroyed') {
            return true;
        }

        // In order to kill the session altogether, like to log the user out, the session id
        // must also be unset. If a cookie is used to propagate the session id (default behavior),
        // then the session cookie must be deleted.
        if (isset($_COOKIE[session_name()])) {
            setcookie(session_name(), '', time() - 100000, '/');
        }

        session_unset();
        session_destroy();

        $this->_state = 'destroyed';
        return true;
    }

    function restart() {
        $this->destroy();
        if ($this->_state !== 'destroyed') {
            // @TODO :: generated error here
            return false;
        }

        $this->_state = 'restart';

        $this->_start();

        $this->_setCounter();

        return true;
    }

    function close() {
        session_write_close();
    }

    function _createId($sessionID=NULL) {

        $id = $sessionID ? $sessionID : 0;
        if (!$id) {
            while (strlen($id) < 32) {
                $id .= mt_rand(0, mt_getrandmax());
            }

            $id = md5(uniqid($id, true));
        }
        return $id;
    }

    function _createToken($length = 32) {
        static $chars = '0123456789abcdef';
        $max = strlen($chars) - 1;
        $token = '';
        $name = session_name();
        for ($i = 0; $i < $length; ++$i) {
            $token .= $chars[(rand(0, $max))];
        }

        return md5($token . $name);
    }

    function _setCounter() {
        $counter = $this->get('session.counter', 0);
        ++$counter;

        $this->set('session.counter', $counter);
        return true;
    }

    function _setTimers() {
        if (!$this->has('session.timer.start')) {
            $start = time();

            $this->set('session.timer.start', $start);
            $this->set('session.timer.last', $start);
            $this->set('session.timer.now', $start);
        }

        $this->set('session.timer.last', $this->get('session.timer.now'));
        $this->set('session.timer.now', time());

        return true;
    }
   
    function clearNamespace($namespace='default') {
        unset($_SESSION[self::getRealNamespace($namespace)]);
    }
    function getRealNamespace($namespace='default') {
        return $namespace = '__' . $namespace;
    }
}

?>