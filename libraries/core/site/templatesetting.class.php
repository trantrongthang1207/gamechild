
<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class TemplateSetting extends Setting {

    var $widgets;

    function __construct($templateName=null) {

        if (!empty($templateName)) {
            parent::__construct(PATH_SITE_SETTINGS . 'templates' . DS . $templateName . ".xml");

            $this->_loadWidgets();
        }
    }

 
    function _loadWidgets() {
        $widget = array();
        if ($blocks = $this->get('/template/blocks/block')) {

            foreach ($blocks as $block) {

                if ($widget_ids = $block->xpath('widget/id')) {
                    foreach ($widget_ids as $widget_id) {
                        $blockName = $this->validate($block['name']);
                        $widgets[$blockName][] = $this->validate($widget_id);
                    }
                }
            }
        }
        $this->widgets = $widgets;
      
    }

    function getWidgets() {
        return $this->widgets;
    }

    function getWidget($blockName) {
    
        return $this->widgets[$blockName];
    }

}

?>
