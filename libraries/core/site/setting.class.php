<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Setting extends RootObject {
    /**
     *
     * @var SimpleXMLElement
     */
    var $xml = null;
    var $xmlFile = '';

    function __construct($xmlFile=null) {

        if (file_exists($xmlFile)) {
            $this->xmlFile = $xmlFile;
            $this->load();
        }
    }

    function setXmlFile($xmlFile=null) {
        if (!empty($xmlFile)) {
            $this->xmlFile = $xmlFile;
        }
    }

    function get($path) {
        if (!empty($this->xml))
            return $this->xml->xpath ($path);
        else
            return null;
    }
    function getAttribute($path, $name,  $default) {
        $attributes=array();

        if (!empty($this->xml)) {
            if ($nodeList=$this->xml->xpath ($path)) {
                foreach ($nodeList as $node) {
                    $attributes[]=$node[$name];
                }
            }
        }
       return $attributes;
    }

    function set() {
        
    }
    /**
     *
     * @param <type> $path
     * @param <type> $default
     * @return <type>
     */
    function getNode($path, $default='') {
        if ($nodes = $this->get($path))
            return $nodes[0];
        else
            return $default;
    }
    /**
     *
     * @param <type> $path
     * @param <type> $default
     * @return <type>
     */
     function getNodeValue($path, $default='') {
        return $this->validate($this->getNode($path, $default));
    }
    /**
     *
     * @param <type> $xmlFile 
     */
    function load($xmlFile=null) {
        $this->xml = null;
        if (!empty($xmlFile)) {
            $this->xmlFile = $xmlFile;
        }

        if (!empty($this->xmlFile)) {
            $this->xml = simplexml_load_file($this->xmlFile);
        }
    }
    /**
     *
     * @param <type> $xmlFileName 
     */
    function save($xmlFileName) {
        
    }
    /**
     *
     * @param <type> $value
     * @return <type> 
     */
    function getValue($value) {
        return trim((string) $value);
    }
    /**
     *
     * @param <type> $value
     * @return <type> 
     */
    function validate($value) {
        return trim((string) $value);
    }
    

}

?>
