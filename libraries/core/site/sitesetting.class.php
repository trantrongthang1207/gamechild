<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class SiteSetting extends Setting {

    function __construct($name=null) {

        if (!empty($name)) {
            parent::__construct(PATH_SITE_SETTINGS . $name . ".xml");
        }
    }

    function getTemplate($default='default') {
        return $this->getNodeValue('/application/site/template', $default);
    }

    function getName($default='') {
        return $this->getNodeValue('/application/site/name', $default);
    }

    function getTitle($default='') {
        return $this->getNodeValue('/application/site/title', $default);
    }

    function getKeywords($default='') {
        return $this->getNodeValue('/application/site/keywords', $default);
    }
     function getDescription($default='') {
        return $this->getNodeValue('/application/site/descriptions', $default);
    }

    function getFrontPage($default='frontpage') {
        return $this->getNodeValue('/application/structure/site/frontpage', $default);
    }

    function getErrorPage($default='error') {
        return $this->getNodeValue('/application/structure/site/errorpage', $default);
    }

    function getWidgets($page_id=null) {
        static $widgets;

        if (!isset($widgets[$page_id]) && $page_id !== null && $pages = $this->get('/application/structure/pages/page')) {

            $widgets[$page_id] = array();

            foreach ($pages as $page) {

                if ($page['id'] == $page_id && $widgetNodes = $page->widgets->widget) {
                    foreach ($widgetNodes as $widget) {
                        $widgets[$page_id][] = $this->validate($widget);
                    }
                }
            }
        }

        return $widgets[$page_id];
    }

    function getWidget($widget_id) {
        $temp = null;
        if ($widgets = $this->get('/application/structure/site/widgets/widget')) {
            foreach ($widgets as $widget) {
                if ($this->validate($widget['id']) == $widget_id) {
                    $temp = $widget;
                    break;
                    ;
                }
            }
        }
        $widget = array();
        if ($temp) {

            $widget['name'] = $this->validate($temp->code);
            $widget['title'] = $this->validate($temp->title);
            $widget['params'] = array();
            if ($params = $temp->params->param) {
                foreach ($params as $param) {
                    $widget['params'][$this->validate($param['name'])] = $this->validate($param);
                }
            }
        }
        return $widget;
    }

    function getPageId($appName) {
        static $pageIds;
        if ($pages = $this->get('/application/structure/pages/page')) {
            foreach ($pages as $page) {
                if ($appCodes = $page->app->code) {
                    foreach ($appCodes as $code) {
                        if ($this->validate($code) == $appName) {

                            return $this->validate($page['id']);
                        }
                    }
                }
            }
        }
        return null;
    }

    function getAppName($pageId) {

        if ($pages = $this->get('/application/structure/pages/page')) {

            foreach ($pages as $page) {

                if ($this->validate($page['id']) == (string) $pageId) {

                    return $this->validate($page->app->code);
                }
            }
        }

        return null;
    }
     function getPageTitle($pageId) {

        if ($pages = $this->get('/application/structure/pages/page')) {

            foreach ($pages as $page) {

                if ($this->validate($page['id']) == (string) $pageId) {

                    return $this->validate($page->title);
                }
            }
        }

        return null;
    }

    function getApp($pageId) {
        $app = array();
        if ($pages = $this->get('/application/structure/pages/page')) {

            foreach ($pages as $page) {

                if ((string) $page['id'] == (string) $pageId) {
                    $app['name'] = $this->validate($page->app->code);
                    $app['params'] = array();
                    if ($params = $page->params->param) {
                        foreach ($params as $param) {
                            $app['params'][$this->validate($param['name'])] = $this->validate($param);
                        }
                    }
                    $app['requests'] = array();
                    if ($requests = $page->app->requests->request) {
                        foreach ($requests as $request) {
                            $app['requests'][$this->validate($request['name'])] = $this->validate($request);
                        }
                    }
                
                }
            }
        }
        return $app;
    }
    function getRequests($pageId) {
       if ($app=$this->getApp($pageId)) {
           return $app['requests'];
       }
    }

}

?>
