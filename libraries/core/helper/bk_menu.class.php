<?php

class menu extends RootObject {

    var $id = NULL;
    var $title = '';
    var $menuitems = array();
    var $menuitemsHtml = '';
    var $html = '';
    var $type = 'tree'; //'tree'|'flat'

    function __construct($menu_id=NULL, $type='tree') {

        $this->id = $menu_id;

        $this->type = $type;

        parent::__construct();
    }

    function __destruct() {

        parent::__destruct();
    }

    /*     * Params $type='tree'|'flat'
     */

    function reset() {

        $this->menuitems = NULL;

        $this->html = '';

        $this->menuitemsHtml = '';
    }

    function getItems() {

        global $db;

        include_once "database" . DS . "menus.table.class.php";

        $menu = new menus_table($db);

        if (!$menu->load($this->id))
            return false;

        $this->title = $menu->get('title');

        $sql = "SELECT * FROM " . $db->nameQuote('menuitems');
        $sql.=" WHERE " . $db->nameQuote('menu_id') . "=" . $db->Quote($this->id);
        $sql.=" ORDER BY " . $db->nameQuote('level') . ", " . $db->nameQuote('parent') . ", " . $db->nameQuote('ordering');

        $db->setQuery($sql);

        $itemsList = $db->loadObjectList();

        switch ($this->type) {

            case 'flat':

                $this->_createFlat($itemsList);

                break;

            case 'tree':

            default:

                include_once CFG_LIBRARIES_PATH . DS . "helper" . DS . "menuitem.class.php";

                $menuitem = new menuitem();

                $this->_createTree($itemsList, $menuitem);

                $this->menuitems = $menuitem->getChilds();

                break;
        }

        return true;
    }

    /* Params
     * * object|array menuitems
     * * (object menutiems) $menuitem_parent_id

     */

    function _createTree(&$menuItems, $menuitem_parent=NULL) {

        include_once CFG_LIBRARIES_PATH . DS . "helper" . DS . "menuitem.class.php";

        $parent_id = $menuitem_parent ? $menuitem_parent->get('id') : 0;

        $menuitem = new menuitem();

        if ($menuItems) {

            foreach ($menuItems as $key => $item) {

                if ($item->parent == $parent_id) {

                    unset($menuItems[$key]);

                    $menuitem = new menuitem($item);

                    $this->_createTree($menuItems, $menuitem);

                    $menuitem_parent->addChild($menuitem);
                }
            }
        }
    }

    /* Params
     * * object|array menuitems
     * * (object menutiems) $menuitem_parent

     */

    function _createFlat(&$menuItems, $parent_id=0) {

        include_once CFG_LIBRARIES_PATH . DS . "helper" . DS . "menuitem.class.php";

        $menuitem = new menuitem();

        if ($menuItems) {

            foreach ($menuItems as $key => $item) {

                if ($item->parent == $parent_id) {

                    unset($menuItems[$key]);

                    $menuitem = new menuitem($item);

                    $this->menuitems[] = $menuitem;

                    $this->_createFlat($menuItems, $menuitem->get('id'));
                }
            }
        }
    }

    /* Params $type='list'| 'select'
     */

    function render($type='list', $html_id=NULL, $name=NULL, $className=NULL, $moreHtmlParams=NULL) {

        if ($html_id)
            $id = "id='$html_id'";

        if ($name)
            $name = "name='$name'";

        if ($className)
            $class = "class='$className'";

        include_once CFG_LIBRARIES_PATH . DS . "helper" . DS . "menuitem.class.php";

        $html = '';

        switch ($type) {

            case 'list':

                foreach ($this->menuitems as $item) {

                    $tempHTML = '';

                    $class = '';

                    //$html.=$item->createMenuLayout();

                    if ($childs = $item->getChilds()) {

                        $temp = new menu();

                        $temp->menuitems = $childs;

                        $temp->render('list');

                        $tempHTML.=$temp->getHTML();

                        $class = 'parent';
                    }

                    $item->render('link');

                    $html.="<li class='$class' >" . $item->getHTML() . $tempHTML . "</li>";
                }

                $this->menuitemsHtml = $html;

                if ($html)
                    $html = "<ul $id $name $className $moreHtmlParams >" . $html . "</ul";

                break;

            case 'select':

                foreach ($this->menuitems as $item) {

                    $tempHTML = '';

                    if ($childs = $item->getChilds()) {

                        $temp = new menu();

                        $temp->menuitems = $childs;

                        $temp->render($type);

                        $tempHTML.=$temp->getHTML();
                    }

                    $item->render('option', ' ');

                    $html.=$item->getHTML() . $tempHTML;
                }

                $this->menuitemsHtml = $html;

                if ($html)
                    $html = "<select  id='$html_id'  name='$name' class='$className'  $moreHtmlParams >" . $html . "</select>";

                break;

            default:

                break;
        }

        $this->html = $html;
    }

    function getHTML() {

        return $this->html;
    }

}

?>
