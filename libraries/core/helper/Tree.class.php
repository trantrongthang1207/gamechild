<?php

class Tree extends RootObject {
	
	var $nodeClass="Node";

	var $rootNode;
		
	var $html;
	
	function __construct($rootNode=NULL) {
	
		if ($rootNode)
			
			$this->rootNode=$rootNode;
				
		parent::__construct();
		
	}
	
	function __desctruct() {
	
		parent::__destruct();
		
	}
	
	function reset() {
	
		$this->rootNode=NULL;
	
	}
	
	function getHTML() {
		
		return $this->html;
		
	}
	
	function setNodeClass($className) {
	
		$this->nodeClass=$className;
		
	}
	
	function _getChildNodes($parentNode) {
	
		
	}
	
	function initialize($rootNode) {
					
		$this->_getChildNodes($rootNode);
		
		$this->rootNode=$rootNode;
		
	}
	
	//params $newType='flat'
	
	function convert($newType='flat') {
		
		$returnData=NULL;
	
		switch ($newType) {
		
			case 'flat':
			
					$returnData=$this->rootNode;
								
					$this->_convertToFlatArray($retunData, $this->rootNode);
			
				break;
				
			default:
				
				break;
				
		}
		
		return $returnData;
		
	}
	
	function _convertToFlatArray(&$returnData, $node) {
	
		$arr=array();
		
		$childNodes=$node->getChild();
		
		if ($childNodes) {
			
			foreach ($childNodes as $node) {
			
				$returnData[]=$node;
			
				$this->_convertToFlatArray($node);
			
			}
		}
		
	}
	
	//Params:$type='list'|'select'
	
	function render($type='list', &$arrHtmlAttr) {// $html_id=NULL, $html_class=NULL, $html_name=NULL) {
	
		if ($arrHtmlAttr['id'])
			
			$id="id='".$arrHtmlAttr['id']."'";
		
		if ($arrHtmlAttr['name']) 
			
			$name="name='".$arrHtmlAttr['name']."'";
			
		if ($arrHtmlAttr['class'])
		
			$class="class='".$arrHtmlAttr['class']."'";
			
		if ($arrHtmlAttr['size'])
		
			$size="size='".$arrHtmlAttr['size']."'";
			
		$html='';	
	
		switch ($type) {
		
			case 'list':
			
					$html=$this->list_render($this->rootNode);	
					
					if ($html)
					
						$html="<ul $id $class $name >".$html."</ul>";	
			
				break;
				
			case 'select':
								
					$html=$this->select_render($this->rootNode);
					
					$html="<select $id $class $name $size >".$preString.$html."</select>";

										
				break;	
				
			default:
			
					
		}	
		
		$this->html=$html;	
		
	
	}
	
	function list_render($node) {
	
		if (!$node)
		
			return NULL;
	
		$html='';
				
		$node->render('a');
		
		$childsHTML='';
		
		if ($childNodes=$node->getChilds()) {
		
			foreach ($childNodes as $childNode) {
			
				$childsHTML.=$this->list_render($childNode);
				
			}
		}	
		
		if ($childsHTML)
		
			$childsHTML="<ul>$childsHTML</ul>";	
			
		if ($node->get('key'))
		
			$html='<li>'.$node->getHTML().$childsHTML.'</li>';
		
		else	
			
			$html.=$childsHTML;	
		
		return $html;	
		
	}
	
	function select_render($node, $preString="") {
	
		if (!$node)
		
			return NULL;
		
		$nodeTitle=$node->get('title');
		
		$node->set('title', $preString.$nodeTitle);	
					
		$node->render('option');
		
		$html=$node->getHTML();
		
		$childsHTML='';
	
		if ($childNodes=$node->getChilds())  {
						
			foreach ($childNodes as $childNode) {
			
				$childsHTML.=$this->select_render($childNode, $preString."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
				
			}	
		
		}
		
		$html.=$childsHTML;
		
		$node->set('title',$nodeTitle);	
		
		return $html;	
		
	}
	
	function layout() {
	
		echo $this->html;
	}
	
	function typeCastingNode($node) {
	
		if (get_class($node)==$this->className)
		
			return NULL;
	
		$className=$this->className;
	
		$tempNode=new $className($this->rootNode);
		
		$childNodes=$node->getChilds();
		
		if ($childNodess) {
		
			foreach ($childNodes as $childNode) {
			
				$newChildNode=$this->typeCastingNode($childNode);
				
				if (!$newChildNode)
					
					return NULL;
				
				$tempNode->addChild($newChildNode);
				
			}
		}
			
		return $tempNode;
		
	}
	
	function convertNodesToNewObject() {
	
		$newRootNode=$this->typeCastingNode($this->rootNode);
		
		if ($newRootNode)
		
			$this->rootNode=$newRootNode;
		
	}
	
}
	
?>