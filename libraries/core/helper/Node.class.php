<?php
class Node extends RootObject {

	var $key=NULL;
	
	var $title=NULL;
	
	//var $level=0;
	
	var $childs=array();
	
	var $html;
	
	function __construct($itemObj=NULL) { //$key=NULL, $title=NULL, $level=0) {
	
		$properties = (array) $itemObj; //cast to an array

		if (is_array($properties))
		{
			foreach ($properties as $k => $v) {
			
				if (property_exists(get_class($this), $k))
				
					$this->$k = $v;
			}

		}
		
		parent::__construct();
		
	}
	
	function __destruct() {
	
		parent::__destruct();
		
	}
	function _includeClass() {
	
		include $this->tableName.".table.class.php";
		
	}
	function getChilds() {
	
		return $this->childs;
		
		
	}
	
	function getHTML() {
	
		return $this->html;
		
	}
	function hasChild() {
		
		return isset($this->childs);
		
	}
	function childsNum() {
	
		return count($this->childs);
	}
	
	function addChild($node) {
	
		$this->childs[]=$node;
		
	}
	
	function render($type='list') {
	
		switch ($type) {
		
			case 'list':
					
					$this->html="<li>".$this->title."</li>";
			
				break;
				
			case 'option':
					
			
					$this->html="<option value='".$this->key."'>".$this->title."</otpion>";
			
				break;
				
			default:
					
		}
	
	}	
	function layout() {
		
		echo $this->html;
		
	}
	
	
}
	
?>