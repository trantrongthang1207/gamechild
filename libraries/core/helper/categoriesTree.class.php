<?php

include_once "Tree.class.php";

class categoriesTree extends Tree {

	var $tableName;
	
	var $title_field;
	
	var $ordering_field;
	
	var $root_node_key=0;
	
	var $seleted_node_key=0;
	
	var $selectedNode;
	
	
	function __construct($tableName, $title_field='title', $ordering_field=NULL) {
	
		$this->tableName=$tableName;
	
		$this->title_field=$title_field;
		
		$this->ordering_field=$ordering_field;
		
		parent::setNodeClass('categoryNode');
		
		parent::__construct();
		
	}
	
	function __desctruct() {
	
		parent::__destruct();
		
	}
	
	function _getChildNodes($parentNode) {
	
		if (!$parentNode)
		
			return NULL;
	
		$parentKey=$parentNode->get('key');
	
		global $db;
		
			
		$sql ="SELECT ".$db->nameQuote('id')." AS ".$db->nameQuote('key')." , ".$db->nameQuote($this->title_field)." AS ".$db->nameQuote('title')
			 ." FROM ".$db->nameQuote($this->tableName)	
			 ." WHERE ".$db->nameQuote('parent')." = ". $db->Quote($parentKey);	
		
		if ($this->ordering_field)
		
			$sql.=" ORDER BY ".$db->nameQuote($this->ordering_field)." ASC ";
		
		$db->setQuery($sql);
		
		$itemList=$db->loadObjectList();
		
		if ($itemList) {
		
			foreach ($itemList as $item) {
			
				$nodeClass=$this->nodeClass;
			
				$node=new $nodeClass($item);
				
				if ($node->get('key')==$this->selected_node_key)
				
					//$this->selectedNode=$node;
					$node->set('selected',true);
			
				$this->_getChildNodes($node);
				
				$parentNode->addChild($node);
				
			}
		
		}
		
	}
	
	function initialize($root_node_key=0, $selected_node_key=0) {
	
		include_once $this->nodeClass.".class.php";
	
		if ($root_node_key)
		
			$this->root_node_key=$root_node_key;
			
		if ($selected_node_key)
		
			$this->selected_node_key=$selected_node_key;
		
		$item->id=$this->root_node_key;
		
		$item->title='Top';	
			
		if ($this->root_node_key) {
		
			global $db;
			
			$sql ="SELECT ".$db->nameQuote('id')." AS ".$db->nameQuote('key')." , ".$db->nameQuote($this->title_field)." AS ".$db->nameQuote('title')
			 ." FROM ".$db->nameQuote($this->tableName)	
			 ." WHERE ".$db->nameQuote('id')." = ". $db->Quote($this->root_node_key);	
				
			$db->setQuery($sql);
			
			$db->query();
			
			$item=$db->loadObject();
				
		}		
				
		$nodeClass=$this->nodeClass;
			
		$rootNode=new $nodeClass($item);
		
		if ($rootNode->get('key')==$this->selected_node_key)
		
			//$this->selectedNode=$rootNode;
			$rootNode->set('selected',true);
			
		parent::initialize($rootNode);
		
				
	}
		
			
}
	
?>