<?php
class menuitem extends RootObject {

	var $id=0;
	
	var $title='';
		
	var $link='';
	
	var $level=0;
	
	var $menu_id=0;
	
	var $childs=array();
	
	var $html='';

        var $type='';
		
	function __construct($menuitemObj=NULL) {
		
		if ($menuitemObj) {
		
			$this->setProperties($menuitemObj);

                        $this->buildLink();
                }
		parent::__construct();
				
	}
	
	function __destruct() {
	
		parent::__destruct();
		
	}
	
	
	
	
	function getHTML() {
			
		return $this->html;
		
	}
	
	function hasChild() {
	
		return count($this->childs);
		
	}
	
	/* Params: (menuitem Object) $child 
	*/
	function addChild(&$child) {
	
		$this->childs[]=$child;
		
	}
	
	function getChilds() {
	
		return $this->childs;
		
	}
        
        function buildLink() {
            
            if ($this->type='component')
                
                $this->link=$this->link."&itemId=".$this->id;
                
        }
	/*params $type='link'|'option'|'text'
	*/
	
	function render($type='link', $preChar='&nbsp;', $html_id=NULL, $className=NULL, $moreHtmlParams=NULL) {
		
		if ($html_id)
			
			$id="id='$html_id'";
		if ($className)
		
			$class="class='$className'";	
			
		$preString='';
				
		for ($i=1; $i<=$this->level; $i++)
		
			$preString.=$preChar;
			
		$preString="<pre>$preString</pre>";	
				
		switch ($type) {
			
			case  'link':
			
			
				$this->html="<a id='$html_id' class='$className' $moreHtmlParams href=".$this->link.">".$preString.$this->title."</a>";
			
				break;
							
			case 'option':
							
				$this->html="<option $id $class $moreHtmlParams  value='".$this->id."'>".$preString.$this->title."</option>";
			
				break;	
				
			case 'text':
				
				$this->html=$preString.$this->title;
			
				break;	
				
			default:
			
				break;	
					
		}
		
	}
	
	
}
?>