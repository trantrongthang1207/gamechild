<?php

class manageDataHelper extends RootObject {

    var $table=NULL;

    var $tableObj=NULL;

    var $template='default';

    var $comUrl='';

    var $comLink='';

    var $sessionSpace='';

    var $data=NULL;

    var $templateFile='default.tpl.php';
	/*
	$newSessionName='';
	
	$modifySessionName='';
	*/
    function __construct($table, $template=NULL, $comUrl=NULL, $sessionSpace=NULL) {

        $this->table=$table;

        if ($template)

            $this->template=$template;

        if ($sessionSpace)

            $this->sessionSpace=$sessionSpace;

        else

            $this->sessionSpace=$this->table;

        if ($comUrl)

            $this->comUrl=$comUrl;

        else

            $this->comUrl="index.php?com=".$this->table."Manager";

        $this->comLink=str_replace('&','&amp;',$this->comUrl);

        global $db;

        $this->includeTableClass();

        $tableClass=$this->table."_table";

        $this->tableObj=new $tableClass ($db);


    }

    function __destruct() {

        parent::__destruct();
    }

    function task_new($redirectUrl=NULL) {

        $module=$_GET['module'];

        global $app;

        $session=$app->get('session');

        switch (strtolower($module)) {

            case 'process':

                if ($session->get('new',NULL,$this->sessionSpace)) {

                    $session->clear('new',$this->sessionSpace);

                    $arrData=$this->inputDataProcess($_POST);

                    //if (inputDataValidate('newCountry',$arrData))
                    //Check input data here.

                    //$data['result']=$this->tableObj->insert($arrData);

                    $data['result']=$this->newRecord($arrData);

                    if ($data['result']) {

                        if  ($redirectUrl)

                            redirect($redirectUrl);

                    }

                    else

                        $data['error']=$this->getErrors();

                    $templateFile='newReport.tpl.php';


                }

                else {

                    redirect($this->comUrl."&task=new&module=newForm");
                }

                break;

            case 'newform':

                if ($session->get('new',NULL,$this->sessionSpace))

                    $session->clear('new',$this->sessionSpace);

                $session->set('new',1,$this->sessionSpace);

                //$data['form_data']=$this->tableObj->getNewFormData();

                $data['form_data']=$this->getNewFormData();

                $data['form_action']=$this->comLink."&amp;task=new&amp;module=process";

                $templateFile='newForm.tpl.php';

                break;

            default:

                redirect($this->comUrl."&task=new&module=newForm");
                //redirect($this->comUrl);

                break;


        }

        $this->parse($data, $templateFile);


    }
    function task_delete($redirectUrl=NULL) {

        $id=$_REQUEST['id'];

        if ($this->tableObj->load($id) ) {

        //$data['result']=$this->tableObj->delete($id);
            $data['result']=$this->deleteRecord($id);

            if ($data['result']) {

                if  ($redirectUrl)

                    redirect($redirectUrl);

            }

            else

                $data['error']=$this->getErrors();

        }

        else {

            $data['result']=false;

            $data['error']="Không có dữ yêu cấu trong DB";


        }


        $templateFile='deleteReport.tpl.php';

        $this->parse($data, $templateFile);
    }

    function task_modify($redirectUrl=NULL) {

        $module=$_GET['module'];

        global $app;

        $session=$app->get('session');

        switch (strtolower($module)) {

            case 'process':

                if ($session->get('modify',NULL,$this->sessionSpace)) {

                    $session->clear('modify',$this->sessionSpace);

                    $id=$_REQUEST['id'];

                    $arrData=$this->inputDataProcess($_POST);

                    //if (inputDataValidate('newCountry',$arrData))
                    //Check input data here.

                    //$data['result']=$this->tableObj->update($id, $arrData);
                    $data['result']=$this->updateRecord($id, $arrData);

                    if ($data['result']) {

                        if  ($redirectUrl)

                            redirect($redirectUrl);

                    }

                    else

                        $data['error']=$this->getErrors();

                    $templateFile='modifyReport.tpl.php';

                }

                else {

                    redirect($this->comUrl."&task=listView");
                }

                break;

            case 'modifyform':

                if ($session->get('modify',NULL,$this->sessionSpace))

                    $session->clear('modify',$this->sessionSpace);

                $session->set('modify',1,$this->sessionSpace);

                $id=$_REQUEST['id'];

                //$data['form_data']=$this->tableObj->getModifyFormData($id);
                $data['form_data']=$this->getModifyFormData($id);

                $data['form_action']=$this->comLink."&amp;task=modify&amp;module=process&amp;id=".$id;

                $templateFile='modifyForm.tpl.php';

                break;

            default:

                redirect($this->comUrl."&task=listView");

                break;
        }

        $this->parse($data, $templateFile);
    }

    function task_detailsView() {

        $id=$_REQUEST['id'];

        //$data['details_data']=$this->tableObj->getDetailsViewData($id);
        $data['details_data']=$this->getDetailsViewData($id);

        $data['newLink']=$this->comLink."&amp;task=new";

        $templateFile='detailsView.tpl.php';

        $this->parse($data, $templateFile);

    }

    function task_listView() {

        $inputData=$this->inputDataProcess($_GET);

        $data['form_data']=$this->getListViewData($inputData);

        foreach ($data['form_data'] as $key=>$obj) {
            if (is_object($obj)) {

                $obj->detailsViewLink=$this->comLink."&amp;task=detailsView&amp;id=".$obj->id;

                $obj->deleteLink=$this->comLink."&amp;task=delete&amp;id=".$obj->id;

                $obj->modifyLink=$this->comLink."&amp;task=modify&amp;module=modifyForm&amp;id=".$obj->id;
            }
        }

        $data['newLink']=$this->comLink."&amp;task=new";

        $templateFile='listView.tpl.php';

        $this->parse($data, $templateFile);

    }

    function _parseTemplate(&$templateFile) {

        global $app;

        $componentObj=$app->get('component');

        $componentObj->setTemplateFile($this->template.DS.$templateFile);
    }

    function _parseData(&$data) {

        global $app;

        $componentObj=$app->get('component');

        $componentObj->setData($data);

    }
    function parse(&$data, &$templateFile) {

        $this->outputDataProcess($data);

        $this->_parseData($data);

        $this->_parseTemplate($templateFile);
    }

    function includeTableClass() {

        include_once "database".DS.$this->table.".table.class.php";

    }
    function outputDataProcess(&$data) {

        return $data;

    }
    function inputDataProcess(&$data) {

        return $data;

    }
    function newRecord(&$data) {

        return $this->tableObj->insert($data);

    }
    function updateRecord($id,&$data) {

        if (method_exists($this->tableObj, 'update'))

            return $this->tableObj->update($id, $data);

        else {

            $this->tableObj->load($id);

            return $this->tableObj->save($data);

        }

    }

    function deleteRecord($id) {

        return $this->tableObj->delete($id);

    }

    function getNewFormData() {

        return NULL;

    }

    function getDetailsViewData($id) {

        if ($this->tableObj->load($id))

            return clone $this->tableObj;

        else

            return false;

    }

    function getModifyFormData($id) {

        if ($this->tableObj->load($id))

            return clone $this->tableObj;

        else

            return false;

    }

    function getListViewData(&$inputData=NULL) {

        return $this->tableObj->loadObjectList();

    }

}


?>	