<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class Time extends RootObject {

    static $monthNames=array(1=>"January","February","March","April", "May","June","July","August", "September","October","November","December");
    var $seconds;
    var $minutes;
    var $hours;
    var $mday;
    var $wday;
    var $mon;
    var $year;
    var $yday;
    var $weekday;
    var $month;
    var $timestamp=0;

    function __construct($timestamp=NULL){

        parent::__construct();

        $this->setTime($timestamp);

   }

   function setTime($timestamp=null) {

       $this->timestamp=$timestamp;

         if (empty($timestamp)) $timestamp=time();

        $this->timestamp=$timestamp;

        $this->setProperties(getdate($this->timestamp));


   }
   function _getTimestampParam($timestamp=null) {
       return $timestamp ? $timestamp : $this->timestamp;
   }
   function getDaysOfMonth($timestamp=null) {
       $timestamp= self::_getTimestampParam($timestamp);
       return date('t', $timestamp);
   } 
   function getMysqlDatetime($timestamp=null) {
        $timestamp= self::_getTimestampParam($timestamp);
        return date('Y-m-d H:i:s', $timestamp);
   }
   function getTime($formart='Y-m-d H:i:s') {
        
   }

}
?>
