<?php

class Router extends RootObject {

    /**
     *
     * @var Request
     */
    function __construct() {

    }

    function _destruct() {

        parent::__destruct();
    }

    function getApplication() {

        $appName=null;
        
        $app = App::getInstance();
        $request = $app->getRequest();
       
        if (empty($_SERVER['QUERY_STRING'])) {           
            $setting = $app->getSetting();
            $pageId=$setting->getFrontPage();
            $request->setVar('pageId', $pageId);
            $appName = $setting->getAppName($pageId);
            $request->setVar('app',$appName);
            $request->setVars($setting->getRequests($pageId));
           
        }
        elseif (!$appName = $request->getVar('app', "")) {
            if ($pageId = trim($request->getVar('pageId', ""))) {
                $setting = $app->getSetting();
                $appName = $setting->getAppName($pageId);
            }
        }

        return $appName;
    }

    function getPageId() {
        $app = App::getInstance();
        $request = $app->getRequest();
        $pageId = $request->getVar('pageId', "");
        
        if (!$pageId) {
            if ($appName = $request->getVar('app', "")) {
                $setting = $app->getSetting();
                $pageId = $setting->getPageId($appName);
            }
        }

        return $pageId;
    }

}

?>