<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Controller {

    var $name;

    var $default_task;

    var $default_view;

    function __construct() {
        
    }

    function display($view=null) {
        
        if (empty($view)) {
            $app = $this->getApp();
            $request = $app->getRequest();
            
            $view=$request->getVar('view','');
            
        }
     
        $viewObj = $this->getView();
        $viewObj->execute($view);
    }

    /**
     *
     * @return Request;
     */
    function &getRequest() {
        $app = $this->getApp();
        return $app->getRequest();
    }

    /**
     *
     * @return Application
     */
    function &getApp() {
        $app = App::getInstance();
        return $app;
    }

    function execute($task='') {
        if (!empty($task)) {
            $method_name = "task_" . $task;
            if (method_exists($this, $method_name))
                $this->$method_name();
        }
        else {
           
            $this->display();
        }
    }

    /**
     *
     * @return Model
     */
    function &getModel() {
        include_once $this->getAppPath() . 'model.php';
        $modelClassName = $this->name . "Model";
        $model = new $modelClassName ();
        return $model;
    }

    /**
     *
     * @return View
     */
    function &getView() {
        include_once $this->getAppPath() . 'view.php';
        $viewClassName = $this->name . "View";
        $model = new $viewClassName();
        return $model;
    }

    function getAppPath() {
        return PATH_APPS . $this->name . DS;
    }

    function includeModel($name=null) {
        include_once $this->getAppPath() . 'model.php';
    }

    function includeView($name=null) {
        include_once $this->getAppPath() . 'model.php';
    }

}

?>
