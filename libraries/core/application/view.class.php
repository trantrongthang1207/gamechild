<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class View {

    var $name;
    /**
     *
     * @var Template
     */
    var $template = null;
    var $data;

    function __construct() {
        $this->initTemplate();
    }

    function getAppPath() {
        return PATH_APPS . $this->name . DS;
    }

    function setData(&$data) {

        if (is_object($data))
            $this->data = clone $data;

        else
            $this->data = $data;
    }

    function setVar($varName, &$value) {

        if (is_object($value))
            $this->data[$varName] = clone $value;

        else
            $this->data[$varName] = $value;
    }

    function display($view='', $tpl='') {
        
        $app = App::getInstance();
        $request = $app->getRequest();

        if (empty($view))
            $view = $request->getVar('view', 'default');

        if (empty($tpl))
            $tpl = $request->getVar('tpl', 'default');

        $template = new AppTemplate($this->name, $view, $tpl);

        if ($this->data)
            $template->setData($this->data);

        echo $template->render();
    }

    function resetTemplate() {
        $this->initTemplate();
    }

    function initTemplate() {
        $this->template = new Template();
        $this->template->setTemplatePath($this->getAppPath() . 'views' . DS);
    }

    /**
     *
     * @return Model
     */
    function &getModel() {
        include_once $this->getAppPath() . 'model.php';
        $modelClassName = $this->name . "Model";
        $model = new $modelClassName ();
        return $model;
    }

    function includeModel($name=null) {
        include_once $this->getAppPath() . 'model.php';
    }

    function execute($view) {

        $method_name = "view_" . $view;
        if (method_exists($this, $method_name))
            $this->$method_name();
    }

}

?>
