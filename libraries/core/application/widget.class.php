<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Widget extends View{

    var $name;
    /**
     *
     * @var Template
     */
    var $template = null;



    function __construct($name) {
        $this->name=$name;
        
    }

    function getWidgetPath() {
        return PATH_APPS . $this->name . DS;
    }
   
    function display($data=null, $tpl='default') {
        $template=new WidgetTemplate($this->name, $tpl);
        if ($data)
            $template->setData($data);
        echo $template->render();
       
        
    }

   

}

?>
