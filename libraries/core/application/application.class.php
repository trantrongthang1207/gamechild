<?php

class Application extends RootObject {

    var $name = 'site';
    var $format = 'html';
    var $session = NULL; //object
    var $router = NULL;
    var $template = 'default';
    var $itemId = NULL;
    var $configs = NULL;
    var $error = NULL;
    var $html = '';
    var $uri = '';

    function application($name='site', $configs) {

        $this->_setName($name);

        $this->configs = $configs;

        $this->uri = "http://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];

        $this->error = NULL;

        $this->debug = NULL;

        $this->template = 'default';
//$this->startSession();
    }

    function __contruct($type) {

        $this->_setName($type);

        $this->uri = NULL;

        $this->error = NULL;

        $this->debug = NULL;

        $this->template = 'default';
//$this->startSession();
    }

    function __destruct() {

        parent::__destruct();
    }

    function _getCFg() {

    }

    function initialize() {

//start session
        $this->session = new Session();

//route

        $request = $this->getRequest();
    }

    function execute($appName='') {

        if (empty($app)) {
            $router = $this->getRouter();
            $appName = $router->getApplication();
            if ($appName == null) {
                $this->redirect(URL_SITE . 'index.php');
            }
        }

        $appExecuteFile = PATH_APPS . $appName . DS . $appName . ".php";

        if (PATH_ROOT !== PATH_SITE && !file_exists($appExecuteFile)) {
            define('PAHT_APPS', PATH_ROOT . 'apps' . DS);
            $appExecuteFile = PATH_APPS . $appName . ".php";
        }

        define('PAHT_APP', PATH_APPS . $appName . DS);

        if (file_exists($appExecuteFile)) {

            ob_start();

            include_once($appExecuteFile);

            $this->html = ob_get_clean();
        } else {

            $this->setError('Application is not found');

            $setting = $this->getSetting();

            $appName = $setting->getAppName($setting->getErrorPage());

            $appExecuteFile = PATH_APPS . $appName . DS . $appName . ".php";

            ob_start();

            include($appExecuteFile);

            $this->html = ob_get_clean();
        }
        if (PATH_ROOT !== PATH_SITE) {
            define('PAHT_APPS', PATH_SITE . 'apps' . DS);
        }
    }

    function _setName($type) {

        if ($type == 'admin')
            $this->name = 'admin';

        else
            $this->name = 'site';
    }

    function render() {

        $request = $this->getRequest();

        if (strtolower($request->getVar('format', 'html')) != 'raw') {

            $siteSetting = $this->getSetting();

            $template = new SiteTemplate($siteSetting->getTemplate());

            $this->html = $template->render();
        }
    }

    function getOutput() {

        return $this->html;
    }

    function output() {

        echo $this->html;
    }

    function setError($error) {

        $this->error[] = $error;
    }

    /**
     *
     * @param <type> $url 
     */
    function redirect($url) {
        if (is_int($url)) {
            $url=self::getPageUrl($url);
        }
        if (!$this->isAjax() && !headers_sent()) {

            header('Location:' . $url);
        } else {
            echo '<script type="text/javascript">
                window.location = "' . $url . '";
            </script>';
        }
        $this->close();
    }

    function getPageUrl($pageId) {

        $uri = self::getUri();
        $setting = self::getSetting();
        if ($application = $setting->getApp($pageId)) {
            $uri->setParam('app', $application['name']);
            $uri->setParams($application['requests']);
            $uri->setParam('pageId', $this->page_id);
            $uri->build();
            return $uri->getUri();
        }
        else
            return null;
    }

    /**
     *
     * @return boolean
     */
    function isAjax() {
        $request = $this->getRequest();
        return (strtolower($request->getVar('format', 'html')) == 'raw');
    }

    /**
     *
     * @param <type> $configs
     * @return Database
     */
    function getDBO() {

        $configs = $this->configs['database'];
        $db = Database::getInstance($configs, 'mysql');

        if (!is_object($db)) {
            $this->setError($db);
            return $db;
        }

        else
            return $db;
    }

    function close() {
        
    }

    function halt($message=null) {
        ob_end_flush();
        exit($message);
    }

    /**
     *
     * @return Session
     */
    function getSession() {
        return $this->session;
    }

    /**
     *
     * @staticvar Request $request
     * @return Request
     */
    function getRequest() {
        static $request;
        if (empty($request))
            $request = new Request ();
        return $request;
    }

    /**
     *
     * @staticvar Response $response
     * @return Response
     */
    function getResponse() {
        static $response;
        if (empty($response))
            $response = new Response ();
        return $response;
    }

    /**
     *
     * @staticvar Language $language
     * @return Language
     */
    function getLanguage() {
        static $language;
        if (empty($language))
            $language = new Language ();
        return $language;
    }

    /**
     *
     * @staticvar Uri $url
     * @return Uri
     */
    function getUri() {
        static $url;
        if (empty($url))
            $url = new Uri($this->uri);
        return $url;
    }

    function getSetting() {
        static $setting;
        if (empty($setting))
            $setting = new SiteSetting('application');
        return $setting;
    }

    function getPageId() {
        $request = $this->getRequest();
        $appName = $request->getVar('app', "");

        $setting = $this->getSetting();
        return $setting->getPageId($appName);
    }

    function getRouter() {
        static $router;
        if (empty($router))
            $router = new Router ();
        return $router;
    }

    function checkConditions() {
        $setting = self::getSetting();
        if ($conditions = $setting->getConditions()) {
            foreach ($conditions as $name => $condition) {
                if ($conditionFile = Path::find(array(PATH_CONDITIONS, PATH_ROOT . 'condistions' . DS), $name . '.php')) {
                    include $conditionFile;
                    $conditionObj = new $name();
                    $conditionObj->check();
                }
            }
        }
      
    }
   
}

?>