<?php

if (!defined('FGC'))
    define('FGC', 1);

if (!defined('DS'))
    define('DS', DIRECTORY_SEPARATOR);
if (!defined('PATH_ROOT'))
    define('PATH_ROOT', dirname(__FILE__) . DS);
include_once PATH_ROOT . 'defines.php';
include_once PATH_SITE . 'configs.php';
include_once( PATH_LIBRARY_CORE . 'functions.php');
include_once( PATH_LIBRARY_CORE . 'object.class.php');
include_once (PATH_SITE . "helper.php");
?>