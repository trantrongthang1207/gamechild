<?php
header('P3P: CP="IDC DSP COR ADM DEVi TAIi PSA PSD IVAi IVDi CONi HIS OUR IND CNT"');
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
define('PATH_ROOT', './../');
include_once PATH_ROOT . 'includes.php';

function getPercentage($value1, $value2) {
    if (!$value1 || !$value2)
	return "0%";
    return (round($value1 / $value2, 2) * 100) . "%";
}

/**
 * dinh dang lai chuoi cau tra loi
 * @param type $string
 * @return string
 */
function formatAnswer($string) {
    if (empty($string))
	return '';
    $arr = array('?', '+', '-', 'x', '\/', '(', ')', '=');
    foreach ($arr as $value) {
	$string = @str_replace($value, ' ' . $value . ' ', $string);
    }
    return $string;
}

class App {

    static $instance;

    function __construct($configs) {

	self::$instance = new Application('site', $configs);
    }

    /**
     *
     * @global <type> $application
     * @return Application
     */
    function &getInstance() {
	return self::$instance;
    }

    function halt($message = null) {
	if (isset(self::$instance))
	    self::$instance->halt($message);
	else
	    exit($message);
    }

    function __call($method_name, $arguments) {
	if (!method_exists($this, $method_name)) {
	    return self::$instance->$method_name();
	}
    }

}

$app = new App($configs);
$application = $app->getInstance();
$application->initialize();
$request = $application->getRequest();
$session = $application->getSession();
$db = $application->getDBO();
//lay toan bo cau hoi da duoc luu vao session
$allQuestion = $session->get('number_testsection_question', array());
$record = count($allQuestion);
$task = $request->getVar('task', '');
$questionid = $request->getVar('questionid');
//luu cau tra loi cua nguoi dung
$user_answer = array();
$user_answer = $session->get('user_answer');
if (count($user_answer) > 0) {
    foreach ($user_answer as $k => $v) {
	if (!in_array($k, $allQuestion)) {
	    if (isset($user_answer[$k])) {
		unset($user_answer[$k]);
	    }
	}
    }
}
$ans = $_REQUEST['nums'];
$user_answer[$questionid] = @implode("", $ans);
$session->set('user_answer', $user_answer);
//lay thoi gian lam bai test
$inputtesttime = $session->get('inputtesttime');
$inputtesttime+=$request->getVar('inputtesttime');
$inputtesttime = $session->set('inputtesttime', $inputtesttime);

//end
$key = @array_search($questionid, $allQuestion);
$questionidnext = $allQuestion[($key + 1)];
$sql = "SELECT * FROM math_questions" .
	" WHERE id = '$questionidnext'";
$db->setQuery($sql);
$result = $db->loadObject();
$answers = json_decode($result->answers);
//neu con co cau hoi
//check het thoi gian lam bai
$finish = $_REQUEST['finish'];
?>
<?php if ($questionidnext != null && $questionidnext > 0 && $finish != 1) { ?>
    <div id="conent_javacript">
        <script type="text/javascript" src="assets/js/reloadscript.js"></script>
    </div>
    <div class="content_question_next content_submit">
        <form id="testdragdrop" method="post" name="answerForm" action="test.php?apps=math&questionid=<?php echo $result->id ?>" AUTOCOMPLETE="OFF">
    	<div id="dragnumber">
		<?php
		$question_content = $result->question_content;
		$arrQuestion_content = @explode('$', $question_content);
		$countQuestion = count($arrQuestion_content);
		for ($i = 0; $i < $countQuestion; $i++) {
		    if ($arrQuestion_content[$i] == '?') {
			?>
	    	    <div class="ui-droppable tv-drop" id="num1">&nbsp;<input type="hidden" value="0" name="nums[]" class="tvnums"/></div>
			<?php
		    } else if ($arrQuestion_content[$i] == '=' || $i == ($countQuestion - 1)) {
			?>
	    	    <div class="no-ui-droppable"><?php echo $arrQuestion_content[$i] ?><input type="hidden" value="<?php echo $arrQuestion_content[$i] ?>" name="nums[]"/></div>
			<?php
		    } else {
			?>
	    	    <div class="no-ui-droppable"><?php echo $arrQuestion_content[$i] ?><input type="hidden" value="<?php echo $arrQuestion_content[$i] ?>" name="nums[]"/></div>
			<?php
		    }
		}
		?>
    	</div>
    	<div id="draglistnumber">
		<?php
		$arr_suggest_answer = @explode(";", $result->suggest_answer);
		foreach ($arr_suggest_answer as $sa_v) {
		    if ($sa_v != '' && is_numeric($sa_v)) {
			?>
	    	    <div class="ui-droppable" id="card<?php echo $sa_v; ?>" data-value="<?php echo $sa_v; ?>"><div class="ui-draggable card9"><?php echo $sa_v; ?></div></div>
			<?php
		    }
		}
		?>
    	</div>
    	<div class="next_testsection">
    	    <input type="hidden" id="tvfinish" value="0" name="finish"/>
    	    <input type="hidden" id="inputtesttime" value="0" name="inputtesttime"/>
    	    <input type="submit" name="submit" value="Next" class="submitnext"/>
    	</div>
        </form>
    </div>
<?php } ?>
<?php
if ($questionidnext == null || $questionidnext == '' || $finish == 1) {
    //lay cau tra loi va tinh cau dung
    $user_answer = $session->get('user_answer');
    $sql = "SELECT * FROM math_questions" .
	    " WHERE published = 1 AND id IN(" . @implode(",", $allQuestion) . ")";
    $db->setQuery($sql);
    $listQuestion = $db->loadObjectList();
    $correct = 0;
    if (count($listQuestion) > 0) {
	foreach ($listQuestion as $value) {
	    $arr_answers = json_decode($value->answers);
	    $right_answer = json_decode($value->right_answer);
	    if (@in_array($user_answer[$value->id], $arr_answers) && @in_array(@array_search($user_answer[$value->id], $arr_answers), $right_answer)) {
		$correct++;
	    }
	}
    }
    //tinh thoi gian
    $timeQuestion = $session->get('inputtesttime');

    function convertTime($timeQuestion) {
	if (!$timeQuestion) {
	    $stringTime = "00:00:00";
	} else {
	    $hour = floor($timeQuestion / 3600);
	    $minute = floor(($timeQuestion - $hour * 3600) / 60);
	    $second = $timeQuestion - $hour * 3600 - $minute * 60;
	    if ($hour < 10) {
		$hour = '0' . $hour;
	    }
	    if ($minute < 10) {
		$minute = '0' . $minute;
	    }
	    if ($second < 10) {
		$second = '0' . $second;
	    }
	    $stringTime = $hour . ':' . $minute . ':' . $second;
	}
	return $stringTime;
    }

    $stringTime = convertTime($timeQuestion);

    //Tinh phan tram cau tra loi dung
    $correct_percent = getPercentage($correct, $record);

    //luu vao db
    $sql_insert = "INSERT INTO math_report(correct_percent) VALUES('" . substr($correct_percent, 0, strlen($correct_percent) - 1) . "')";
    $db->setQuery($sql_insert);
    $db->query();
    ?>
    <style type="text/css">
        .timeout_game,
        .testsecexit{
    	display: none;
        }
    </style>
    <div class="result">
        <div class="result_inner result_math_test">
    	<div class="result_title">
    	</div>
    	<div class="result_complate">
    	    <label>You Scored:</label> <?php echo $correct . "&nbsp;&nbsp;"; ?>out of<?php echo "&nbsp;&nbsp;" . $record . "&nbsp;&nbsp;"; ?>Questions
    	</div>
    	<div class="result_percentage">
    	    <label>You Scored:<span> <?php echo $correct_percent; ?></span></label>
    	</div>
    	<div class="resutl_time">
    	    <label>Time Used:<span> <?php echo $stringTime; ?></span></label>
    	</div>
    	<div class="result_show">
    	    <a href="#" class="aresult_show">Click Here to view answers!</a>
    	</div>
    	<div class="retake_testsec">
    	    <a href="/math/math.html">&nbsp;</a>
    	</div>
        </div>
    </div>
    <div class="fgcresult_show">
	<?php
	for ($k = 0; $k < count($allQuestion); $k++):
	    $sql = "SELECT * FROM math_questions" .
		    " WHERE published = 1 AND id = '{$allQuestion[$k]}'";
	    $db->setQuery($sql, 0, 1);

	    if ($resultQuestion = $db->loadObject()) {
		?>
	        <div class="inner">
	    	<div class="fgcresult_left">
	    	    <span>Q.<?php echo $k + 1; ?></span>
	    	</div>
	    	<div class="fgcresult_right">
	    	    <div class="fgccontent_question">
	    		<div id="dragnumber">
				<?php
				$question_content = $resultQuestion->question_content;
				$arrQuestion_content = @explode('$', $question_content);
				$countQuestion = count($arrQuestion_content);
				for ($i = 0; $i < $countQuestion; $i++) {
				    if ($arrQuestion_content[$i] == '?') {
					?>
		    		    <div class="ui-droppable tv-drop" id="num1">&nbsp;<?php echo $arrQuestion_content[$i] ?></div>
					<?php
				    } else if ($arrQuestion_content[$i] == '=' || $i == ($countQuestion - 1)) {
					?>
		    		    <div class="no-ui-droppable"><?php echo $arrQuestion_content[$i] ?></div>
					<?php
				    } else {
					?>
		    		    <div class="no-ui-droppable"><?php echo $arrQuestion_content[$i] ?></div>
					<?php
				    }
				}
				?>
	    		</div>
	    	    </div>
	    	    <div class="fgccontent_answers">
	    		<ul class="listanswers">
	    		    <li><label style="font-weight: bold;">Correct Answers:</label></li>
				<?php
				$ans = json_decode($resultQuestion->answers);
				$set_correct = true;
				if ($resultQuestion->right_answer != '') {
				    $right_answer = json_decode($resultQuestion->right_answer);
				} else {
				    $right_answer = array();
				}
				foreach ($ans as $j => $value) {
				    if (@in_array($j, $right_answer)) {
					?>
		    		    <li>
		    			<label for="question_<?php echo $i ?>">
						<?php echo formatAnswer($value); ?>
		    			</label>
					    <?php
					    if (@in_array($user_answer[$resultQuestion->id], $ans) && @in_array(@array_search($user_answer[$resultQuestion->id], $ans), $right_answer) && $set_correct == true) {
						$set_correct = false;
					    }
					    ?>
		    		    </li>
					<?php
				    }
				}
				?>
	    		</ul>
	    	    </div>
	    	    <div class="line text_color text_bold">
	    		<span>Your Answer: <?php
				if (isset($user_answer[$resultQuestion->id]) && $user_answer[$resultQuestion->id] != '') {
				    echo formatAnswer($user_answer[$resultQuestion->id]);
				    if (@in_array($user_answer[$resultQuestion->id], $ans) && @in_array(@array_search($user_answer[$resultQuestion->id], $ans), $right_answer)) {
					echo '<img src="assets/images/tick.png"/>';
				    }
				} else
				    echo 'Timeout';
				?></span><?php if (!in_array($user_answer[$resultQuestion->id], $ans) || !in_array(@array_search($user_answer[$resultQuestion->id], $ans), $right_answer)) echo '<img src="assets/images/publish_x.png"/>' ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	    	    </div>
	    	</div>
	        </div>
	    <?php } endfor; ?>
    </div>
    <?php
}
?>