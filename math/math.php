<?php
header('P3P: CP="IDC DSP COR ADM DEVi TAIi PSA PSD IVAi IVDi CONi HIS OUR IND CNT"');
include_once ('header_new.tpl.php');
?>
<style type="text/css">  
    body {
        background-position: 0 -73px;
    }
    #header {
        height: 15px;
    }
    .pre_next{
        padding-top: 0;
    }
    .question_answer > div{
        padding-bottom: 20px;
    }
</style> 
<link type="text/css" rel="stylesheet" href="assets/css/style.css"/>
<link type="text/css" rel="stylesheet" href="assets/css/jquery.ui.dialog.css"/>
<link type="text/css" rel="stylesheet" href="assets/css/dragdrop.css"/>
<div id="conent_javacript">
    <script type="text/javascript" src="assets/js/jquery-ui.js"></script>
    <script type="text/javascript" src="assets/js/reloadscript.js"></script>
    <script type="text/javascript" src="assets/js/tvscript.min.js"></script>
</div>
<!--[if IE 7]>
<style type="text/css">
        .radioquestion{
            margin: 0 2px 0 0;
        }
        .testsecexit a,
        #view_string_time{
            display: block;
        }
</style>
<![endif]-->
<?php
$app = App::getInstance();

$db = $app->getDBO();

include_once (PATH_ADMIN_APPS . 'models' . DS . 'config.model.php');
$model = new configModel();

$number_random_question = $model->getvalueConfig('number_random_question_math')->value;
if (!$number_random_question) {
    $number_random_question = 10;
}
$sql = "SELECT * FROM math_questions" .
	" WHERE published = 1" .
	" ORDER BY RAND()";
$db->setQuery($sql, 0, $number_random_question);
$results = $db->loadObjectList();
//lay so nguoi da tham gia bai thi
$sql_total = "SELECT COUNT(id) AS total FROM math_report";
$db->setQuery($sql_total);
$total_test = $db->loadResult();
//lay ti le phan tram
$sql_total_percent = "SELECT SUM(correct_percent) AS total_per FROM math_report";
$db->setQuery($sql_total_percent);
$total_per = $db->loadResult();

function getPercentage($value1, $value2) {
    if (!$value1 || !$value2)
	return "0%";
    return (round($value1 / $value2, 2)) . "%";
}

if (empty($results)) {
    ?>
    <div class="noquestion">No Exists question</div>
    <?php
} else {
    ?>
    <?php
    $result = $results[0];
    $answers = json_decode($result->answers);

    $session->clear('inputtesttime');
    $session->clear('user_answer');
    $session->clear('testsection_result_answer');
    $session->clear('number_answer_testsection');
    $session->clear('number_testsection_question', array());
    $testsection_question = array();
    for ($i = 0; $i < count($results); $i++) {
	$testsection_question[] = $results[$i]->id;
	?>
	<script type="text/javascript">
	    arrQuestionid.push(<?php echo $results[$i]->id; ?>);
	</script>
	<?php
    }
    $session->set('number_testsection_question', $testsection_question);

    $record = count($results);

    $timeViewAnswer = ($model->getvalueConfig('time_round_math')->value);
    $timeViewAnswer = ($timeViewAnswer > 0) ? $timeViewAnswer : 0;
    $timeQuestion = $timeViewAnswer * $number_random_question;

    function convertTime($timeQuestion) {
	if (!$timeQuestion) {
	    $stringTime = "00:00:00";
	} else {
	    $hour = floor($timeQuestion / 3600);
	    $minute = floor(($timeQuestion - $hour * 3600) / 60);
	    $second = $timeQuestion - $hour * 3600 - $minute * 60;
	    if ($hour < 10) {
		$hour = '0' . $hour;
	    }
	    if ($minute < 10) {
		$minute = '0' . $minute;
	    }
	    if ($second < 10) {
		$second = '0' . $second;
	    }
	    $stringTime = $hour . ':' . $minute . ':' . $second;
	}
	return $stringTime;
    }

    $stringTime = convertTime($timeQuestion);
    ?>
    <div id="wrapper_question" class="fgcmath_test" timeView="<?php echo $timeViewAnswer; ?>" countRecord ="<?php echo $record; ?>">
        <div class="testsectioinstart">
    	<div class="testsecbody">
    	    <h1>
    		Mental Arithmetic Test
    	    </h1>
    	    <p>
    		This is a test for mental arithmetic (math), so do <span class="colred ftbold">NOT</span> use a calculator. The test consists of 15 time limited questions. 
    	    </p>
    	    <br/>
    	    <br/>
    	    <br/>
    	    <p>
    		<span class="colred ftbold">Example 1:</span>
    	    </p>
    	    <br/>
    	    <p>
    		Drag the digits into the gaps so that the predefined result is correct.
    	    </p>
    	    <p>
    		In the example below you need to drag number 7 and 6 to the gray area for correct answer.
    	    </p>
    	    <div>
    		<img src="assets/images/tutorial.gif"/>
    	    </div>
    	    <br/>
    	    <br/>
    	    <br/>
    	    <div style="font-size: 18px; text-align: center;">
    		Average Score: <?php echo getPercentage($total_per, $total_test); ?>
    	    </div>
    	</div>
    	<br/>
    	<br/>
    	<div class="testsecclick">
    	    <a id="testsecstart">&nbsp;</a>
    	</div>
        </div>
        <div class="content_game_math_question">
    	<div id="fgcloading">
    	    <img src="assets/images/ajaxLoader.gif"/>
    	</div>
    	<div class="timeout_game">
    	    <span id ="view_string_time"></span>
    	</div>
    	<div class="testsecexit">
    	    <a href="/math/math.html">&nbsp;</a>
    	</div>
    	<div class ="content_submit" id="tvcontent">
    	    <form id="testdragdrop" method="post" name="answerForm" action="test.php?apps=math&questionid=<?php echo $result->id ?>" AUTOCOMPLETE="OFF">
    		<div id="dragnumber">
			<?php
			$question_content = $result->question_content;
			$arrQuestion_content = explode('$', $question_content);
			$countQuestion = count($arrQuestion_content);
			for ($i = 0; $i < $countQuestion; $i++) {
			    if ($arrQuestion_content[$i] == '?') {
				?>
	    		    <div class="ui-droppable tv-drop" id="num1">&nbsp;<input type="hidden" value="0" name="nums[]" class="tvnums"/></div>
				<?php
			    } else if ($arrQuestion_content[$i] == '=' || $i == ($countQuestion - 1)) {
				?>
	    		    <div class="no-ui-droppable"><?php echo $arrQuestion_content[$i] ?><input type="hidden" value="<?php echo $arrQuestion_content[$i] ?>" name="nums[]"/></div>
				<?php
			    } else {
				?>
	    		    <div class="no-ui-droppable"><?php echo $arrQuestion_content[$i] ?><input type="hidden" value="<?php echo $arrQuestion_content[$i] ?>" name="nums[]"/></div>
				<?php
			    }
			}
			?>
    		</div>
    		<div id="draglistnumber">
			<?php
			$arr_suggest_answer = explode(";", $result->suggest_answer);
			foreach ($arr_suggest_answer as $sa_v) {
			    if ($sa_v != '' && is_numeric($sa_v)) {
				?>
	    		    <div class="ui-droppable" id="card<?php echo $sa_v; ?>" data-value="<?php echo $sa_v; ?>"><div class="ui-draggable card9"><?php echo $sa_v; ?></div></div>
				<?php
			    }
			}
			?>
    		</div>
    		<div class="next_testsection">
    		    <input type="hidden" id="tvfinish" value="0" name="finish"/>
    		    <input type="hidden" id="inputtesttime" value="0" name="inputtesttime"/>
    		    <input type="submit" name="submit" value="Next" class="submitnext"/>
    		</div>
    	    </form>
    	</div>
        </div>
        <img src="assets/images/bg_fsh_title.png" style="display: none"/>
    </div>
    <script type="text/javascript">
        console.log(arrQuestionid)
    </script>
    <?php
}
include_once ('setup-page/footer.tpl.php');
?>