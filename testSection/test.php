<?php
header('P3P: CP="IDC DSP COR ADM DEVi TAIi PSA PSD IVAi IVDi CONi HIS OUR IND CNT"');
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
define('PATH_ROOT', './../');
include_once PATH_ROOT . 'includes.php';

function getPercentage($value1, $value2) {
    if (!$value1 || !$value2)
        return "0%";
    return (round($value1 / $value2, 2) * 100) . "%";
}

class App {

    static $instance;

    function __construct($configs) {

        self::$instance = new Application('site', $configs);
    }

    /**
     *
     * @global <type> $application
     * @return Application
     */
    function &getInstance() {
        return self::$instance;
    }

    function halt($message = null) {
        if (isset(self::$instance))
            self::$instance->halt($message);
        else
            exit($message);
    }

    function __call($method_name, $arguments) {
        if (!method_exists($this, $method_name)) {
            return self::$instance->$method_name();
        }
    }

}

$app = new App($configs);
$application = $app->getInstance();
$application->initialize();
$request = $application->getRequest();
$session = $application->getSession();
$db = $application->getDBO();
$strabc = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
//lay toan bo cau hoi da duoc luu vao session
$allQuestion = $session->get('number_testsection_question', array());
$record = count($allQuestion);
$task = $request->getVar('task', '');
$questionid = $request->getVar('questionid');
//luu cau tra loi cua nguoi dung
$user_answer = array();
$user_answer = $session->get('user_answer');
if (count($user_answer) > 0) {
    foreach ($user_answer as $k => $v) {
        if (!in_array($k, $allQuestion)) {
            if (isset($user_answer[$k])) {
                unset($user_answer[$k]);
            }
        }
    }
}
$ans = $request->getVar('answers');
$user_answer[$questionid] = $ans[0];
$session->set('user_answer', $user_answer);
//lay thoi gian lam bai test
$inputtesttime = $session->get('inputtesttime');
$inputtesttime+=$request->getVar('inputtesttime');
$inputtesttime = $session->set('inputtesttime', $inputtesttime);

//end
$key = @array_search($questionid, $allQuestion);
$questionidnext = $allQuestion[($key + 1)];
$sql = "SELECT * FROM practicetestsection" .
        " WHERE id = '$questionidnext'";
$db->setQuery($sql);
$result = $db->loadObject();
$answers = json_decode($result->answers);
//neu con co cau hoi
?>
<?php if ($questionidnext != null && $questionidnext > 0) { ?>
    <div class="content_question_next">
        <form class="fgcform_answer_math" id="fgcform_testsection" method="post" name="answerForm" action="test.php?apps=testsection&questionid=<?php echo $result->id ?>" AUTOCOMPLETE="OFF">
            <div class="title_game_math">
                <span class="numone"><?php echo $result->content_question ?></span>
            </div>
            <div class ="question_answer">
                <ul class="listanswers">
                    <?php
                    for ($i = 0; $i < count($answers); $i++) {
                        ?>
                        <li>
                            <input class="radioquestion" id="question_<?php echo $i ?>" type="radio" name="answers[]" value="<?php echo $i ?>"/>
                            <label for="question_<?php echo $i ?>">
                                <?php echo $strabc[$i]; ?>)&nbsp;&nbsp;&nbsp;
                                <?php echo $answers[$i] ?>
                            </label>
                        </li>
                        <?php
                    }
                    ?>
                </ul>
            </div>
            <div class="pre_next_testsection">
                <span class="next_testsection">
                    <input type="hidden" id="inputtesttime" value="0" name="inputtesttime"/>
                    <input type="hidden" name="questionid" value="<?php echo $result->id ?>" id="fgcquestionid"/>
                    <input type="submit" name="submit" value="Next"/>
                </span>
            </div>

        </form>
    </div>
<?php } ?>
<?php
if ($questionidnext == null || $questionidnext == '') {
    //lay cau tra loi va tinh cau dung
    $user_answer = $session->get('user_answer');

    $sql = "SELECT * FROM practicetestsection" .
            " WHERE published = 1 AND id IN(" . implode(",", $allQuestion) . ")";
    $db->setQuery($sql);
    $listQuestion = $db->loadObjectList();
    $correct = 0;
    if (count($listQuestion) > 0) {
        foreach ($listQuestion as $value) {
            if ($value->right_answer == $user_answer[$value->id]) {
                $correct++;
            }
        }
    }
    //tinh thoi gian

    $timeQuestion = $session->get('inputtesttime');

    function convertTime($timeQuestion) {
        if (!$timeQuestion) {
            $stringTime = "00:00:00";
        } else {
            $hour = floor($timeQuestion / 3600);
            $minute = floor(($timeQuestion - $hour * 3600) / 60);
            $second = $timeQuestion - $hour * 3600 - $minute * 60;
            if ($hour < 10) {
                $hour = '0' . $hour;
            }
            if ($minute < 10) {
                $minute = '0' . $minute;
            }
            if ($second < 10) {
                $second = '0' . $second;
            }
            $stringTime = $hour . ':' . $minute . ':' . $second;
        }
        return $stringTime;
    }

    $stringTime = convertTime($timeQuestion);

    //Tinh phan tram cau tra loi dung
    $correct_percent = getPercentage($correct, $record);

    //luu vao db
    $sql_insert = "INSERT INTO practicetestsection_report(correct_percent) VALUES('" . substr($correct_percent, 0, strlen($correct_percent) - 1) . "')";
    $db->setQuery($sql_insert);
    $db->query();
    ?>
    <style type="text/css">
        .timeout_game,
        .testsecexit{
            display: none;
        }
    </style>
    <div class="result">
        <div class="result_inner result_math_test">
            <div class="result_title">
            </div>
            <div class="result_complate">
                <label>You Scored:</label> <?php echo $correct . "&nbsp;&nbsp;"; ?>out of<?php echo "&nbsp;&nbsp;" . $record . "&nbsp;&nbsp;"; ?>Questions
            </div>
            <div class="result_percentage">
                <label>You Scored:<span> <?php echo $correct_percent; ?></span></label>
            </div>
            <div class="resutl_time">
                <label>Time Used:<span> <?php echo $stringTime; ?></span></label>
            </div>
            <div class="result_show">
                <a href="#" class="aresult_show">Click Here to view answers!</a>
            </div>
            <div class="retake_testsec">
                <a href="/testSection/testsection.html">&nbsp;</a>
            </div>
        </div>
    </div>
    <div class="fgcresult_show">
        <?php
        for ($k = 0; $k < count($allQuestion); $k++):
            $sql = "SELECT * FROM practicetestsection" .
                    " WHERE published = 1 AND id = '{$allQuestion[$k]}'";
            $db->setQuery($sql, 0, 1);
            if ($resultQuestion = $db->loadObject()) {
                ?>
                <div class="inner">
                    <div class="fgcresult_left">
                        <span>Q.<?php echo $k + 1; ?></span>
                    </div>
                    <div class="fgcresult_right">
                        <div class="fgccontent_question">
                            <?php echo $resultQuestion->content_question; ?>
                        </div>
                        <div class="fgccontent_answers">
                            <ul class="listanswers">
                                <?php
                                $alph = array(
                                    0 => 'A',
                                    1 => 'B',
                                    2 => 'C',
                                    3 => 'D'
                                );
                                $ans = json_decode($resultQuestion->answers);
                                foreach ($ans as $j => $value) {
                                    ?>
                                    <li>
                                        <input class="radioquestion" <?php
                                        if ($user_answer[$resultQuestion->id] == $j && $user_answer[$resultQuestion->id] != null) {
                                            echo 'checked="true"';
                                        }
                                        ?> type="radio"  name="ans<?php echo $resultQuestion->id; ?>[]"/>
                                        <label for="question_<?php echo $i ?>">
                                            <?php echo $alph[$j]; ?>)&nbsp;&nbsp;&nbsp;
                                            <?php echo $value; ?>
                                        </label>
                                        <?php
                                        if ($user_answer[$resultQuestion->id] == $j && $user_answer[$resultQuestion->id] != null) {
                                            if ($user_answer[$resultQuestion->id] == $resultQuestion->right_answer) {
                                                ?>
                                                <img src="assets/images/tick.png"/>
                                                <?php
                                            } else {
                                                ?>
                                                <img src="assets/images/publish_x.png"/>
                                                <?php
                                            }
                                        }
                                        if ($resultQuestion->right_answer == $j && $user_answer[$resultQuestion->id] != $resultQuestion->right_answer) {
                                            ?>
                                            <img src="assets/images/tick.png"/>
                                            <?php
                                        }
                                        ?>
                                    </li>
                                    <?php
                                }
                                ?>
                            </ul>
                        </div>
                        <div class="line text_color text_bold">
                            <span>Correct Answer: <?php echo $alph[$resultQuestion->right_answer]; ?></span>
                        </div>
                    </div>
                </div>
            <?php } endfor; ?>
    </div>
    <?php
}
?>

