<?php
header('P3P: CP="IDC DSP COR ADM DEVi TAIi PSA PSD IVAi IVDi CONi HIS OUR IND CNT"');
include_once ('header_new.tpl.php');
?>
<style type="text/css">  
    body {
        background-position: 0 -73px;
    }
    #header {
        height: 15px;
    }
    .pre_next{
        padding-top: 0;
    }
    .question_answer > div{
        padding-bottom: 20px;
    }
</style> 
<link type="text/css" rel="stylesheet" href="assets/css/style.css"/>
<!--[if IE 7]>
<style type="text/css">
        .radioquestion{
            margin: 0 2px 0 0;
        }
        .testsecexit a,
        #view_string_time{
            display: block;
        }
</style>
<![endif]-->
<?php
$app = App::getInstance();

$db = $app->getDBO();

include_once (PATH_ADMIN_APPS . 'models' . DS . 'config.model.php');
$model = new configModel();

$number_random_question = $model->getvalueConfig('number_random_question_testsection')->value;
if (!$number_random_question) {
    $number_random_question = 10;
}
$sql = "SELECT * FROM practicetestsection" .
        " WHERE published = 1" .
        " ORDER BY RAND()";
$db->setQuery($sql, 0, $number_random_question);
$results = $db->loadObjectList();
//lay so nguoi da tham gia bai thi
$sql_total = "SELECT COUNT(id) AS total FROM practicetestsection_report";
$db->setQuery($sql_total);
$total_test = $db->loadResult();
//lay ti le phan tram
$sql_total_percent = "SELECT SUM(correct_percent) AS total_per FROM practicetestsection_report";
$db->setQuery($sql_total_percent);
$total_per = $db->loadResult();

function getPercentage($value1, $value2) {
    if (!$value1 || !$value2)
        return "0%";
    return (round($value1 / $value2, 2)) . "%";
}

if (empty($results)) {
    ?>
    <div class="noquestion">No Exists question</div>
    <?php
} else {
    ?>
    <div id="conent_javacript">
        <script type="text/javascript" src="assets/js/test_section.js"></script>
    </div>
    <?php
    $result = $results[0];
    $answers = json_decode($result->answers);
    $strabc = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

    $session->clear('inputtesttime');
    $session->clear('user_answer');
    $session->clear('testsection_result_answer');
    $session->clear('number_answer_testsection');
    $session->clear('number_testsection_question', array());
    $testsection_question = array();
    for ($i = 0; $i < count($results); $i++) {
        $testsection_question[] = $results[$i]->id;
        ?>
        <script type="text/javascript">
            arrQuestionid.push(<?php echo $results[$i]->id; ?>);
        </script>
        <?php
    }
    $session->set('number_testsection_question', $testsection_question);

    $record = count($results);

    $timeViewAnswer = ($model->getvalueConfig('time_round_testsection')->value);
    $timeViewAnswer = ($timeViewAnswer > 0)
            ? $timeViewAnswer
            : 0;
    $timeQuestion = $timeViewAnswer * $number_random_question;

    function convertTime($timeQuestion) {
        if (!$timeQuestion) {
            $stringTime = "00:00:00";
        } else {
            $hour = floor($timeQuestion / 3600);
            $minute = floor(($timeQuestion - $hour * 3600) / 60);
            $second = $timeQuestion - $hour * 3600 - $minute * 60;
            if ($hour < 10) {
                $hour = '0' . $hour;
            }
            if ($minute < 10) {
                $minute = '0' . $minute;
            }
            if ($second < 10) {
                $second = '0' . $second;
            }
            $stringTime = $hour . ':' . $minute . ':' . $second;
        }
        return $stringTime;
    }

    $stringTime = convertTime($timeQuestion);
    ?>
    <div id="wrapper_question" class="fgcmath_test" timeView="<?php echo $timeViewAnswer; ?>" countRecord ="<?php echo $record; ?>">
        <div class="testsectioinstart">
            <div class="testsecbody">
                <h1>
                    Mental Arithmetic Test
                </h1>
                <p>
                    This is a test for mental arithmetic (math), so do <span class="colred ftbold">NOT</span> use pen and paper or a calculator. Do not consult others. The test consists of 10 time limited multiple choice questions. 
                </p>
                <br/>
                <br/>
                <br/>
                <p>
                    <span class="colred ftbold">Example 1:</span>
                </p>
                <br/>
                <p style="font-style: italic;">
                    You will see a large equation and you need to find the correct answer.
                </p>
                <p style="font-style: italic;">
                    In this case B is the correct answer.
                </p>
                <div>
                    <img src="assets/images/tutorial.gif"/>
                </div>
                <p>
                    <span class="colred">
                        Please Note:
                    </span>
                    The division sign will be displayed like a colon (:)
                </p>
                <br/>
                <br/>
                <br/>
                <div style="font-size: 18px; text-align: center;">
                    Average Score: <?php echo getPercentage($total_per, $total_test); ?>
                </div>
            </div>
            <br/>
            <br/>
            <div class="testsecclick">
                <a id="testsecstart">&nbsp;</a>
            </div>
        </div>
        <div class="content_game_math_question">
            <div id="fgcloading">
                <img src="assets/images/ajaxLoader.gif"/>
            </div>
            <div class="timeout_game">
                <span id ="view_string_time"></span>
            </div>
            <div class="testsecexit">
                <a href="/testSection/testsection.html">&nbsp;</a>
            </div>
            <div class ="content_submit">
                <form class="fgcform_answer_math" id="fgcform_testsection" method="post" name="answerForm" action="test.php?apps=testsection&questionid=<?php echo $result->id ?>" AUTOCOMPLETE="OFF">
                    <div class="title_game_math">
                        <span class="numone"><?php echo $result->content_question ?></span>
                    </div>
                    <div class ="question_answer">
                        <ul class="listanswers">
                            <?php
                            for ($i = 0; $i < count($answers); $i++) {
                                ?>
                                <li>
                                    <input class="radioquestion" id="question_<?php echo $i ?>" type="radio" name="answers[]" value="<?php echo $i ?>"/>
                                    <label for="question_<?php echo $i ?>">
                                        <?php echo $strabc[$i]; ?>)&nbsp;&nbsp;&nbsp;
                                        <?php echo $answers[$i] ?>
                                    </label>
                                </li>
                                <?php
                            }
                            ?>
                        </ul>
                    </div>
                    <div class="pre_next_testsection">
                        <span class="next_testsection">
                            <input type="hidden" id="inputtesttime" value="0" name="inputtesttime"/>
                            <input type="hidden" name="questionid" value="<?php echo $result->id ?>" id="fgcquestionid"/>
                            <input type="submit" name="submit" value="Next"/>
                        </span>
                    </div>
                </form>
            </div>
        </div>
        <img src="assets/images/bg_fsh_title.png" style="display: none"/>
    </div>
    <script type="text/javascript">
        console.log(arrQuestionid)
    </script>
    <?php
}
include_once ('setup-page/footer.tpl.php');
?>