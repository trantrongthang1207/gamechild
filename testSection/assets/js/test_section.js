/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
var arrQuestionid = [];
function getKeyByValue(value, arr) {
    for (var prop in arr) {
        if (arr.hasOwnProperty(prop)) {
            if (parseInt(arr[prop]) === parseInt(value)) {
                return prop;
            }
        }
    }
}
var noanswer = false;
jQuery(document).ready(function($) {
    var timeout = $("#wrapper_question").attr("timeview");
    var minutes = "";
    var seconds = "";
    timeout = parseInt(timeout);
    minutes = "" + parseInt((timeout / 60));
    seconds = ":" + (timeout % 60);
    var record = $("#wrapper_question").attr('countrecord');
    $(document).delegate("form#fgcform_testsection", "submit", function(e) {
        e.preventDefault();
        var url = $(this).attr("action");
        var questionid = $("#fgcquestionid").val();

        var radioquestion = $('.radioquestion');
        var checkanswer = false;
        for (var i = 0, len = radioquestion.length; i < len; i++) {
            if ($(radioquestion[i]).is(":checked")) {
                checkanswer = true;
            }
        }
        if (checkanswer === true && noanswer == true) {
            $("#fgcloading").show();
            $.ajax({
                url: url,
                type: "post",
                data: $(this).serialize(),
                dataType: "html",
                success: function(response) {
                    $(".content_submit").html(response);
                    $(".content_game_math_question").show(function() {
                        //console.log(getKeyByValue(questionid, arrQuestionid))
                        if (typeof getKeyByValue(questionid, arrQuestionid) != 'undefined') {
                            cd(minutes, seconds);
                        } else {
                            $(".timeout_game").css({
                                display: "none"
                            })
                        }
                    });
                    $("#fgcloading").hide();
                }
            })
        } else if (checkanswer === true && noanswer == false) {
            $("#fgcloading").show();
            $.ajax({
                url: url,
                type: "post",
                data: $(this).serialize(),
                dataType: "html",
                success: function(response) {
                    $(".content_submit").html(response);
                    $(".content_game_math_question").show(function() {
                        //console.log(getKeyByValue(questionid, arrQuestionid))
                        if (typeof getKeyByValue(questionid, arrQuestionid) != 'undefined') {
                            cd(minutes, seconds);
                        } else {
                            $(".timeout_game").css({
                                display: "none"
                            })
                        }
                    });
                    $("#fgcloading").hide();
                }
            })
        } else if (checkanswer === false && noanswer == true) {
            $("#fgcloading").show();
            $.ajax({
                url: url,
                type: "post",
                data: $(this).serialize(),
                dataType: "html",
                success: function(response) {
                    $(".content_submit").html(response);
                    $(".content_game_math_question").show(function() {
                        //console.log(getKeyByValue(questionid, arrQuestionid))
                        if (typeof getKeyByValue(questionid, arrQuestionid) != 'undefined') {
                            cd(minutes, seconds);
                        } else {
                            $(".timeout_game").css({
                                display: "none"
                            })
                        }
                    });
                    $("#fgcloading").hide();
                }
            })
            noanswer = false;
        } else if (checkanswer == false) {
            alert("Please choose answer!");
        }
    })

    $(document).delegate(".aresult_show", "click", function(e) {
        e.preventDefault();
        $(".fgcresult_show").show();
    })

    $("#testsecstart").click(function(e) {
        e.preventDefault();
        $(".testsectioinstart").slideUp(500);
        $(".content_game_math_question").slideDown(500)
		$('html,body').animate({
            scrollTop: 0 + 'px'
        }, 700);
        cd(minutes, seconds);
    })

    var mins
    var secs;

    function cd(min, sec) {
        mins = 1 * m(min); // change minutes here
        secs = 0 + s(sec); // change seconds here (always add an additional second to your total)
        redo();
    }

    function m(obj) {
        for (var i = 0; i < obj.length; i++) {
            if (obj.substring(i, i + 1) == ":")
                break;
        }
        return(obj.substring(0, i));
    }

    function s(obj) {
        for (var i = 0; i < obj.length; i++) {
            if (obj.substring(i, i + 1) == ":")
                break;
        }
        return(obj.substring(i + 1, obj.length));
    }

    function dis(mins, secs) {
        var disp;
        if (mins <= 9) {
            disp = " 0";
        } else {
            disp = " ";
        }
        disp += mins + ":";
        if (secs <= 9) {
            disp += "0" + secs;
        } else {
            disp += secs;
        }
        return(disp);
    }

    function redo() {
        secs--;
        if (secs == -1) {
            secs = 59;
            mins--;
        }
        if ($(".timeout_game").is(":hidden")) {
            return;
        }
        $("#view_string_time").html(dis(mins, secs) + '<span id="sec">sec</span>'); // setup additional displays here.
        $("#inputtesttime").val(parseInt($("#inputtesttime").val()) + 1);
        if ((mins == 0) && (secs == 0)) {
            var radioquestion = $('.radioquestion');
            var checkanswer = false;
            for (var i = 0, len = radioquestion.length; i < len; i++) {
                if ($(radioquestion[i]).is(":checked")) {
                    checkanswer = true;
                }
            }
            if (!checkanswer) {
                ans = window.confirm('You did not answer the recent question, do you want to continue?');
                if (ans == true) {
                    noanswer = true;
                    $("form#fgcform_testsection").submit();
                } else {
                    window.location = 'testsection.html';
                }
            } else {
                $("form#fgcform_testsection").submit();
            }
        } else {
            //cd = setTimeout("redo()",1000);
            if (window.timeout_submit)
                clearTimeout(window.timeout_submit);
            window.timeout_submit = window.setTimeout(function() {
                redo();
            }, 1000)
        }
    }


})




