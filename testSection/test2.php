<?php
define('PATH_ROOT', './../');
include_once PATH_ROOT . 'includes.php';

function getPercentage($value1, $value2) {
    if (!$value1 || !$value2)
        return "0%";
    return (round($value1 / $value2, 2) * 100) . "%";
}

class App {

    static $instance;

    function __construct($configs) {

        self::$instance = new Application('site', $configs);
    }

    /**
     *
     * @global <type> $application
     * @return Application
     */
    function &getInstance() {
        return self::$instance;
    }

    function halt($message = null) {
        if (isset(self::$instance))
            self::$instance->halt($message);
        else
            exit($message);
    }

    function __call($method_name, $arguments) {
        if (!method_exists($this, $method_name)) {
            return self::$instance->$method_name();
        }
    }

}

$app = new App($configs);
$application = $app->getInstance();
$application->initialize();
$request = $application->getRequest();
$session = $application->getSession();
print_r($session->get('test'));