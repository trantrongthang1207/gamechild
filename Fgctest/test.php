<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>PHPMailer - sendmail test</title>
</head>
<body>
<?php
require 'PHPMailer/PHPMailerAutoload.php';

//Create a new PHPMailer instance
$mail = new PHPMailer();
// Set PHPMailer to use the sendmail transport
$mail->isSendmail();
//Set who the message is to be sent from
$mail->setFrom('test1.hoangbien@gmail.com', 'Hoang Bien');
//Set an alternative reply-to address
$mail->addReplyTo('hoangbien264@gmail.com', 'Hoang Bien Admin');
//Set who the message is to be sent to
$mail->addAddress('kemly.vn@gmail.com', 'Ngoc Hang');
//Set the subject line
$mail->Subject = 'PHPMailer sendmail test';
//Read an HTML message body from an external file, convert referenced images to embedded,
//convert HTML into a basic plain-text alternative body

//$mail->msgHTML(file_get_contents('contents.html'), dirname(__FILE__));
$content = "<p>Dear Gerry ,</p>
                            <p><b>Credit Card Payment Confirmation</b></p>
                            <p>This is an automated email receipt to notify you that your recent invoice GFake1301 from Tutoring Advantage has been processed to your registered credit card.</p>
                                <table>
                                <tbody><tr>
                                    <td>Status:</td>
                                    <td>Approved</td>
                                </tr>
                                <tr>
                                    <td>Invoice Number:&nbsp;&nbsp;&nbsp;</td>
                                    <td>GFake1301</td>
                                </tr>
                                <tr>
                                    <td>Amount Paid:</td>
                                    <td>$100.49</td>
                                </tr>
                                <tr>
                                    <td>Date Paid:</td>
                                    <td>21/07/2014</td>
                                </tr>
                                </tbody></table>
                                <p>If you have any questions, please reply to this email or contact us on the details below.</p>
                                <p>Thank you,</p>
                                <p>Tutoring Advantage<br>
                                T: 02 8072 3961 <br>  
                                F: 02 8072 3962 <br>  
                                E: <a href='mailto:info@tutoringadvantage.com.au'>info@tutoringadvantage.com.au</a>
                                </p><br>";
$mail->msgHTML($content);
//Replace the plain text body with one created manually
$mail->AltBody = 'This is a plain-text message body';
//Attach an image file
$mail->addAttachment('invoice-ATest1301.pdf');

//send the message, check for errors
if (!$mail->send()) {
    echo "Mailer Error: " . $mail->ErrorInfo;
} else {
    echo "Message sent!";
}
?>
</body>
</html>