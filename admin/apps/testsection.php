<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
include_once (PATH_APPS . 'models' . DS . 'testsection.model.php');
include_once (PATH_APPS . 'controllers' . DS . 'testsection.controller.php');
$app = App::getInstance();
$request = $app->getRequest();
$task = $request->getVar('task', '');
switch ($task) {
    case 'save':
        $controller = new testsectionController();
        $controller->save();
        break;
    case 'delete':
        $controller = new testsectionController();
        $controller->delete();
        break;
    case 'save_config':
        $controller = new testsectionController();
        $controller->save_config();
        break;
    case 'published':
        $controller = new testsectionController();
        $controller->published();
        break;
    case 'deletereport':
        $controller = new testsectionController();
        $controller->deletereport();
        break;
    default :
        include_once (PATH_APPS . 'views' . DS . 'testsection' . DS . 'testsection.view.php');
        break;
}
?>