<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class helper {

    function getObjectByID($array, $key = '', $value) {
        if ($key == '') {
            for ($i = 0; $i < count($array); $i++) {
                if ($array[$i] == $value)
                    return true;
            }
        } else {
            for ($i = 0; $i < count($array); $i++) {
                if ($array[$i]->$key == $value)
                    return true;
            }
        }
        return false;
    }

    function showSelect($array, $name, $title, $value, $class, $id_name = 'select_option', $selected = '', $default = '-Choose a Object-') {
        $html = '';
        if (!empty($array)) {
            $html = "<select name='$name' class='$class' id='$id_name'>";
            if (!self::getObjectByID($array, $value, $selected)) {
                $html .= "<option value='0'>$default</option>";
            }
            for ($i = 0; $i < count($array); $i++) {
                if ($array[$i]->$value == $selected) {
                    $html .= "<option value='" . $array[$i]->$value . "' selected >" . $array[$i]->$title . "</option>";
                } else {
                    $html .= "<option value='" . $array[$i]->$value . "' >" . $array[$i]->$title . "</option>";
                }
            }
            $html .="</select>";
        }
        return $html;
    }

    function displayeditor($name, $row, $cols, $content, $urlsite) {
        $url = $urlsite . 'apps/helpers';

        $script = "<script type='text/javascript' src='http://www.google.com/jsapi'></script>
<script type='text/javascript'>
    google.load('jquery', '1');
</script>

<!-- Load TinyMCE -->
<script type='text/javascript' src='$url/tiny_mce/jquery.tinymce.js'></script>
<script type='text/javascript'>
    $().ready(function() {
        $('textarea.tinymce').tinymce({
            // Location of TinyMCE script
            script_url : '$url/tiny_mce/tiny_mce.js',

            // General options
            mode : 'textareas',
            theme : 'advanced',
            plugins : 'pagebreak,style,layer,table,save,advhr,advlink,advimage,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,advlist',

            // Theme options
            theme_advanced_buttons1 : 'bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect',
            theme_advanced_buttons2 : 'bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor',
            theme_advanced_buttons3 : 'tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen',
            theme_advanced_buttons4 : 'insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak',

            theme_advanced_toolbar_location : 'top',
            theme_advanced_toolbar_align : 'left',
            theme_advanced_statusbar_location : 'bottom',
            theme_advanced_resizing : true,

            // Example content CSS (should be your site CSS)
            content_css : 'css/content.css',

            // Drop lists for link/image/media/template dialogs
            template_external_list_url : 'lists/template_list.js',
            external_link_list_url : 'lists/link_list.js',
            external_image_list_url : 'lists/image_list.js',
            media_external_list_url : 'lists/media_list.js',

            // Replace values for the template plugin
            template_replace_values : {
                username : 'Some User',
                staffid : '991234'
            }
        });
    });
</script>
<!-- /TinyMCE -->

<textarea id='$name' name='$name' rows='$row' cols='$cols' class='tinymce'>
    $content
 </textarea>";
        return $script;
    }

    function display_editorTiny($name, $row, $cols, $content, $urlsite, $class_name) {
        $url = $urlsite . 'apps/helpers';

        $script = "<script type='text/javascript' src='http://www.google.com/jsapi'></script>
<script type='text/javascript'>
    google.load('jquery', '1');
</script>

<!-- Load TinyMCE -->
<script type='text/javascript' src='$url/tiny_mce/jquery.tinymce.js'></script>
<script type='text/javascript'>
    $().ready(function() {
        $('textarea.$class_name').tinymce({
            // Location of TinyMCE script
            script_url : '$url/tiny_mce/tiny_mce.js',

            // General options
            mode : 'textareas',
            theme : 'advanced',
            plugins : 'pagebreak,style,layer,table,save,advhr,advlink,advimage,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,advlist',

            // Theme options
            theme_advanced_buttons1 : 'bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect',
            theme_advanced_buttons2 : 'bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor',
            theme_advanced_buttons3 : 'tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen',
            theme_advanced_buttons4 : 'insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak',

            theme_advanced_toolbar_location : 'top',
            theme_advanced_toolbar_align : 'left',
            theme_advanced_statusbar_location : 'bottom',
            theme_advanced_resizing : true,

            // Example content CSS (should be your site CSS)
            content_css : 'css/content.css',

            // Drop lists for link/image/media/template dialogs
            template_external_list_url : 'lists/template_list.js',
            external_link_list_url : 'lists/link_list.js',
            external_image_list_url : 'lists/image_list.js',
            media_external_list_url : 'lists/media_list.js',

            // Replace values for the template plugin
            template_replace_values : {
                username : 'Some User',
                staffid : '991234'
            }
        });
    });
</script>
<!-- /TinyMCE -->

<textarea id='$name' name='$name' rows='$row' cols='$cols' class='$class_name'>
    $content
 </textarea>";
        return $script;
    }

    function display_wysiwyg($name, $row, $cols, $content, $urlsite) {
        $url = $urlsite . 'apps/helpers';

        $script = "
        <link rel='stylesheet' href='$url/openwysiwyg_v1.4.7/docs/style.css' type='text/css'>
<script type='text/javascript' src='$url/openwysiwyg_v1.4.7/scripts/wysiwyg.js'></script>
<script type='text/javascript' src='$url/openwysiwyg_v1.4.7/scripts/wysiwyg-settings.js'></script>
<script type='text/javascript'>
    WYSIWYG.attach($name);
</script>

<textarea id='$name' name='$name' rows='$row' cols='$cols'>
    $content
 </textarea>";
        return $script;
    }

    function showListRadio($array, $name, $value, $title, $checked, $class = '') {
        $html = '';
        if (!empty($array)) {
            $html .="<div class='{$class}'>";
            for ($i = 0; $i < count($array); $i++) {
                if ($array[$i]->$value == $checked) {
                    $html .="<input type='radio' name='{$name}' value='{$array[$i]->$value}' CHECKED/>{$array[$i]->$title}";
                } else {
                    $html .="<input type='radio' name='{$name}' value='{$array[$i]->$value}'/>{$array[$i]->$title}";
                }
            }
            $html.="</div>";
        }
        return $html;
    }

    function showListcheck($name, $value) {
        $html = '';
        if ($value >= 0) {
            $html .="<div class='{$class}'>";
            if ($value == 1) {
                $html .="<input type='checkbox' name='{$name}' value='{$value}' CHECKED/>";
            } else {
                $html .="<input type='checkbox' name='{$name}' value='{$value}'/>";
            }
            $html.="</div>";
        }
        return $html;
    }

    function format_date($date, $format = '') {
        if ($date != '' && $date != '0000-00-00' && $date != '0000-00-00 00:00:00') {
            $date = date_create($date);
            if ($forma != '')
                return date_format($date, 'd/m/Y H:i:s');
            return date_format($date, $format);
        }
        return '';
    }

    function Paging($total, $item_pager, $apps = '', $subtask = '') {
        if ($apps == '') {
            $app = $_REQUEST['apps'];
        } else {
            $app = $apps;
        }
        $page = intval($_REQUEST['page']);
        if ($page == "") {
            $page = "1";
        }
        $currentpage = $page;

        if ($page >= 2) {
            $pagingstart = ($page - 1) * $item_pager;
        } else {
            $pagingstart = "0";
        }
        $toppage = ceil($total / $item_pager);
        $pagelinks = "";
        $theprevpage = $currentpage - 1;
        $thenextpage = $currentpage + 1;

        if ($currentpage > 0) {
            if ($currentpage > 1) {
                $pagelinks.="<a href='index.php?apps={$app}&page=1&$subtask'>First</a>&nbsp;";
                $pagelinks.="...&nbsp;";
                $pagelinks.="<a href='index.php?apps={$app}&page=$theprevpage&$subtask'>Prev</a>&nbsp;";
            }
            $counter = 0;
            $lowercount = $currentpage - 5;
            if ($lowercount <= 0)
                $lowercount = 1;

            while ($lowercount < $currentpage) {
                $pagelinks.="<a href='index.php?apps={$app}&page=$lowercount&$subtask'>$lowercount</a>&nbsp;";
                $lowercount++;
                $counter++;
            }

            $pagelinks.="<a class='selected'>$currentpage</a>&nbsp;";

            $uppercounter = $currentpage + 1;

            while (($uppercounter < $currentpage + 10 - $counter) && ($uppercounter <= $toppage)) {
                $pagelinks.="<a href='index.php?apps={$app}&page=$uppercounter&$subtask'>$uppercounter</a>&nbsp;";
                $uppercounter++;
            }

            if ($currentpage < $toppage) {
                $pagelinks.="<a href='index.php?apps={$app}&page=$thenextpage&$subtask'>" . "Next" . " &raquo;</a>&nbsp;";
                $pagelinks.="...&nbsp;&nbsp;";
                $pagelinks.="<a href='index.php?apps={$app}&page=$toppage&$subtask'>" . "Last" . "</a>&nbsp;";
            }
        }
        if ($pagelinks != "" && $toppage > 1) {
            return "<div class='paging_record_item'>" . $pagelinks . "</div>";
        }
        return "";
    }

    function showSelectTime($unit_time, $name, $class, $id_name = 'select_option', $selected = '', $default = '-Choose a Object-') {
        $array = array();
        switch ($unit_time) {
            case 'd':
                unset($array);
                for ($i = 1; $i <= 31; $i++) {
                    $array[$i - 1] = $i;
                }
                break;
            case 'm':
                unset($array);
                for ($i = 1; $i <= 12; $i++) {
                    $array[$i - 1] = $i;
                }
                break;
            case 'y':
                unset($array);
                for ($i = 1; $i <= 10; $i++) {
                    $array[$i - 1] = $i;
                }
                break;
            default :
                unset($array);
                for ($i = 1; $i <= 10; $i++) {
                    $array[$i - 1] = $i;
                }
                break;
        }
        $html = '';
        if ($selected > count($array) || $selected == '') {
            $selected = 1;
        }
        if (!empty($array)) {
            $html = "<select name='$name' class='$class' id='$id_name' style='width=40px '>";
            if (!self::getObjectByID($array, '', $selected)) {
                $html .= "<option value='0'>$default</option>";
            }
            for ($i = 0; $i < count($array); $i++) {
                if ($array[$i] == $selected) {
                    $html .= "<option value='" . $array[$i] . "' selected >" . $array[$i] . "</option>";
                } else {
                    $html .= "<option value='" . $array[$i] . "' >" . $array[$i] . "</option>";
                }
            }
            $html .="</select>";
        }
        return $html;
    }

    function getAllTypeQuestion($db) {
        /* @var $db Mysql */
        $sql = "SELECT * FROM type_questions WHERE 1";
        $db->setQuery($sql);
        if ($result = $db->loadObjectList())
            return $result;
        return array();
    }

    function shoPosition($value) {
        $arrayposition = array('Bottomleft', 'Bottomcenter', 'Bottomright', 'Centerleft', 'Center', 'Centerright', 'Topleft', 'Topcenter', 'Topright');
        if ($value > 0) {
            $name = $arrayposition[$value - 1];
        } else {
            $name = 'No';
        }
        return $name;
    }

    function getarrayPosition($value) {
        if (($value != '' || $value != null) && $value != 'no') {
            $array = explode('-', $value);

            $returnthis = array();
            for ($i = 0; $i < count($array); $i++) {
                $string = $array[$i];
                if ($string != '') {
                    $arrayvalue = explode('|', $string);
                    $object = new stdClass();
                    $object->location = $arrayvalue[0];
                    $object->position = $arrayvalue[1];
                    $object->timeout = $arrayvalue[2];
                    $object->order = $arrayvalue[3];
                    $returnthis[$object->location . '_' . $object->position] = $object;
                }
            }
            return $returnthis;
        } else {
            return array();
        }
    }

    function convertToStringPosition($array, $valuedefault, $orderdefault = 1) {
        $string = '';
        $arrayKey = array('right_1', 'right_2', 'right_3', 'right_4', 'right_5', 'right_6', 'right_7', 'right_8', 'right_9', 'left_1', 'left_2', 'left_3', 'left_4', 'left_5', 'left_6', 'left_7', 'left_8', 'left_9');
        $newArray = array();
        foreach ($array as $key => $value) {
            if (in_array($key, $arrayKey) && $value > 0) {
                if ($key == ('right_' . $value)) {
                    $time = (intval($array['right_input_' . $value]) > 0) ? intval($array['right_input_' . $value]) : $valuedefault;
                    $order = (intval($array['right_order_' . $value]) > 0) ? intval($array['right_order_' . $value]) : $orderdefault;
                    $value = "R|" . $value . "|" . $time . "|" . $order;
                    $newArray[] = $value;
                } else if ($key == ('left_' . $value)) {
                    $time = (intval($array['left_input_' . $value]) > 0) ? intval($array['left_input_' . $value]) : $valuedefault;
                    $order = (intval($array['left_order_' . $value]) > 0) ? intval($array['left_order_' . $value]) : $orderdefault;
                    $value = "L|" . $value . "|" . $time . '|' . $order;
                    $newArray[] = $value;
                }
            }
        }
        if (count($newArray) <= 0) {
            return 'no';
        } else {
            $newArray = self::sortArray($newArray);
            $string = implode('-', $newArray);
            return $string;
        }
    }

    function sortArray($array) {
        $array = $array;
        for ($i = 0; $i < count($array); $i++) {
            for ($j = $i + 1; $j < count($array); $j++) {
                $value1 = explode('|', $array[$i]);
                $value2 = explode('|', $array[$j]);
                if ($value1[3] > $value2[3]) {
                    $temp = $array[$i];
                    $array[$i] = $array[$j];
                    $array[$j] = $temp;
                }
            }
        }
        return $array;
    }

    function uploadImage($pathFile, $key = 'image1', $filezie = 10) {
        //Trong truong hop ko chon anh
        if ($_FILES[$key]["error"] > 0)
            return array('error' => "Return Code: " . $_FILES[$key]["error"], 'image' => '');
        //Bat dau thuc hien upload anh
        $allowedExts = array(".gif", ".jpeg", ".jpg", ".png");
        $arrayTypeFile = array("image/gif", "image/jpeg", "image/jpg", "image/x-png", "image/pjpeg", "image/png");
        $extension = strtolower(substr($_FILES[$key]['name'], strrpos($_FILES[$key]['name'], '.')));
        if (in_array($_FILES[$key]["type"], $arrayTypeFile) && ($_FILES[$key]["size"] < $filezie * 1024 * 1024) && in_array($extension, $allowedExts)) {
//            if ($_FILES[$key]["error"] > 0) {
//                $error = "Return Code: " . $_FILES[$key]["error"];
//                $image = '';
//            } else {
                $filename = time() . '-' . $_FILES[$key]["name"];
                if (move_uploaded_file($_FILES[$key]["tmp_name"], $pathFile . DS . $filename)) {
                    // $image = $random . '-' . $_FILES["images"]["name"];
                    $error = "";
                    $image = $filename;
                } else {
                    $error = 'Error upload image';
                    $image = '';
                }
         //   }
        } else {
            $error = "File not exit or largest file size";
            $image = '';
        }
        return array('error' => $error, 'image' => $image);
    }

    function renderSelectPosition($postion_slected, $right_postion) {
        global $arrayPostions;
        ?>
        <table class="listposition">
            <tr>
                <th width="8%">Choose position</th>
                <th width="5%"> Positions </th>
                <th width="37%">Right position</th>
            </tr>
            <?php foreach ($arrayPostions as $postion) {
                ?>
                <tr>
                    <td><input type="checkbox" name="postion<?php echo $postion['position'] ?>" 
                               <?php if (in_array($postion['position'], $postion_slected)): ?>checked="checked"<?php endif; ?> value="<?php echo $postion['position']; ?>"></td>
                    <td><?php echo $postion['text']; ?></td>
                    <td><input type="radio" name="right_postion" value="<?php echo $postion['position']; ?>" 
                               <?php if ($postion['position'] == $right_postion): ?>checked="checked"<?php endif; ?> /></td>
                </tr>
            <?php } ?>

        </table>
        <?php
    }

    function renderPostionAnswer($stringPost) {
        if (!$stringPost)
            echo '';
        $arrayPostion = explode(";", $stringPost);
        foreach ($arrayPostion as $postion) {
            ?>
            <p>Position <?php echo $postion; ?></p>
            <?php
        }
    }

}
?>
