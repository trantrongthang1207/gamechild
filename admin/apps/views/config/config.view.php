<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
$title_manager = "Config manager";
$app = App::getInstance();
$request = $app->getRequest();
$msg = $request->getVar('msg', '');
$time_unit1 = new stdClass();
$time_unit1->value = "m";
$time_unit1->name = "Month";
$time_unit2 = new stdClass();
$time_unit2->value = "d";
$time_unit2->name = "Day";
$time_unit3 = new stdClass();
$time_unit3->value = "y";
$time_unit3->name = "Year";
$time_units[] = $time_unit2;
$time_units[] = $time_unit1;
$time_units[] = $time_unit3;
$end_time = $model->getvalueConfig('end_time')->value;
$end_time = explode(':', $end_time);
$unit = $end_time[1];
$time_value = $end_time[0];
$lists['end_time'] = helper::showSelectTime($unit, 'end_time','select_end_time', 'select_option_time', $time_value, '-Select Value Time-');
$lists['time_unit'] = helper::showSelect($time_units, 'time_unit', 'name', 'value', "select_time_unit","select_option_unit",$unit,'-Select Time Unit-');
include_once (PATH_APPS . 'views' . DS . 'config' . DS . 'update_config_default.php');
?>
