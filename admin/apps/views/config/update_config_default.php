<script type="text/javascript" src="<?php echo URL_TEMPLATE ?>js/fgc.js.checkbox.js"></script>
<script type="text/javascript" src="<?php echo URL_TEMPLATE ?>js/submit.js"></script>
<div id="top">
    <div class="top_left incom-48-user">
        <span class="title_manager"><?php echo $title_manager; ?></span>
        <span class="message_manager"><?php echo $msg; ?></span>
    </div>
    <div class="top_right">
        <table class="toolbar">
            <tr>
                <td id="toolbar-save" class="button">
                    <a class="toolbar" href="javascript: submitform()">
                        <span title="Save" class="icon-32-save">
                        </span>
                        Save
                    </a>
                </td>

                <td id="toolbar-cancel" class="button">
                    <a class="toolbar" href="index.php?apps=config">
                        <span title="Cancel" class="icon-32-cancel">
                        </span>
                        Cancel
                    </a>
                </td>
        </table>

    </div>
</div>
<div id="fgc_content">
    <form id="frm_user_edit" name="adminForm" action="index.php?apps=config&task=save" method="post">
        <div id="config-pay">
            <label class="config_paypal">Config for paypal</label>
            <div>
                <label class="fgc_config">Time to see interview for pay once:</label>
                <div class="value_time_config"><?php echo $lists['end_time']; ?></div>  <?php echo $lists['time_unit']; ?>
            </div>
            <div>
                <label class="fgc_config">Test mode:</label>
                <input id="fgc_config"type="checkbox" name="paypal_testmode" <?php if ($model->getvalueConfig('paypal_testmode')->value == 1): ?>checked<?php endif; ?>/>
            </div>
            <div>
                <label class="fgc_config">Seller email:</label>
                <input id="fgc_config_email" class="required email" type="text" name="paypal_seller_email" value="<?php echo $model->getvalueConfig('paypal_seller_email')->value ?>"/>
            </div>
        </div>
        <div id="config-page-site">
            <label class="config_site">Config for site</label>
            <div>
                <label class="fgc_config">Number of records per page:</label><input class="required number" type="text" name="item_pager" value="<?php echo $model->getvalueConfig('item_pager')->value ?>"/>
            </div>
            <div>
                <label class="fgc_config">Email site:</label><input id="email_site" class="required email" type="text" name="email_site" value="<?php echo $model->getvalueConfig('email_site')->value ?>"/>
            </div>
        </div>
<!--        <input type="hidden" name="name" value="<?php //echo $config->name;         ?>"/>-->

        <style type="text/css">
            .formsetfb {
                color: #4C4748;
                font-weight: bold;
            }
            .formsetfb label {
                float: left;
                width: 200px;
            }
            .fgcline {
                padding: 5px 0;
            }
            .color2375E0{
                color: #2375E0;
            }
            .note {
                color: #000000;
                padding: 5px 0;
            }
            .formsetfb input {
                border: 1px solid #CCCCCC;
                border-radius: 2px 2px 2px 2px;
                padding: 5px;
                width: 200px;
            }
            .notetop {
                margin-bottom: 30px;
            }
            .colorscheme {
                border: 1px solid #CCCCCC;
                border-radius: 2px 2px 2px 2px;
                width: 202px;
            }
        </style>
        <div class="formsetfb">
            <div class="inner">
                <div class="contenttop">
                    <h1>Create One for Your Site or Fan Page Now: </h1>
                    <div class="notetop">Fill out all Fields Below. Any URL should have http://</div>
                    <div class="fgcline">
                        <label class="color2375E0">Facebook Like URL</label>
                        <input type="text" class="fburl" name="fburl" value="<?php echo $model->getvalueConfig('fburl')->value ?>"/>
                    </div>
                    <div class="fgcline">
                        <label class="color2375E0">Link to Download</label>
                        <input type="text" class="linkdownload" name="linkdownload" value="<?php echo $model->getvalueConfig('linkdownload')->value ?>"/>
                    </div>
                    <div class="fgcline">
                        <label class="color2375E0">Width</label>
                        <input type="text" class="imagewidth" name="imagewidth" value="<?php echo $model->getvalueConfig('imagewidth')->value ?>"/>
                    </div>
                    <div class="fgcline">
                        <label>Color Scheme</label>
                        <select name="colorscheme" class="colorscheme">
                            <option value="1">Light</option>
                            <option value="0">Dark</option>
                        </select>
                        <div class="note">Hint: choose Light if you are placing this on a Dark background</div>
                    </div>
                    <div class="fgcline">
                        <label>Email</label>
                        <input type="text" class="fbemail" name="fbemail" value="<?php echo $model->getvalueConfig('fbemail')->value ?>"/>
                    </div>
                </div>

                <div class="contentbutton">

                </div>
            </div>
        </div>
    </form>
</div>
