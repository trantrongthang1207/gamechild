<script type="text/javascript" src="<?php echo URL_TEMPLATE ?>js/fgc.js.checkbox.js"></script>
<script type="text/javascript" src="<?php echo URL_TEMPLATE ?>js/submit.js"></script>
<div id="top">
    <div class="top_left incom-48-user"><?php echo $msg; ?></div>
    <div class="top_right">
        <table class="toolbar">
            <tr>
                <td id="toolbar-save" class="button">
                    <a class="toolbar" href="javascript: submitform()">
                        <span title="Save" class="icon-32-save">
                        </span>
                        Save
                    </a>
                </td>

                <td id="toolbar-cancel" class="button">
                    <a class="toolbar" href="index.php?apps=testscore">
                        <span title="Cancel" class="icon-32-cancel">
                        </span>
                        Cancel
                    </a>
                </td>
        </table>
    </div>
</div>
<div id="fgc_content">
    <form id="frm_user_edit" name="adminForm" action="index.php?apps=testscore&task=save" method="post" enctype='multipart/form-data'>
        <div>
            <label class="lab_user">Image 1:</label><br/>
            <input type="file" name="image1" id="image1"/>
            <?php if (@$question->image1!=""): ?>
                <img style="width: 400px; max-height: 200px" src="<?php echo URL_STIE . '../testScore/uploadfiles/' . $question->image1; ?>"/>
            <?php endif; ?>
        </div>  
        <div>
            <label class="lab_user">Image 2:</label>
            <br/>
            <input type="file" name="image2" id="image2"/>
            <?php if (@$question->image2!=""): ?>
                <img style="width: 400px; max-height: 200px" src="<?php echo URL_STIE . '../testScore/uploadfiles/' . $question->image2; ?>"/>
            <?php endif; ?>
        </div>
        <div>
            <label class="lab_user">Published:</label>
            <?php echo $lists['published']; ?>
        </div>
        <input type="hidden" name="question_id" value="<?php echo $id; ?>"/>
    </form>
</div>