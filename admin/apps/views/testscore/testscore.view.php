<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
$app = App::getInstance();
$request = $app->getRequest();
$task = $request->getVar('task', '');
$pathview = PATH_APPS . 'views' . DS . 'testscore' . DS;
include_once (PATH_APPS . 'helpers' . DS . 'helper.php');
$model = new testscoreModel();

switch ($task) {
    case 'edit':
        $id = isset($_REQUEST['cid']) ? $_REQUEST['cid'] : '0';
        if(is_array($id)){
            $id = $id[0];
        }
        
        if ($id > 0) {
            $msg = "Edit question";
        } else {
            $msg = "Add new question";
        }
        $question = $model->getQuestion();
        //$answer = $model->getAnswer();

        if ($id > 0) {
            $default = $question->published;
        } else {
            $default = 1;
        }
        $object1 = new stdClass();
        $object1->value = '0';
        $object1->name = 'No';
        $object2 = new stdClass();
        $object2->value = '1';
        $object2->name = 'Yes';
        $array[] = $object1;
        $array[] = $object2;

        $lists['published'] = helper::showListRadio($array, 'published', 'value', 'name', $default, 'radio_question_game');

        //$editor = helper::displayeditor('content_text', 20, 20, stripslashes($question->content_text), URL_STIE);

        include_once ($pathview . 'add_edit_testscore_question_default.php');
        break;
    case 'editpq':
        $id = isset($_REQUEST['cid']) ? $_REQUEST['cid'] : '0';
        $id_q = @$_REQUEST['id'];
        $id = @$id[0];

        if ($id > 0) {
            $msg = "Edit answer";
        } else {
            $msg = "Add new answer";
        }
        $question = $model->getAnswerById($id);

        include_once ($pathview . 'edit_testscore_answers_default.php');
        break;
    case 'addpq':
        $id_q = $request->getVar('id', 0);
        $msg = "Add new answer";

        include_once ($pathview . 'add_testscore_answers_default.php');
        break;
    case 'viewanswers':
        $msg = $request->getVar('msg', '');
        $title_manager = "List Answers Manager";
        $id = isset($_REQUEST['id']) ? $_REQUEST['id'] : '0';
        $layout = 'lists_testscore_answers_default.php';
        $rows_pq = $model->getListAnswers($id);
        include_once ($pathview . $layout);
        break;
    case 'ansers':

        $id_q = $request->getVar('id', 0);
        $msg = "Add new answer";

        include_once ($pathview . 'add_testscore_answers_default.php');
        break;
    case 'reports':
        $msg = $request->getVar('msg', '');
        $title_manager = "List Reports";
        $page = $request->getVar('page', 0);
        
        $item_page = 50;
        if ($page == 0 || $page == 1) {
            $offset = 0;
            $limit = $item_page;
        } else {
            $offset = ($page - 1) * $item_page;
            $limit = $item_page;
        }
        
        $rows = $model->getAllReport($offset, $limit);
        
        $totalRecord = $model->countAllReport();
         include_once ($pathview . 'lists_report_default.php');
        break;
    case 'configtime':
        include_once (PATH_APPS . 'models' . DS . 'config.model.php');
        $modelConfig = new configModel();

        $time_round = $modelConfig->getvalueConfig('time_round_testscore')->value;
        if (!$time_round) {
            $time_round = 0;
        }
        //change by Hoang Bien 05Nov2013
        $record_testscore_question = $modelConfig->getvalueConfig('number_question_testscore')->value;
        //end change
        include_once ($pathview . 'config_time.php');
        break;
    default :

        $rows = $model->getAllQuestions();

        $msg = $request->getVar('msg', '');
        $title_manager = "List Questions Manager";
        include_once ($pathview . 'lists_testscore_question_default.php');
        break;
}
?>
