<script type="text/javascript" src="<?php echo URL_TEMPLATE ?>js/fgc.js.checkbox.js"></script>
<script type="text/javascript" src="<?php echo URL_TEMPLATE ?>js/submit.js"></script>
<script type="text/javascript" src="<?php echo URL_TEMPLATE ?>js/js.publish.js"></script>
<div id="wrapper">
    <div id="top">
        <div class="top_left incom-48-user">
            <span class="title_manager"><?php echo $title_manager; ?></span>
            <span class="message_manager"><?php echo $msg; ?></span>
        </div>
        <div class="top_right">
            <table class="toolbar">
                <tr>
                    <td id="toolbar-new" class="button">
                        <a class="toolbar" onclick="return checked('addpq')">
                            <span title="New" class="icon-32-new">
                            </span>
                            New
                        </a>
                    </td>
                    <td id="toolbar-edit" class="button">
                        <a class="toolbar" onclick="return checked('editpq')">
                            <span title="Edit" class="icon-32-edit">
                            </span>
                            Edit
                        </a>
                    </td>
                    <td id="toolbar-delete" class="button">
                        <a class="toolbar" onclick="return checked('deleteaq')" href="#">
                            <span title="Delete" class="icon-32-delete">
                            </span>
                            Delete
                        </a>
                    </td>
                    <td id="toolbar-cancel" class="button">
                        <a class="toolbar" href="index.php?apps=testscore">
                            <span title="Cancel" class="icon-32-cancel">
                            </span>
                            Cancel
                        </a>
                    </td>
            </table>
        </div>
        <!--            <a href="index.php?apps=interview&task=delete">Delete</a><a href="index.php?apps=interview&task=edit">Edit</a><a href="index.php?apps=interview&task=new">New</a></div>-->
    </div>
    <div id="fgc_content">
        <form name="adminForm" method="post" action="index.php?apps=testscore&id=<?php  if(isset($rows_pq[0]->question_id)) echo $rows_pq[0]->question_id; else echo @$_REQUEST['id']; ?>">
            <table class="list_table">
                <tr class="list_titles">
                    <td height="10" scope="col" style="width: 4%">#</td>
                    <td height="20" scope="col" style="width: 3%"><input type="checkbox" name="checkAll" value="1"/></td>
                    <td height="20" scope="col" style="width: 60%">Name Position</td>
                    <td height="15" scope="col" style="width: 16%">Positions</td>
                    <td height="15" scope="col" style="width: 16%">Right Position</td>
                    <td height="20" scope="col" style="width: 4%">ID</td>
                </tr>
                <?php
                for ($i = 0; $i < count($rows_pq); $i++):
                    $row = $rows_pq[$i];
                    ?>
                    <tr class="list_details">
                        <td><?php echo $i + 1; ?></td>
                        <td><input type="checkbox" size="2" name="cid[]" value="<?php echo $row->id ?>"/></td>
                        <td style="text-align:left; padding-left: 10px;">
                            <a href="index.php?apps=testscore&task=editpq&cid[]=<?php echo $row->id ?>&id=<?php echo $row->question_id;?>">
                                <?php echo $row->namepostion; ?>
                            </a>
                        </td>
                        <td style="text-align:left; padding-left: 10px;">

                            <?php $helper = new helper();
                             $helper->renderPostionAnswer($row->postions);
                            ?>
                        </td>
                        <td><?php echo $row->right_postion; ?>
                        </td>
                        <td><?php echo $row->id; ?></td>
                    </tr>
                <?php endfor; ?>
            </table>
            <input type="hidden" name="apps" value="testscore"/>
            <input type="hidden" name="task" value="" id="name_task_submit"/>
        </form>

    </div>
</div>


