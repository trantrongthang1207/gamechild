<script type="text/javascript" src="<?php echo URL_TEMPLATE ?>js/fgc.js.checkbox.js"></script>
<script type="text/javascript" src="<?php echo URL_TEMPLATE ?>js/submit.js"></script>
<script type="text/javascript" src="<?php echo URL_TEMPLATE ?>js/js.publish.js"></script>
<div id="wrapper">
    <div id="top">
        <div class="top_left incom-48-user">
            <span class="title_manager"><?php echo $title_manager; ?></span>
            <span class="message_manager"><?php echo $msg; ?></span>
        </div>
        <div class="top_right">
            <table class="toolbar">
                <tr>
                    <td id="toolbar-new" class="button">
                        <a class="toolbar" onclick="return checked('add')">
                            <span title="New" class="icon-32-new">
                            </span>
                            New
                        </a>
                    </td>
                    <td id="toolbar-edit" class="button">
                        <a class="toolbar" onclick="return checked('edit')">
                            <span title="Edit" class="icon-32-edit">
                            </span>
                            Edit
                        </a>
                    </td>
                    <td id="toolbar-delete" class="button">
                        <a class="toolbar" onclick="return checked('delete')" href="#">
                            <span title="Delete" class="icon-32-delete">
                            </span>
                            Delete
                        </a>
                    </td>
            </table>
        </div>
    </div>
    <div id="fgc_content">
        <form name="adminForm" method="post" action="index.php?apps=testscore">
            <table class="list_table">
                <tr class="list_titles">
                    <td height="20" scope="col" style="width: 4%">#</td>
                    <td height="20" scope="col" style="width: 3%"><input type="checkbox" name="checkAll" value="1"/></td>
                    <td height="20" scope="col" style="width: 15%">Image 1</td>
                    <td height="20" scope="col" style="width: 40%">Image 2</td>
                    <td height="20" scope="col" style="width: 15%">Show answer</td>
                    <td height="20" scope="col" style="width: 10%">Published</td>
                    <td height="20" scope="col" style="width: 4%">ID</td>
                </tr>
                <?php
                for ($i = 0; $i < count($rows); $i++):
                    $row = $rows[$i];
                    ?>
                    <tr class="list_details">
                        <td><?php echo $i + 1; ?></td>
                        <td><input type="checkbox" size="2" name="cid[]" value="<?php echo $row->id ?>"/></td>
                        <td style="text-align:center; padding-left: 10px;">
                            <a href="index.php?apps=testscore&task=edit&cid[]=<?php echo $row->id ?>">
                                <img style="width: 200px; max-height: 100px" src="<?php echo URL_STIE . '../testScore/uploadfiles/' . $row->image1; ?>"/>
                            </a>
                        </td>
                        <td style="text-align:left; padding-left: 10px;">
                            <img style="width: 200px; max-height: 100px" src="<?php echo URL_STIE . '../testScore/uploadfiles/' . $row->image2; ?>"/>
                        </td>
                        <td style="text-align:center; padding-left: 10px;">
                            <?php echo $model->showLinkAnsers($row->id); ?>
                        </td>
                        <td><?php if ($row->published == 1): ?>
                                <a id="fgc_block" class="fgc_block_<?php echo $row->id ?>" action="<?php echo $row->published ?>" onclick="publishedManagement('<?php echo $row->id ?>','<?php echo URL_STIE ?>','<?php echo URL_TEMPLATE ?>','testscore')">
                                    <img class="image_block_<?php echo $row->id ?>" src="<?php echo URL_TEMPLATE ?>images/tick.png"/></a>
                            <?php else: ?>
                                <a id="fgc_block" class="fgc_block_<?php echo $row->id ?>" action="<?php echo $row->published; ?>" onclick="publishedManagement('<?php echo $row->id ?>','<?php echo URL_STIE ?>','<?php echo URL_TEMPLATE ?>','testscore')">
                                    <img class="image_block_<?php echo $row->id ?>" src="<?php echo URL_TEMPLATE ?>images/publish_x.png"/>
                                </a>
                            <?php endif; ?>
                        </td>
                        <td><?php echo $row->id; ?></td>
                    </tr>
                <?php endfor; ?>
            </table>
            <input type="hidden" name="apps" value="testscore"/>
            <input type="hidden" name="task" value="" id="name_task_submit"/>
        </form>
    </div>
</div>


