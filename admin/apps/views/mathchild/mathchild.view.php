<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
$app = App::getInstance();
$request = $app->getRequest();
$task = $request->getVar('task', '');
$pathview = PATH_APPS . 'views' . DS . 'mathchild' . DS;
include_once (PATH_APPS . 'helpers' . DS . 'helper.php');

$model = new mathchildModel();
$strabc = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
switch ($task) {
    case 'edit':
	$id = isset($_REQUEST['cid']) ? $_REQUEST['cid'] : '0';
	if ($id > 0) {
	    $msg = "Edit mathchild";
	} else {
	    $msg = "Add new mathchild";
	}
	$question = $model->getQuestion();

	// list choice free or paid
	$object1 = new stdClass();
	$object1->value = '0';
	$object1->name = 'No';
	$object2 = new stdClass();
	$object2->value = '1';
	$object2->name = 'Yes';
	$array[] = $object1;
	$array[] = $object2;
	if ($id > 0) {
	    $default = $question->published;
	} else {
	    $default = 1;
	}
	$lists['published'] = helper::showListRadio($array, 'published', 'value', 'name', $default, 'radio_question_game');

	$editor = helper::displayeditor('content_question', 20, 20, stripslashes($question->content_question), URL_STIE);
	
	include_once ($pathview . 'edit_mathchild_default.php');
	break;
    case 'view':
	$viewlayout = $request->getVar('layout');
	if ($viewlayout == 'configtime') {
	    $title_manager = "Set time round";
	    $msg = $request->getVar('msg');
	    include_once (PATH_APPS . 'models' . DS . 'config.model.php');
	    $modelConfig = new configModel();
	    $time_round = $modelConfig->getvalueConfig('time_round_mathchild')->value;
	    if (!$time_round) {
		$time_round = 0;
	    }
	    $number_random_question_mathchild = $modelConfig->getvalueConfig('number_random_question_mathchild')->value;
	    if (!$number_random_question_mathchild) {
		$number_random_question_mathchild = 15;
	    }
	    $layout = 'config_time.php';
	} else {
	    $layout = 'lists_mathchild_default.php';
	    $rows = $model->getAllQuestions();
	}
	include_once ($pathview . $layout);
	break;
    case 'reports':
	$page = $request->getVar('page', 0);
	$item_page = 50;
	if ($page == 0 || $page == 1) {
	    $offset = 0;
	    $limit = $item_page;
	} else {
	    $offset = ($page - 1) * $item_page;
	    $limit = $item_page;
	}

	$totalRecord = $model->countAllReport();
	$rows = $model->getAllReport($offset, $limit);
	$msg = $request->getVar('msg', '');
	$title_manager = "Math Report";
	include_once ($pathview . 'lists_report_default.php');
	break;
    case 'configtime':
	include_once (PATH_APPS . 'models' . DS . 'config.model.php');
	$modelConfig = new configModel();

	$time_round = $modelConfig->getvalueConfig('time_round_mathchild')->value;
	if (!$time_round) {
	    $time_round = 0;
	}
	$number_random_question_mathchild = $modelConfig->getvalueConfig('number_random_question_mathchild')->value;
	if (!$number_random_question_mathchild) {
	    $number_random_question_mathchild = 15;
	}
	include_once ($pathview . 'config_time.php');
	break;
    default :

	$rows = $model->getAllQuestions();
	$msg = $request->getVar('msg', '');
	$title_manager = "Math Manager";
	include_once ($pathview . 'lists_mathchild_default.php');
	break;
}
?>
