<script type="text/javascript" src="<?php echo URL_TEMPLATE ?>js/fgc.js.checkbox.js"></script>
<script type="text/javascript" src="<?php echo URL_TEMPLATE ?>js/submit.js"></script>
<script type="text/javascript" src="<?php echo URL_TEMPLATE ?>js/jquery.mask.js"></script>
<script type="text/javascript" src="<?php echo URL_TEMPLATE ?>js/mathchildscript.js"></script>
<link href="<?php echo URL_TEMPLATE ?>css/mathstyle.css" type="text/css" rel="stylesheet"/>  
<div id="top">
    <div class="top_left incom-48-user"><?php echo $msg; ?></div>
    <div class="top_right">
        <table class="toolbar">
            <tr>
                <td id="toolbar-save" class="button">
                    <a class="toolbar" href="#" id="submitformmathchild">
                        <span title="Save" class="icon-32-save">
                        </span>
                        Save
                    </a>
                </td>

                <td id="toolbar-cancel" class="button">
                    <a class="toolbar" href="index.php?apps=mathchild">
                        <span title="Cancel" class="icon-32-cancel">
                        </span>
                        Cancel
                    </a>
                </td>
        </table>
    </div>
</div>
<div id="fgc_content" class="practice_section">
    <form id="frm_user_edit" name="adminForm" action="index.php?apps=mathchild&task=save" method="post">
        <div>
            <label class="lab_user">Content question:</label>
        </div>
	<div class="clearfix"></div>
	<div id="dragnumber">
	    <div class="operationlists">
		<?php
		$operations = explode('$', $question->question_content);
		$resul_mathchild = $operations[count($operations) - 1];
		$mathchildModel = new mathchildModel();
		$arr_operation = array('?', '+', '-', 'x', '/', '(', ')');
		unset($operations[count($operations) - 1]);
		foreach ($operations as $value) {
		    if (in_array($value, $arr_operation) || is_numeric($value) === true)
			echo $mathchildModel->renderSelectForQuestion($value);
		}
		?>
	    </div>
	    <div class="operationresult">
		<div class="no-ui-droppable">=<input type="hidden" value="=" name="question_content[]" class="tvnums"/></div>
		<div class="no-ui-droppable"><input type="text" value="<?php echo $resul_mathchild; ?>" name="question_content[]" class="tvnums mathchild_answers"/></div>
	    </div>
	</div>
	<div class="clearfix"></div>
	<div>
	    <select name="" size="1" id="operations">
		<option value="?">&nbsp;?&nbsp;</option>
		<option value="+">&nbsp;+&nbsp;</option>
		<option value="-">&nbsp;-&nbsp;</option>
		<option value="x">&nbsp;x&nbsp;</option>
		<option value="/">&nbsp;/&nbsp;</option>
		<option value="(">&nbsp;(&nbsp;</option>
		<option value=")">&nbsp;)&nbsp;</option>
		<option value="N">&nbsp;0&nbsp;</option>
	    </select>
	    <a href="#" id="addoperation">Add operation</a>
	</div>
	<div class="clearfix"></div>
	<div>Numbers:</div>
	<div>
	    <input type="text" value="<?php echo $question->suggest_answer; ?>" name="suggest_answer" class="suggest_answer"/>(Separate by semicolon)
	</div>
        <div>
            <label class="lab_user">Published:</label>
	    <?php echo $lists['published']; ?>
        </div>
        <input type="hidden" name="type" value="mathchild">
        <input type="hidden" name="id" value="<?php echo $question->id ?>">
    </form>
</div>