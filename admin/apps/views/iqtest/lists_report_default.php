<script type="text/javascript" src="<?php echo URL_TEMPLATE ?>js/fgc.js.checkbox.js"></script>
<script type="text/javascript" src="<?php echo URL_TEMPLATE ?>js/submit.js"></script>
<script type="text/javascript" src="<?php echo URL_TEMPLATE ?>js/js.publish.js"></script>
<div id="wrapper">
    <div id="top">
        <div class="top_left incom-48-user">
            <span class="title_manager"><?php echo $title_manager; ?></span>
            <span class="message_manager"><?php echo $msg; ?></span>
        </div>
        <div class="top_right">
            <table class="toolbar">
                <tr>
                    <td id="toolbar-delete" class="button">
                        <a class="toolbar" onclick="return checked('deletereport')" href="#">
                            <span title="Delete" class="icon-32-delete">
                            </span>
                            Delete
                        </a>
                    </td>
            </table>
        </div>
        <!--            <a href="index.php?apps=interview&task=delete">Delete</a><a href="index.php?apps=interview&task=edit">Edit</a><a href="index.php?apps=interview&task=new">New</a></div>-->
    </div>
    <div id="fgc_content">
        <form name="adminForm" method="post" action="index.php?apps=iqtest">
            <table class="list_table">
                <tr class="list_titles">
                    <td height="20" scope="col" style="width: 4%">#</td>
                    <td height="20" scope="col" style="width: 3%"><input type="checkbox" name="checkAll" value="1"/></td>
                    <td height="20" scope="col" style="width: 45%">Correct percent</td>
                    <td height="20" scope="col" style="width: 4%">ID</td>
                </tr>
                <?php
                for ($i = 0; $i < count($rows); $i++):
                    $row = $rows[$i];
                    ?>
                    <tr class="list_details">
                        <td><?php echo $i + 1; ?></td>
                        <td><input type="checkbox" size="2" name="cid[]" value="<?php echo $row->id ?>"/></td>
                        <td><?php echo $row->correct_percent; ?></td>
                        <td><?php echo $row->id; ?></td>
                    </tr>
                <?php endfor; ?>
            </table>
            <input type="hidden" name="apps" value="iqtest"/>
            <input type="hidden" name="task" value="" id="name_task_submit"/>
        </form>
         <div><?php
            $helper = new helper();
            echo $helper->Paging($totalRecord, $item_page, 'iqtest', 'task=report');
            ?></div>
    </div>
</div>


