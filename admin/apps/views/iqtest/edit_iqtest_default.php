<script type="text/javascript" src="<?php echo URL_TEMPLATE ?>js/fgc.js.checkbox.js"></script>
<script type="text/javascript" src="<?php echo URL_TEMPLATE ?>js/submit.js"></script>
<div id="top">
    <div class="top_left incom-48-user"><?php echo $msg; ?></div>
    <div class="top_right">
        <table class="toolbar">
            <tr>
                <td id="toolbar-save" class="button">
                    <a class="toolbar" href="javascript: submitform()">
                        <span title="Save" class="icon-32-save">
                        </span>
                        Save
                    </a>
                </td>

                <td id="toolbar-cancel" class="button">
                    <a class="toolbar" href="index.php?apps=iqtest">
                        <span title="Cancel" class="icon-32-cancel">
                        </span>
                        Cancel
                    </a>
                </td>
        </table>
    </div>
</div>
<div id="fgc_content" class="practice_section">
    <form id="frm_user_edit" name="adminForm" action="index.php?apps=iqtest&task=save" method="post" enctype="multipart/form-data">
        <div>
            <label class="lab_user">Image question:</label>
            <div>
                <br/>
                <input type="file" name="images"/>
                <br/>
                <br/>
                <?php if ($question->images != '') { ?>
                    <img style="width: 600px; max-height: 200px" src="<?php echo '../../../IQtest/uploadfiles/' . $question->images ?>" alt="<?php echo $question->images ?>"/>
                <?php } ?>
            </div>
        </div>
        <div>Answers (choose a right answer):</div>
        <div>
            <ul class="listanswers">
                <?php
                for ($i = 0; $i < 8; $i++) {
                    ?>
                    <li>
                        <span>
                            <?php echo $strabc[$i]; ?>
                        </span>
                        <input type="radio" name="right_answer" <?php
                        echo $right_answer == $i
                                ? 'checked="true"'
                                : ''
                        ?> value="<?php echo $i ?>"/>
                    </li>
                    <?php
                }
                ?>
            </ul>
        </div>
        <div>
            <label class="lab_user">Published:</label>
            <?php echo $lists['published']; ?>
        </div>
        <input type="hidden" name="type" value="iqtest">
        <input type="hidden" name="id" value="<? echo $question->id ?>">
    </form>
</div>