<script type="text/javascript" src="<?php echo URL_TEMPLATE ?>js/fgc.js.checkbox.js"></script>
<script type="text/javascript" src="<?php echo URL_TEMPLATE ?>js/submit.js"></script>
<script type="text/javascript" src="<?php echo URL_TEMPLATE ?>js/jquery.mask.js"></script>
<script type="text/javascript" src="<?php echo URL_TEMPLATE ?>js/mathscript.js"></script>
<link href="<?php echo URL_TEMPLATE ?>css/mathstyle.css" type="text/css" rel="stylesheet"/>  
<div id="top">
    <div class="top_left incom-48-user"><?php echo $msg; ?></div>
    <div class="top_right">
        <table class="toolbar">
            <tr>
                <td id="toolbar-save" class="button">
                    <a class="toolbar" href="#" id="submitformmath">
                        <span title="Save" class="icon-32-save">
                        </span>
                        Save
                    </a>
                </td>

                <td id="toolbar-cancel" class="button">
                    <a class="toolbar" href="index.php?apps=math">
                        <span title="Cancel" class="icon-32-cancel">
                        </span>
                        Cancel
                    </a>
                </td>
        </table>
    </div>
</div>
<div id="fgc_content" class="practice_section">
    <form id="frm_user_edit" name="adminForm" action="index.php?apps=math&task=save" method="post">
        <div>
            <label class="lab_user">Content question:</label>
        </div>
	<div class="clearfix"></div>
	<div id="dragnumber">
	    <div class="operationlists">
		<?php
		$operations = explode('$', $question->question_content);
		$resul_math = $operations[count($operations) - 1];
		$mathModel = new mathModel();
		$arr_operation = array('?', '+', '-', 'x', '/', '(', ')');
		unset($operations[count($operations) - 1]);
		foreach ($operations as $value) {
		    if (in_array($value, $arr_operation) || is_numeric($value) === true)
			echo $mathModel->renderSelectForQuestion($value);
		}
		?>
	    </div>
	    <div class="operationresult">
		<div class="no-ui-droppable">=<input type="hidden" value="=" name="question_content[]" class="tvnums"/></div>
		<div class="no-ui-droppable"><input type="text" value="<?php echo $resul_math; ?>" name="question_content[]" class="tvnums math_answers"/></div>
	    </div>
	</div>
	<div class="clearfix"></div>
	<div>
	    <select name="" size="1" id="operations">
		<option value="?">&nbsp;?&nbsp;</option>
		<option value="+">&nbsp;+&nbsp;</option>
		<option value="-">&nbsp;-&nbsp;</option>
		<option value="x">&nbsp;x&nbsp;</option>
		<option value="/">&nbsp;/&nbsp;</option>
		<option value="(">&nbsp;(&nbsp;</option>
		<option value=")">&nbsp;)&nbsp;</option>
		<option value="N">&nbsp;0&nbsp;</option>
	    </select>
	    <a href="#" id="addoperation">Add operation</a>
	</div>
	<div class="clearfix"></div>
        <div>Answers (choose a right answer):</div>
        <div>
            <ul class="listanswers">
		<?php
		$total_answer = count($answers);
		if (count($answers) > 0) {
		    for ($i = 0; $i < $total_answer; $i++) {
			?>
			<li>
			    <span>
				<?php echo $strabc[$i]; ?>
			    </span>
			    <input type="checkbox" name="right_answer[]" <?php
			    if (@in_array($i, $right_answer))
				echo 'checked="true"';
			    ?> value="<?php echo $i ?>" class="right_answer"/>
			    <input class="math_answers" type="text" name="answers[]" value="<?php echo $answers[$i] ?>"/><span class="tvdelete"></span>
			</li>
			<?php
		    }
		} else {
		    for ($i = 0; $i < $total_answer; $i++) {
			?>
			<li>
			    <span>
				<?php echo $strabc[$i]; ?>
			    </span>
			    <input type="checkbox" name="right_answer[]" value="<?php echo $i ?>" class="right_answer"/>
			    <input type="text" name="answers[]" class="math_answers"/><span class="tvdelete"></span>
			</li>
			<?php
		    }
		}
		?>
            </ul>
	    <a href="#" id="addanswer">Add answers</a>
        </div>
	<div>Numbers:</div>
	<div>
	    <input type="text" value="<?php echo $question->suggest_answer; ?>" name="suggest_answer" class="suggest_answer"/>(Separate by semicolon)
	</div>
        <div>
            <label class="lab_user">Published:</label>
	    <?php echo $lists['published']; ?>
        </div>
        <input type="hidden" name="type" value="math">
        <input type="hidden" name="id" value="<? echo $question->id ?>">
    </form>
</div>