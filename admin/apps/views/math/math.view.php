<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
$app = App::getInstance();
$request = $app->getRequest();
$task = $request->getVar('task', '');
$pathview = PATH_APPS . 'views' . DS . 'math' . DS;
include_once (PATH_APPS . 'helpers' . DS . 'helper.php');

$model = new mathModel();
$strabc = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
switch ($task) {
    case 'edit':
	$id = isset($_REQUEST['cid']) ? $_REQUEST['cid'] : '0';
	if ($id > 0) {
	    $msg = "Edit math";
	} else {
	    $msg = "Add new math";
	}
	$question = $model->getQuestion();

	// list choice free or paid
	$object1 = new stdClass();
	$object1->value = '0';
	$object1->name = 'No';
	$object2 = new stdClass();
	$object2->value = '1';
	$object2->name = 'Yes';
	$array[] = $object1;
	$array[] = $object2;
	if ($id > 0) {
	    $default = $question->published;
	} else {
	    $default = 1;
	}
	$lists['published'] = helper::showListRadio($array, 'published', 'value', 'name', $default, 'radio_question_game');

	$editor = helper::displayeditor('content_question', 20, 20, stripslashes($question->content_question), URL_STIE);

	if ($question->answers != '') {
	    $answers = json_decode($question->answers);
	} else {
	    $answers = array();
	}
	if ($question->right_answer != '') {
	    $right_answer = json_decode($question->right_answer);
	} else {
	    $right_answer = array();
	}
	include_once ($pathview . 'edit_math_default.php');
	break;
    case 'view':
	$viewlayout = $request->getVar('layout');
	if ($viewlayout == 'configtime') {
	    $title_manager = "Set time round";
	    $msg = $request->getVar('msg');
	    include_once (PATH_APPS . 'models' . DS . 'config.model.php');
	    $modelConfig = new configModel();
	    $time_round = $modelConfig->getvalueConfig('time_round_math')->value;
	    if (!$time_round) {
		$time_round = 0;
	    }
	    $number_random_question_math = $modelConfig->getvalueConfig('number_random_question_math')->value;
	    if (!$number_random_question_math) {
		$number_random_question_math = 15;
	    }
	    $layout = 'config_time.php';
	} else {
	    $layout = 'lists_math_default.php';
	    $rows = $model->getAllQuestions();
	}
	include_once ($pathview . $layout);
	break;
    case 'reports':
	$page = $request->getVar('page', 0);
	$item_page = 50;
	if ($page == 0 || $page == 1) {
	    $offset = 0;
	    $limit = $item_page;
	} else {
	    $offset = ($page - 1) * $item_page;
	    $limit = $item_page;
	}

	$totalRecord = $model->countAllReport();
	$rows = $model->getAllReport($offset, $limit);
	$msg = $request->getVar('msg', '');
	$title_manager = "Math Report";
	include_once ($pathview . 'lists_report_default.php');
	break;
    case 'configtime':
	include_once (PATH_APPS . 'models' . DS . 'config.model.php');
	$modelConfig = new configModel();

	$time_round = $modelConfig->getvalueConfig('time_round_math')->value;
	if (!$time_round) {
	    $time_round = 0;
	}
	$number_random_question_math = $modelConfig->getvalueConfig('number_random_question_math')->value;
	if (!$number_random_question_math) {
	    $number_random_question_math = 15;
	}
	include_once ($pathview . 'config_time.php');
	break;
    default :

	$rows = $model->getAllQuestions();
	$msg = $request->getVar('msg', '');
	$title_manager = "Math Manager";
	include_once ($pathview . 'lists_math_default.php');
	break;
}
?>
