<script type="text/javascript" src="<?php echo URL_TEMPLATE ?>js/fgc.js.checkbox.js"></script>
<script type="text/javascript" src="<?php echo URL_TEMPLATE ?>js/submit.js"></script>
<div id="top">
      <div class="top_left incom-48-user">
            <span class="title_manager"><?php echo $title_manager;?></span>
             <span class="message_manager"><?php echo $msg;?></span>
      </div>
    <div class="top_right">
        <table class="toolbar">
            <tr>
                <td id="toolbar-save" class="button">
                    <a class="toolbar" href="javascript: submitform()">
                        <span title="Save" class="icon-32-save">
                        </span>
                        Save
                    </a>
                </td>

                <td id="toolbar-cancel" class="button">
                    <a class="toolbar" href="index.php?apps=testsection">
                        <span title="Cancel" class="icon-32-cancel">
                        </span>
                        Cancel
                    </a>
                </td>
        </table>
    </div>
</div>
<div id="fgc_content">
    <form id="frm_user_edit" class="form_config_time" name="adminForm" action="index.php?apps=testsection&task=save_config" method="post">
        <div class="set_time_view"><label>Time for each round:</label><input type="text" name="time_round_testsection" value="<?php echo $time_round?>">(seconds)</div>
        <div class="set_time_view"><label>Number questions each time for match test game:</label><input type="text" name="number_random_question_testsection" value="<?php echo $number_question_match;?>">(Questions)</div>
    </form>
</div>
