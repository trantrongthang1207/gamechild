<script type="text/javascript" src="<?php echo URL_TEMPLATE ?>js/fgc.js.checkbox.js"></script>
<script type="text/javascript" src="<?php echo URL_TEMPLATE ?>js/submit.js"></script>
<div id="top">
    <div class="top_left incom-48-user"><?php echo $msg; ?></div>
    <div class="top_right">
        <table class="toolbar">
            <tr>
                <td id="toolbar-save" class="button">
                    <a class="toolbar" href="javascript: submitform()">
                        <span title="Save" class="icon-32-save">
                        </span>
                        Save
                    </a>
                </td>

                <td id="toolbar-cancel" class="button">
                    <a class="toolbar" href="index.php?apps=testsection">
                        <span title="Cancel" class="icon-32-cancel">
                        </span>
                        Cancel
                    </a>
                </td>
        </table>
    </div>
</div>
<div id="fgc_content" class="practice_section">
    <form id="frm_user_edit" name="adminForm" action="index.php?apps=testsection&task=save" method="post">
        <div>
            <label class="lab_user">Content question:</label>
            <div class="editor">
                <?php echo $editor; ?>
            </div>
        </div>
        <div>Answers (choose a right answer):</div>
        <div>
            <ul class="listanswers">
                <?php
                if (count($answers) > 0) {
                    for ($i = 0; $i < 4; $i++) {
                        ?>
                        <li>
                            <span>
                                <?php echo $strabc[$i]; ?>
                            </span>
                            <input type="radio" name="right_answer" <?php
                            echo $right_answer == $i
                                    ? 'checked="true"'
                                    : ''
                            ?> value="<?php echo $i ?>"/>
                            <input type="text" name="answers[]" value="<?php echo $answers[$i] ?>"/>
                        </li>
                        <?php
                    }
                } else {
                    for ($i = 0; $i < 4; $i++) {
                        ?>
                        <li>
                            <span>
                                <?php echo $strabc[$i]; ?>
                            </span>
                            <input type="radio" name="right_answer" value="<?php echo $i ?>"/>
                            <input type="text" name="answers[]"/>
                        </li>
                        <?php
                    }
                }
                ?>
            </ul>
        </div>
        <div>
            <label class="lab_user">Published:</label>
            <?php echo $lists['published']; ?>
        </div>
        <input type="hidden" name="type" value="testsection">
        <input type="hidden" name="id" value="<? echo $question->id ?>">
    </form>
</div>