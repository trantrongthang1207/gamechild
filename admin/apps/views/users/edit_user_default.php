<script type="text/javascript" src="<?php echo URL_TEMPLATE ?>js/fgc.js.checkbox.js"></script>
<script type="text/javascript" src="<?php echo URL_TEMPLATE ?>js/submit.js"></script>
<!--<script type="text/javascript" src="<?php echo URL_TEMPLATE ?>js/fgc.validate.js"></script>-->
<div id="top">
    <div class="top_left incom-48-user">
        <span class="title_manager"><?php echo $title_manager;?></span>
    </div>
    <div class="top_right">
        <table class="toolbar">
            <tr>
                <td id="toolbar-save" class="button">
                    <a class="toolbar" href="javascript: submitform()">
                        <span title="Save" class="icon-32-save">
                        </span>
                        Save
                    </a>
                </td>

                <td id="toolbar-cancel" class="button">
                    <a class="toolbar" href="index.php?apps=user">
                        <span title="Cancel" class="icon-32-cancel">
                        </span>
                        Cancel
                    </a>
                </td>
        </table>
<!--        <a href="index.php?apps=user&task=save">Save</a><a href="index.php?apps=user">Cancel</a>-->
    </div>
</div>
<div id="fgc_content">
    <form id="frm_user_edit" name="adminForm" action="index.php?apps=user&task=save" method="post">

        <div><label class="lab_user">Name:</label>
            <input  id="fgc_airline" type="text" name="name" value="<?php echo stripcslashes($user->name); ?>" class="required">
        </div>
        <div><label class="lab_user">Username:</label>
            <input  id="fgc_airline" type="text" name="username" value="<?php echo stripcslashes($user->username); ?>" class="required">
        </div>
        <div><label class="lab_user">Email:</label>
            <input  id="fgc_airline" type="text" name="email" value="<?php echo stripcslashes($user->email); ?>" class="required email">
        </div>
        <?php if($usernamesession==$user->username || $nametask!='edit'):?>
        <div><label class="lab_user">New Password:</label>
            <input  id="fgc_airline" type="password" name="password" value="" <?php if ($nametask!='edit'):?>class="required"<?php endif;?>>
        </div>
        <div><label class="lab_user">Verify Password:</label>
            <input  id="fgc_airline" type="password" name="verify_password" <?php if ($nametask!='edit'):?>class="required pass"<?php endif;?>>
        </div>
        <?php endif;?>
        <div><label class="lab_user">Group:</label>
            <?php echo $lists['user_type'];?>
        </div>
        <div>
            <label class="block_user">Block</label>
            <?php echo $lists['radioblock'];?>
        </div>
        <input type="hidden" name="id" value="<? echo $user->id ?>">
    </form>
</div>