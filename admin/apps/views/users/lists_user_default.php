<script type="text/javascript" src="<?php echo URL_TEMPLATE ?>js/fgc.js.checkbox.js"></script>
<script type="text/javascript" src="<?php echo URL_TEMPLATE ?>js/submit.js"></script>
<script type="text/javascript" src="<?php echo URL_TEMPLATE ?>js/fgc.block.user.js"></script>
<div id="wrapper">
    <div id="top">
        <div class="top_left incom-48-user">
            <span class="title_manager"><?php echo $title_manager; ?></span>
            <span class="message_manager"><?php echo $msg;?></span>
        </div>
        <div class="top_right">
            <table class="toolbar">
                <tr>
                    <td id="toolbar-new" class="button">
                        <a class="toolbar" onclick="return checked('add')">
                            <span title="New" class="icon-32-new">
                            </span>
                            New
                        </a>
                    </td>

                    <td id="toolbar-edit" class="button">
                        <a class="toolbar" onclick="return checked('edit')">
                            <span title="Edit" class="icon-32-edit">
                            </span>
                            Edit
                        </a>
                    </td>
                    <td id="toolbar-delete" class="button">
                        <a class="toolbar" onclick="return checked('delete')" href="#">
                            <span title="Delete" class="icon-32-delete">
                            </span>
                            Delete
                        </a>
                    </td>
            </table>
            <!--            <a href="index.php?apps=user&task=delete">Delete</a><a href="index.php?apps=user&task=edit&cid[]=1">Edit</a><a href="index.php?apps=user&task=edit">New</a>-->
        </div>
    </div>
    <div id="fgc_content">
        <form name="adminForm" method="post" action="index.php?apps=user" id="adminform">
            <table class="list_table">
                <tr class="list_titles">
                    <td height="20" scope="col" style="width: 4%">#</td>
                    <td height="20" scope="col" style="width: 3%"><input type="checkbox" name="checkAll" value="1"/></td>
                    <td height="20" scope="col" style="width: 17% text-align:left;">Name</td>
                    <td height="20" scope="col" style="width: 20% ">Username</td>
                    <td height="20" scope="col" style="width: 8%">Enabled</td>
                    <td height="20" scope="col" style="width: 10%">Type</td>
                    <td height="20" scope="col" style="width: 33%">Email</td>
                    <td height="20" scope="col" style="width: 5%">ID</td>
                </tr>
                <?php
                for ($i = 0; $i < count($rows); $i++):
                    $row = $rows[$i];
                ?>
                    <tr class="list_details">
                        <td><?php echo $i + 1; ?></td>
                        <td><input type="checkbox" size="2" name="cid[]" value="<?php echo $row->id ?>"/></td>
                        <td style="text-align:left; padding-left: 10px;"><a href="index.php?apps=user&task=edit&cid[]=<?php echo $row->id ?>"><?php echo stripcslashes($row->name); ?></a></td>
                        <td style="text-align:left; padding-left: 10px;"><?php echo $row->username; ?></td>
                        <td><?php if($row->block==0):?>
                            <a id="fgc_block" class="fgc_block_<?php echo $row->id?>" action="<?php echo $row->block?>" onclick="blockuser('<?php echo $row->id?>','<?php echo URL_STIE?>','<?php echo URL_TEMPLATE ?>')">
                                <img class="image_block_<?php echo $row->id?>" src="<?php echo URL_TEMPLATE ?>images/tick.png"/></a>
                                <?php else:?>
                            <a id="fgc_block" class="fgc_block_<?php echo $row->id?> action="<?php echo $row->block ?>" onclick="blockuser('<?php echo $row->id?>','<?php echo URL_STIE?>','<?php echo URL_TEMPLATE ?>')">
                                <img class="image_block_<?php echo $row->id?>" src="<?php echo URL_TEMPLATE ?>images/publish_x.png"/>
                            </a>
                                <?php endif;?>
                        </td>
                        <td><?php echo $row->user_type; ?></td>
                        <td><a href="index.php?apps=user&task=edit&cid[]=<?php echo $row->id ?>"><?php echo $row->email; ?></a></td>

                        <td><?php echo $row->id; ?></td>
                    </tr>
                <?php
                    endfor;
                ?>
            </table>
            <input type="hidden" name="apps" value="user"/>
            <input type="hidden" name="task" value="" id="name_task_submit"/>
        </form>
    </div>
    <div class="paging_data"><?php echo $paging?></div>
</div>



