<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
$app = App::getInstance();
$request = $app->getRequest();
$task = $request->getVar('task', '');
$pathview = PATH_APPS . 'views' . DS . 'users' . DS;
include_once (PATH_APPS . 'helpers' . DS . 'helper.php');
include_once (PATH_APPS . 'models' . DS . 'config.model.php');
switch ($task) {
    case 'edit':
        $model = new userModel();
        $user = $model->getUser();
        $session = $app->getSession();
        $usernamesession = $session->get('username', '', 'adminlogin');
        $arrayradio = array();
        $block = new stdClass();
        $block->value = 0;
        $block->name = 'No';
        $blocked = new stdClass();
        $blocked->value = 1;
        $blocked->name = 'Yes';
        $arrayradio[] = $block;
        $arrayradio[] = $blocked;
        if ($user->block) {
            $block = $user->block;
        } else {
            $block = 0;
        }
        $lists['radioblock'] = helper::showListRadio($arrayradio, 'block', 'value', 'name', $block, 'radio_block_user');
        $id = isset($_REQUEST['cid']) ? $_REQUEST['cid'] : '0';
        if ($id > 0) {
            $title_manager = "Edit user";
            $nametask = "edit";
        } else {
            $nametask = "add";
            $title_manager = "Add new user";
        }
        $typeuser = new stdClass();
        $typeuser->name = "Admin";
        $typeuser->value = 'admin';
        $typeuser2 = new stdClass();
        $typeuser2->name = "User";
        $typeuser2->value = 'user';
        $arraytype[] = $typeuser;
        $arraytype[] = $typeuser2;
        $lists['user_type'] = helper::showSelect($arraytype, 'user_type', 'name', 'value', 'required', "select_type_user", $user->user_type, '-Select type user-');
        include_once ($pathview . 'edit_user_default.php');
        break;
    case 'view':
        $model = new configModel();
        $item_pagper = $model->getvalueConfig('item_pager')->value;
        $model = new userModel();
        $currentpage = $request->getVar('page', 0);
        if ($currentpage) {
            $currentpage = $currentpage;
        } else {
            $currentpage = 1;
        }
        if ($currentpage == 1) {
            $offset = 0;
            $limit = $item_pagper;
        } else if ($currentpage > 1) {
            $offset = ($currentpage - 1) * $item_pagper;
            $limit = $item_pagper;
        }
        $rows = $model->getListUser($limit, $offset);
        $msg = $request->getVar('msg', '');
        $title_manager = "User Manager";
        $total = $model->countUser();
        $paging = helper::Paging($total, $item_pagper, 'user');
        include_once ($pathview . 'lists_user_default.php');
        break;
    default :
        $model = new configModel();
        $item_pagper = $model->getvalueConfig('item_pager')->value;
        $currentpage = $request->getVar('page', 0);
        if ($currentpage) {
            $currentpage = $currentpage;
        } else {
            $currentpage = 1;
        }
        if ($currentpage == 1) {
            $offset = 0;
            $limit = $item_pagper;
        } else if ($currentpage > 1) {
            $offset = ($currentpage - 1) * $item_pagper;
            $limit = $item_pagper;
        }
        $model = new userModel();
        $rows = $model->getListUser($limit, $offset);
        $msg = $request->getVar('msg', '');
        $title_manager = "User Manager";
        $total = $model->countUser();
        $paging = helper::Paging($total, $item_pagper, 'user');
        include_once ($pathview . 'lists_user_default.php');
        break;
}
?>
