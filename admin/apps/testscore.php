<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
include_once (PATH_APPS . 'models' . DS . 'testscore.model.php');
include_once (PATH_APPS . 'controllers' . DS . 'testscore.controller.php');
$app = App::getInstance();
$request = $app->getRequest();
$task = $request->getVar('task', '');
switch ($task) {
    case 'save':
        $controller = new testscoreController();
        $controller->saveq();
        break;
    case 'delete':
        $controller = new testscoreController();
        $controller->delete();
        break;
    case 'save_config':
        $controller = new testscoreController();
        $controller->save_config();
        break;
    case 'published':
        $controller = new testscoreController();
        $controller->published();
        break;
    case 'deletereport':
        $controller = new testscoreController();
        $controller->deletereport();
        break;
    case 'saveanswer':
        $controller = new testscoreController();
        $controller->saveanswer();
        break;
    case 'deleteaq':
        $controller = new testscoreController();
        $controller->deleteanswer();
        break;
    default:
        include_once (PATH_APPS . 'views' . DS . 'testscore' . DS . 'testscore.view.php');
        break;
}

?>