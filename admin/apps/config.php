<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
include_once (PATH_APPS . 'models' . DS . 'config.model.php');
include_once (PATH_APPS . 'controllers' . DS . 'config.controller.php');
$app = App::getInstance();
$request = $app->getRequest();
$task = $request->getVar('task', '');
switch ($task) {
    case 'save':
        $controller = new configController();
        $controller->save();
        break;
    case 'view':
        $model = new configModel();
        //$config = $model->getvalueConfig('price_interview');
        include_once (PATH_APPS . 'views' . DS . 'config' . DS . 'config.view.php');
        break;
    case 'ajax_time':
        $unit_time = $request->getVar('unint_time','');
        $select = helper::showSelectTime($unit_time, 'end_time', 'select_end_time', 'select_option', '', '-Select Value Time-');
        echo $select;
        exit();
        break;
    default :
        $model = new configModel();
        //$config = $model->getvalueConfig('price_interview');
        include_once (PATH_APPS . 'views' . DS . 'config' . DS . 'config.view.php');
        break;
}
?>
