<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
include_once (PATH_APPS . 'models' . DS . 'iqtest.model.php');
include_once (PATH_APPS . 'controllers' . DS . 'iqtest.controller.php');
$app = App::getInstance();
$request = $app->getRequest();
$task = $request->getVar('task', '');
switch ($task) {
    case 'save':
        $controller = new iqtestController();
        $controller->save();
        break;
    case 'delete':
        $controller = new iqtestController();
        $controller->delete();
        break;
    case 'save_config':
        $controller = new iqtestController();
        $controller->save_config();
        break;
    case 'published':
        $controller = new iqtestController();
        $controller->published();
        break;
    case 'deletereport':
        $controller = new iqtestController();
        $controller->deletereport();
        break;
    default :
        include_once (PATH_APPS . 'views' . DS . 'iqtest' . DS . 'iqtest.view.php');
        break;
}
?>