<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
include_once (PATH_APPS . 'models' . DS . 'math.model.php');
include_once (PATH_APPS . 'controllers' . DS . 'math.controller.php');
$app = App::getInstance();
$request = $app->getRequest();
$task = $request->getVar('task', '');
switch ($task) {
    case 'save':
        $controller = new mathController();
        $controller->save();
        break;
    case 'delete':
        $controller = new mathController();
        $controller->delete();
        break;
    case 'save_config':
        $controller = new mathController();
        $controller->save_config();
        break;
    case 'published':
        $controller = new mathController();
        $controller->published();
        break;
    case 'deletereport':
        $controller = new mathController();
        $controller->deletereport();
        break;
    default :
        include_once (PATH_APPS . 'views' . DS . 'math' . DS . 'math.view.php');
        break;
}
?>