<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
include_once (PATH_APPS.'models'.DS.'user.model.php');
include_once (PATH_APPS.'controllers'.DS.'user.controller.php');
$app = App::getInstance();
$request = $app->getRequest();
$task = $request->getVar('task','');
switch ($task){
case 'save':
    $controller = new userController();
    $controller->save();
    break;
case 'delete':
    $controller = new userController();
    $controller->delete();
    break;
case 'block':
    $controller = new userController();
    $controller->block();
    break;
default :
    include_once (PATH_APPS.'views'.DS.'users'.DS.'user.view.php');
    break;
}

?>
