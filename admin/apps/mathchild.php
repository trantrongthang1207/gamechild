<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
include_once (PATH_APPS . 'models' . DS . 'mathchild.model.php');
include_once (PATH_APPS . 'controllers' . DS . 'mathchild.controller.php');
$app = App::getInstance();
$request = $app->getRequest();
$task = $request->getVar('task', '');
switch ($task) {
    case 'save':
        $controller = new mathchildController();
        $controller->save();
        break;
    case 'delete':
        $controller = new mathchildController();
        $controller->delete();
        break;
    case 'save_config':
        $controller = new mathchildController();
        $controller->save_config();
        break;
    case 'published':
        $controller = new mathchildController();
        $controller->published();
        break;
    case 'deletereport':
        $controller = new mathchildController();
        $controller->deletereport();
        break;
    default :
        include_once (PATH_APPS . 'views' . DS . 'mathchild' . DS . 'mathchild.view.php');
        break;
}
?>