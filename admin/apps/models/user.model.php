<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
include_once (PATH_LIBARIES_TABLE . 'users.class.php');

class userModel {

    function __construct() {
        $app = App::getInstance();
        $request = $app->getRequest();
        $cid = $request->getVar('cid', array(0));
        $this->setId($cid[0]);
    }

    function setId($id) {
        $this->id = $id;
    }

    function getUser() {
        $app = App::getInstance();
        /* @var $db Database */
        $db = $app->getDBO();
        $table = new UsersTable($db);
        $result = $table->load_object($this->id);
        if (!empty($result)) {
            return $result;
        } else {
            $result = new stdClass();
            $result->id = 0;
            $result->name = '';
            $result->username = '';
            $result->email = '';
            $result->password = '';
            $result->user_type = '';
            $result->block = '';
            $result->send_email = '';
            $result->register_date = '';
            $result->last_visit_date = '';
            $result->checked_out = '';
            $result->checked_out_time = '';
            return $result;
        }
    }

    function delete($id) {
        $app = App::getInstance();
        /* @var $db Database */
        $db = $app->getDBO();

        include_once (PATH_LIBARIES_TABLE . DS . 'subscribe.class.php');
        $table_sub = new SubscribeTable($db);
        $query_sub = "SELECT * FROM subscribe_interviews where userid = " . $db->Quote($id);
        $db->setQuery($query_sub);
        $sub = $db->loadObjectList();

        for ($i = 0; $i < count($sub); $i++) {
            $table_sub->delete($sub[$i]->id);
        }

        $table = new UsersTable($db);
        return $table->delete($id);
    }

    function insertUpdate($records) {
        $app = App::getInstance();
        /* @var $db Database */
        $db = $app->getDBO();
        $table = new UsersTable($db);
        if (!empty($records)) {
            if (!$table->save($records))
                return false;
        }
        return true;
    }

    function getListUser($limit=0,$offset=0) {
        $app = App::getInstance();
        /* @var $db Database */
        $db = $app->getDBO();
        $query = "SELECT * FROM users order by id desc";
        $db->setQuery($query, $offset, $limit);
        if ($results = $db->loadObjectList())
            return $results;
        else
            return null;
    }

    function countUser() {
        $app = App::getInstance();
        $db = $app->getDBO();
        $query = "SELECT count(id) as countuser FROM users";
        $db->setQuery($query);
        if ($result = $db->loadResult()) {
            return $result;
        }else {
            return 0;
        }
    }

    function getuserbyusername($username) {
        $app = App::getInstance();
        /* @var $db Database */
        $db = $app->getDBO();
        $query = "SELECT * FROM users WHERE username=" . $db->Quote($username) . " LIMIT 1";
        $db->setQuery($query);
        if ($results = $db->loadObject())
            return $results;
        else
            return array();
    }

    function getusernamebyemail($email) {
        $app = App::getInstance();
        /* @var $db Database */
        $db = $app->getDBO();
        $query = "SELECT * FROM users WHERE username=" . $db->Quote($email) . " LIMIT 1";
        $db->setQuery($query);
        if ($results = $db->loadObject())
            return $results;
        else
            return null;
    }

    function block($id, $bl) {
        $app = App::getInstance();
        /* @var $db Database */
        $db = $app->getDBO();
        if ($bl == 0) {
            $query = "UPDATE users SET block=1 WHERE id=" . $db->Quote($id);
        } else {
            $query = "UPDATE users SET block=0 WHERE id=" . $db->Quote($id);
        }
        $db->setQuery($query);
        if ($db->query())
            return true;
        else
            return false;
    }

}

?>
