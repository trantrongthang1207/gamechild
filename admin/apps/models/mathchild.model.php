<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of questiongame
 *
 * @author AdminFGC
 */
//include_once (PATH_LIBARIES_TABLE . 'questionsgame.class.php');
class mathchildModel {

    //put your code here
    function __construct() {
	$app = App::getInstance();
	$request = $app->getRequest();
	$cid = $request->getVar('cid', array());
	$this->setId($cid[0]);
    }

    function setId($id) {
	$this->id = $id;
    }

    /**
     * @param  typequestion, $limit, $offset
     * @return array
     */
    function getAllQuestions($offset = 0, $limit = 0) {
	$app = App::getInstance();
	$db = $app->getDBO();
	$sql = "SELECT * FROM mathchild_questions order by id desc";
	$db->setQuery($sql, $offset, $limit);
	if ($result = $db->loadObjectList()) {
	    return $result;
	} else {
	    return array();
	}
    }

    function getQuestion() {
	$app = App::getInstance();
	$db = $app->getDBO();
	$sql = "SELECT * FROM mathchild_questions WHERE id = '{$this->id}'";
	$db->setQuery($sql, 0, 1);
	if ($result = $db->loadObject()) {
	    return $result;
	} else {
	    return false;
	}
    }

    function getAllReport($offset = 0, $limit = 0) {
	$app = App::getInstance();
	$db = $app->getDBO();
	$sql = "SELECT * FROM mathchild_report";
	$db->setQuery($sql, $offset, $limit);
	if ($result = $db->loadObjectList()) {
	    return $result;
	} else {
	    return array();
	}
    }

    function countAllReport() {
	$app = App::getInstance();
	$db = $app->getDBO();
	$sql = "SELECT count(id) as countobject FROM mathchild_report";
	$db->setQuery($sql);
	if ($result = $db->loadObject()) {
	    return $result->countobject;
	} else {
	    return 0;
	}
    }

    function deletereport($id) {
	$app = App::getInstance();
	/* @var $db Database */
	$db = $app->getDBO();
	$table = new Math_reportTable($db);
	return $table->delete($id);
    }

    function delete($id) {
	$app = App::getInstance();
	/* @var $db Database */
	$db = $app->getDBO();
	$table = new Mathchild_questionsTable($db);
	return $table->delete($id);
    }

    function insertUpdate($records) {
	$app = App::getInstance();
	/* @var $db Database */
	$db = $app->getDBO();
	$table = new Mathchild_questionsTable($db);
	$records['question_content'] = implode("$", $records['question_content']);
	if (!empty($records)) {
	    if (!$table->save($records))
		return false;
	}
	return true;
    }

    function getResult($number1, $cal, $number2) {
	switch ($cal) {
	    case '+':
		$number = $number1 + $number2;
		break;
	    case '-':
		$number = $number1 - $number2;
		break;
	    case '*':
		$number = $number1 * $number2;
		break;
	    case '/':
		$number = $number1 / $number2;
		break;
	    default :
		$number = 0;
	}
	return number_format($number, 2, '.', '');
    }

    function getNamCalculation($cal) {
	//subtraction, division, multiplication, plus
	switch ($cal) {
	    case '+':
		$name = 'plus';
		break;
	    case '-':
		$name = 'Subtraction';
		break;
	    case '*':
		$name = 'Multiplaication';
		break;
	    case '/':
		$name = 'Division';
		break;
	    default :
		$name = 'no';
		break;
	}
	return $name;
    }

    function published($id, $published) {
	$app = App::getInstance();
	/* @var $db Database */
	$db = $app->getDBO();
	$query = "UPDATE mathchild_questions SET published='$published' WHERE id=" . $db->Quote($id);
	$db->setQuery($query);
	if ($db->query())
	    return true;
	else
	    return false;
    }

    /**
     * Loai bo ky tu $ trong chuoi cau hoi
     * @param type $string
     * @return string
     */
    function filterQuestionContent($string) {
	if (empty($string))
	    return '';
	return str_replace("$", "", $string);
    }

    function renderSelectForQuestion($operation) {
	ob_start();
	if (is_numeric($operation) === true) {
	    ?>
	    <div class="ui-droppable tv-drop"><span></span>
	        <input type="text" name="question_content[]" value="<?php echo $operation; ?>" class="input_question_content">
	    </div>
	    <?php
	} else {
	    ?>
	    <div class="ui-droppable tv-drop"><span></span>
	        <select name="question_content[]" size="1" class="operations">
	    	<option <?php echo $operation == '?' ? 'selected="true"' : ''; ?> value="?">&nbsp;?&nbsp;</option>
	    	<option <?php echo $operation == '+' ? 'selected="true"' : ''; ?> value="+">&nbsp;+&nbsp;</option>
	    	<option <?php echo $operation == '-' ? 'selected="true"' : ''; ?> value="-">&nbsp;-&nbsp;</option>
	    	<option <?php echo $operation == 'x' ? 'selected="true"' : ''; ?> value="x">&nbsp;x&nbsp;</option>
	    	<option <?php echo $operation == '/' ? 'selected="true"' : ''; ?> value="/">&nbsp;/&nbsp;</option>
	    	<option <?php echo $operation == '(' ? 'selected="true"' : ''; ?> value="(">&nbsp;(&nbsp;</option>
	    	<option <?php echo $operation == ')' ? 'selected="true"' : ''; ?> value=")">&nbsp;)&nbsp;</option>
	    	<option <?php echo $operation == 'N' ? 'selected="true"' : ''; ?> value="N">&nbsp;0&nbsp;</option>
	        </select>
	    </div>
	    <?php
	}
	$contents = ob_get_contents();
	ob_end_clean();
	return $contents;
    }

    /**
     * dinh dang lai chuoi cau tra loi
     * @param type $string
     * @return string
     */
    function formatAnswer($string) {
	if (empty($string))
	    return '';
	$arr = array('?', '+', '-', 'x', '\/', '(', ')', '=');
	foreach ($arr as $value) {
	    $string = str_replace($value, ' ' . $value . ' ', $string);
	}
	return $string;
    }

}
?>
