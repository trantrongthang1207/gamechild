<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of questiongame
 *
 * @author AdminFGC
 */
//include_once (PATH_LIBARIES_TABLE . 'questionsgame.class.php');
class testsectionModel {

    //put your code here
    function __construct() {
        $app = App::getInstance();
        $request = $app->getRequest();
        $cid = $request->getVar('cid', array());
        $this->setId($cid[0]);
    }

    function setId($id) {
        $this->id = $id;
    }

    /**
     * @param  typequestion, $limit, $offset
     * @return array
     */
    function getAllQuestions($offset = 0, $limit = 0) {
        $app = App::getInstance();
        $db = $app->getDBO();
        $sql = "SELECT * FROM practicetestsection order by id desc";
        $db->setQuery($sql, $offset, $limit);
        if ($result = $db->loadObjectList()) {
            return $result;
        } else {
            return array();
        }
    }

    function getQuestion() {
        $app = App::getInstance();
        $db = $app->getDBO();
        $sql = "SELECT * FROM practicetestsection WHERE id = '{$this->id}'";
        $db->setQuery($sql, 0, 1);
        if ($result = $db->loadObject()) {
            return $result;
        } else {
            return false;
        }
    }

    function getAllReport($offset = 0, $limit = 0) {
        $app = App::getInstance();
        $db = $app->getDBO();
        $sql = "SELECT * FROM practicetestsection_report";
        $db->setQuery($sql, $offset, $limit);
        if ($result = $db->loadObjectList()) {
            return $result;
        } else {
            return array();
        }
    }

    function countAllReport() {
        $app = App::getInstance();
        $db = $app->getDBO();
        $sql = "SELECT count(id) as countobject FROM practicetestsection_report";
        $db->setQuery($sql);
        if ($result = $db->loadObject()) {
            return $result->countobject;
        } else {
            return 0;
        }
    }

    function deletereport($id) {
        $app = App::getInstance();
        /* @var $db Database */
        $db = $app->getDBO();
        $table = new Testsection_reportTable($db);
        return $table->delete($id);
    }

    function delete($id) {
        $app = App::getInstance();
        /* @var $db Database */
        $db = $app->getDBO();
        $table = new TestsectionTable($db);
        return $table->delete($id);
    }

    function insertUpdate($records) {
        $app = App::getInstance();
        /* @var $db Database */
        $db = $app->getDBO();
        $table = new TestsectionTable($db);
        $records['answers'] = json_encode($records['answers']);
        if (!empty($records)) {
            if (!$table->save($records))
                return false;
        }
        return true;
    }

    function getResult($number1, $cal, $number2) {
        switch ($cal) {
            case '+':
                $number = $number1 + $number2;
                break;
            case '-':
                $number = $number1 - $number2;
                break;
            case '*':
                $number = $number1 * $number2;
                break;
            case '/':
                $number = $number1 / $number2;
                break;
            default :
                $number = 0;
        }
        return number_format($number, 2, '.', '');
    }

    function getNamCalculation($cal) {
        //subtraction, division, multiplication, plus
        switch ($cal) {
            case '+':
                $name = 'plus';
                break;
            case '-':
                $name = 'Subtraction';
                break;
            case '*':
                $name = 'Multiplaication';
                break;
            case '/':
                $name = 'Division';
                break;
            default :
                $name = 'no';
                break;
        }
        return $name;
    }

    function published($id, $published) {
        $app = App::getInstance();
        /* @var $db Database */
        $db = $app->getDBO();
        $query = "UPDATE practicetestsection SET published='$published' WHERE id=" . $db->Quote($id);
        $db->setQuery($query);
        if ($db->query())
            return true;
        else
            return false;
    }

}

?>
