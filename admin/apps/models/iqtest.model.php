<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of questiongame
 *
 * @author AdminFGC
 */
//include_once (PATH_LIBARIES_TABLE . 'questionsgame.class.php');
class iqtestModel {

    //put your code here
    function __construct() {
        $app = App::getInstance();
        $request = $app->getRequest();
        $cid = $request->getVar('cid', array());
        $this->setId($cid[0]);
    }

    function setId($id) {
        $this->id = $id;
    }

    /**
     * @param  typequestion, $limit, $offset
     * @return array
     */
    function getAllQuestions($offset = 0, $limit = 0) {
        $app = App::getInstance();
        $db = $app->getDBO();
        $sql = "SELECT * FROM iqtest order by id desc";
        $db->setQuery($sql, $offset, $limit);
        if ($result = $db->loadObjectList()) {
            return $result;
        } else {
            return array();
        }
    }

    function getQuestion() {
        $app = App::getInstance();
        $db = $app->getDBO();
        $sql = "SELECT * FROM iqtest WHERE id = '{$this->id}'";
        $db->setQuery($sql, 0, 1);
        if ($result = $db->loadObject()) {
            return $result;
        } else {
            return false;
        }
    }

    function getAllReport($offset = 0,$limit = 0) {
        $app = App::getInstance();
        $db = $app->getDBO();
        $sql = "SELECT * FROM iqtest_report";
        $db->setQuery($sql,$offset,$limit);
        if ($result = $db->loadObjectList()) {
            return $result;
        } else {
            return array();
        }
    }

    function countAllReport() {
        $app = App::getInstance();
        $db = $app->getDBO();
        $sql = "SELECT count(id) as countobject FROM iqtest_report";
        $db->setQuery($sql);
        if ($result = $db->loadObject()) {
            return $result->countobject;
        } else {
            return 0;
        }
    }

    function deletereport($id) {
        $app = App::getInstance();
        /* @var $db Database */
        $db = $app->getDBO();
        $table = new iqtest_reportTable($db);
        return $table->delete($id);
    }

    function delete($id) {
        $app = App::getInstance();
        /* @var $db Database */
        $db = $app->getDBO();
        $table = new iqtestTable($db);
        return $table->delete($id);
    }

    function insertUpdate($records) {

        $allowedExts = array(".gif", ".jpeg", ".jpg", ".png");
        $extension = strtolower(substr($_FILES['images']['name'], strrpos($_FILES['images']['name'], '.')));
        if ((($_FILES["images"]["type"] == "image/gif") || ($_FILES["images"]["type"] == "image/jpeg") || ($_FILES["images"]["type"] == "image/jpg") || ($_FILES["images"]["type"] == "image/pjpeg") || ($_FILES["images"]["type"] == "image/x-png") || ($_FILES["images"]["type"] == "image/png")) && ($_FILES["images"]["size"] < 10 * 1024 * 1024) && in_array($extension, $allowedExts)) {
            if ($_FILES["images"]["error"] > 0) {
                echo "Return Code: " . $_FILES["images"]["error"] . "<br>";
                $image = '';
            } else {
                $rootSite = dirname(dirname(dirname(dirname(__FILE__))));
                if (file_exists($rootSite . "/IQtest/uploadfiles/" . $_FILES["images"]["name"])) {
                    echo $_FILES["images"]["name"] . " already exists. ";
                    $image = '';
                } else {
                    $random = time();
                    if (move_uploaded_file($_FILES["images"]["tmp_name"], $rootSite . "/IQtest/uploadfiles/" . $random . '-' . $_FILES["images"]["name"])) {
                        $image = $random . '-' . $_FILES["images"]["name"];
                    } else {
                        $image = '';
                    }
                }
            }
        } else {
            if ($_FILES["images"]["name"] != '') {
                //echo "Invalid file";
                $image = '';
            } else {
                $image = '';
            }
        }

        $app = App::getInstance();
        /* @var $db Database */
        $db = $app->getDBO();
        $table = new iqtestTable($db);
        $info = $this->getQuestion();
        if ($image == '') {
            $records['images'] = $info->images;
        } else {
            $records['images'] = $image;
        }
        if (!empty($records)) {
            if (!$table->save($records))
                return false;
        }
        return true;
    }

    function getResult($number1, $cal, $number2) {
        switch ($cal) {
            case '+':
                $number = $number1 + $number2;
                break;
            case '-':
                $number = $number1 - $number2;
                break;
            case '*':
                $number = $number1 * $number2;
                break;
            case '/':
                $number = $number1 / $number2;
                break;
            default :
                $number = 0;
        }
        return number_format($number, 2, '.', '');
    }

    function getNamCalculation($cal) {
        //subtraction, division, multiplication, plus
        switch ($cal) {
            case '+':
                $name = 'plus';
                break;
            case '-':
                $name = 'Subtraction';
                break;
            case '*':
                $name = 'Multiplaication';
                break;
            case '/':
                $name = 'Division';
                break;
            default :
                $name = 'no';
                break;
        }
        return $name;
    }

    function published($id, $published) {
        $app = App::getInstance();
        /* @var $db Database */
        $db = $app->getDBO();
        $query = "UPDATE iqtest SET published='$published' WHERE id=" . $db->Quote($id);
        $db->setQuery($query);
        if ($db->query())
            return true;
        else
            return false;
    }

}

?>
