<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
include_once (PATH_LIBARIES_TABLE . 'config.class.php');

class configModel {

    function __construct() {
        $app = App::getInstance();
        $request = $app->getRequest();
        $cid = $request->getVar('cid', array(0));
        $this->setId($cid[0]);
    }

    function setId($id) {
        $this->id = $id;
    }

    function getConfig() {
        $app = App::getInstance();
        /* @var $db Database */
        $db = $app->getDBO();
        $table = new ConfigTable($db);
        $result = $table->load_object($this->id);
        if (!empty($result)) {
            return $result;
        } else {
            $result = new stdClass();

            $result->id = 0;

            $result->name = '';

            $result->value = '';

            $result->checked_out = '';

            $result->checked_out_time = '';
        }
        return $result;
    }

    function getvalueConfig($name) {
        $app = App::getInstance();
        /* @var $db Database */
        $db = $app->getDBO();

        $query = "SELECT * FROM config WHERE name=" . $db->Quote($name) . " LIMIT 1";
        $db->setQuery($query);
        $result = $db->loadObject();
        if (!empty($result))
            return $result;
        else
            return null;
    }

    function checkconfigname($name) {
        $app = App::getInstance();
        /*
         * @var $db Database
         */
        $db = $app->getDBO();
        $sql = "SELECT * FROM config WHERE name=" . $db->Quote($name);
        $db->setQuery($sql);
        if ($db->loadObjectList()) {
            return true;
        } else {
            return false;
        }
    }

    function setConfig($name, $value) {
        $app = App::getInstance();
        /* @var $db Database */
        $db = $app->getDBO();
        if ($this->checkconfigname($name)) {
            $query = "UPDATE config SET value=" . $db->Quote($value) . " WHERE name=" . $db->Quote($name);
        } else {
            $query = "INSERT INTO config(`id` ,`name` ,`value`) value(NULL, " . $db->Quote($name) . ", " . $db->Quote($value) . ")";
        }
        $db->setQuery($query);
        if ($db->query()) {
            return true;
        } else {
            return false;
        }
    }

    function delete($id) {
        $app = App::getInstance();
        /* @var $db Database */
        $db = $app->getDBO();
        $table = new ConfigTable($db);
        return $table->delete($id);
    }

}

?>
