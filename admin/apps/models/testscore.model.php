<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of questiongame
 *
 * @author AdminFGC
 */

include_once (PATH_LIBARIES_TABLE . 'testscore_questions.class.php');
include_once (PATH_LIBARIES_TABLE . 'testscore_answers.class.php');
include_once (PATH_LIBARIES_TABLE . 'testscore_report.class.php');
include_once (PATH_APPS . 'Enums' . DS . 'testscore.php');

class testscoreModel {

    //put your code here
    function __construct() {
        $app = App::getInstance();
        $request = $app->getRequest();
        $cid = $request->getVar('cid', array());
        $this->setId($cid[0]);
    }

    function setId($id) {
        $this->id = $id;
    }

    /**
     * @param  typequestion, $limit, $offset
     * @return array
     */
    function getAllQuestions($offset = 0, $limit = 0) {
        $app = App::getInstance();
        $db = $app->getDBO();
        $sql = "SELECT * FROM " . StringTable::TABLE_TESTSCORE_QUESTIONS . " WHERE 1 order by id desc";
        $db->setQuery($sql, $offset, $limit);
        if ($result = $db->loadObjectList()) {
            return $result;
        } else {
            return array();
        }
    }

    function getQuestion($id = 0) {
        $app = App::getInstance();
        if (!$id) {
            $id = $this->id;
        }
        $db = $app->getDBO();
        $sql = "SELECT * FROM " . StringTable::TABLE_TESTSCORE_QUESTIONS . " WHERE id = '$id'";
        $db->setQuery($sql, 0, 1);
        if ($result = $db->loadObject()) {
            return $result;
        } else {
            return false;
        }
    }

    function showLinkAnsers($id) {

        $app = App::getInstance();
        $db = $app->getDBO();
        //$item = $request->getVar('item', '');
        $sql = "SELECT * FROM " . StringTable::TABLE_TESTSCORE_ANSWERS . " WHERE `question_id` = {$db->Quote($id)} ";
        $db->setQuery($sql, 0, 1);
        if ($result = $db->loadObject()) {
            return "<a href = 'index.php?apps=testscore&task=viewanswers&id={$result->question_id}'>Show answers</a>";
        } else {
            return "<a href = 'index.php?apps=testscore&task=ansers&id={$id}'>Add answers</a>";
        }
    }

    function getListAnswers($question_id) {
        $app = App::getInstance();
        $db = $app->getDBO();
        $sql = "SELECT * FROM " . StringTable::TABLE_TESTSCORE_ANSWERS . " WHERE question_id = {$db->Quote($question_id)} "
                . "order by id asc";
        $db->setQuery($sql);
        if ($result = $db->loadObjectList()) {
            return $result;
        } else {
            return array();
        }
    }

    function delete($id) {
        $app = App::getInstance();
        /* @var $db Database */
        $db = $app->getDBO();
        //$tablea = new testroundsTable($db);
        $pathSite = PATH_SITE . 'testScore' . DS . 'uploadfiles' . DS;
        if ($question = $this->getQuestion($id)) {
            $table = new Testscore_questionsTable($db);
            if ($table->delete($id)) {
                //delete image and delete answers
                if (file_exists($pathSite . $question->image1)) {
                    @unlink($pathSite . $question->image1);
                }
                if (file_exists($pathSite . $question->image2)) {
                    @unlink($pathSite . $question->image2);
                }
                $query = "DELETE FROM " . StringTable::TABLE_TESTSCORE_ANSWERS . " WHERE `question_id` = '$id'";
                $db->setQuery($query);
                $db->query();
            }
            return true;
        } else {
            return false;
        }
        //return $tableq->delete($id) && $tablea->delete($id);
    }

    function deleteanswer($id) {
        $app = App::getInstance();
        /* @var $db Database */
        $db = $app->getDBO();
        $table = new Testscore_answersTable($db);
        return $table->delete($id);
    }

    function deletereport($id) {
        $app = App::getInstance();
        /* @var $db Database */
        $db = $app->getDBO();
        $table = new Testscore_reportTable($db);
        return $table->delete($id);
    }

    function published($id, $published) {
        $app = App::getInstance();
        /* @var $db Database */
        $db = $app->getDBO();
        $query = "UPDATE " . StringTable::TABLE_TESTSCORE_QUESTIONS . " SET published='$published' WHERE id=" . $db->Quote($id);
        $db->setQuery($query);
        if ($db->query())
            return true;
        else
            return false;
    }

    function getAnswerById($id) {
        $app = App::getInstance();
        $db = $app->getDBO();
        $query = "SELECT * FROM " . StringTable::TABLE_TESTSCORE_ANSWERS . " WHERE `id` = '$id'";
        $db->setQuery($query, 0, 1);
        if ($object = $db->loadObject()) {
            if ($object->postions != "") {
                $object->postions = explode(';', $object->postions);
            } else {
                $object->postions = '';
            }
            return $object;
        } else {
            return false;
        }
    }

    function getAllReport($offset = 0, $limit = 0) {
        $app = App::getInstance();
        $db = $app->getDBO();
        $sql = "SELECT * FROM " . StringTable::TABLE_TESTCSCORE_REPORT . " ORDER BY correct_percent desc";
        $db->setQuery($sql, $offset, $limit);
        // echo $db->getQuery();
        if ($result = $db->loadObjectList()) {
            return $result;
        } else {
            return array();
        }
    }

    function countAllReport() {
        $app = App::getInstance();
        $db = $app->getDBO();
        $sql = "SELECT count(id) as countobject FROM " . StringTable::TABLE_TESTCSCORE_REPORT;
        $db->setQuery($sql);
        if ($result = $db->loadObject()) {
            return $result->countobject;
        } else {
            return 0;
        }
    }

}

?>
