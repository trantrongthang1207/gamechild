<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
include_once (PATH_APPS . 'models' . DS . 'user.model.php');

class loginModel {

    function __construct() {

    }

    function login() {
        $app = App::getInstance();
        $session = $app->getSession();
        $records = $_POST;
        $model = new userModel();
        $user = $model->getuserbyusername($records['username']);

        if (!empty($user)) {
            if ($user->password == md5($records['password']) && $user->user_type == 'admin'&& $user->block==0) {
                $session->set('username', $user->username, 'adminlogin');
                $session->set('usertype', $user->user_type, 'adminlogin');
                $currentTime = date('Y-m-d h:i:s');
                $db = $app->getDBO();
                $query = "UPDATE users SET last_visit_date=" . $db->Quote($currentTime) . " WHERE username={$db->Quote($user->username)} AND password={$db->Quote($user->password)}";
                $db->setQuery($query);
                $db->query();
                $app->redirect('index.php');
            } else {
                $msg = "Username and password do not match! or this user blocked";
                $session->clear('username', 'adminlogin');
                $session->clear('usertype', 'adminlogin');
                $app->redirect("index.php?apps=login&msg={$msg}");
            }
        } else {
            $msg = "username not exist";
            $session->clear('username', 'adminlogin');
            $session->clear('usertype', 'adminlogin');

            $app->redirect("index.php?apps=login&msg={$msg}");
        }
    }

    function logout() {
        $app = App::getInstance();
        $session = $app->getSession();
        $session->clear('username', 'adminlogin');
        $session->clear('usertype', 'adminlogin');
        $app->redirect("index.php?apps=login");
    }

}

?>
