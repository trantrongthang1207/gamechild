<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of questionsgame
 *
 * @author AdminFGC
 */
include_once (PATH_APPS . 'models' . DS . 'testsection.model.php');

class testsectionController {
    //put your code here
    function __construct() {

    }
    function delete() {
        $app = App::getInstance();
        $request = $app->getRequest();
        $model = new testsectionModel();
        $cid = $request->getVar('cid', array(0));
        $count = 0;
        for ($i = 0; $i < count($cid); $i++) {
            if ($model->delete($cid[$i])) {
                $count++;
            }
            $msg = "Deleted ($count) successfull!";
        }
        $url = "index.php?apps=testsection&msg={$msg}";
        $app->redirect($url);
    }
    
    function deletereport() {
        $app = App::getInstance();
        $request = $app->getRequest();
        $model = new testsectionModel();
        $cid = $request->getVar('cid', array(0));
        $count = 0;
        for ($i = 0; $i < count($cid); $i++) {
            if ($model->deletereport($cid[$i])) {
                $count++;
            }
            $msg = "Deleted ($count) successfull!";
        }
        $url = "index.php?apps=testsection&task=report&msg={$msg}";
        $app->redirect($url);
    }

    function published() {
        $app = App::getInstance();
        $request = $app->getRequest();
        $cid = $request->getVar('cid', 0);
        $publish = $request->getVar('valuepublish', 0);
        if ($publish == 0) {
            $published = 1;
        } else {
            $published = 0;
        }
        $model = new testsectionModel();
        if ($model->published($cid, $published)) {
            if ($published == 1) {
                echo "1";
            } else {
                echo "0";
            }
        } else {
            if ($published == 0) {
                echo "0";
            } else {
                echo "1";
            }
        }
    }

    function save_config() {
        $app = App::getInstance();
        include_once (PATH_APPS . 'models' . DS . 'config.model.php');
        $records = $_POST;
        $model = new configModel();
        foreach ($records as $key => $value) {
            if ($value == '')
                $value = 0;
            if ($model->setConfig($key, intval($value))) {
                $msg = "Update successfull";
            } else {

                $msg = "No Update";
            }
        }
        $link = "index.php?apps=testsection&task=view&layout=configtime&msg={$msg}";
        $app->redirect($link);
    }

    function save() {
        $app = App::getInstance();
        $request = $app->getRequest();
        $model = new testsectionModel();
        $records = $_POST;
        if ($records['id'] != '' || intval($records['id']) > 0) {
            if ($records['content_question'] != '' && $records['right_answer'] != '' ) {
                if ($model->insertUpdate($records)) {
                    $msg = "Update successfull";
                } else {
                    $msg = "No update";
                }
            } else {
                $msg = 'Input data is not null';
            }
        } else {
            if ($records['content_question'] != '' && $records['right_answer'] != '' ) {
                if ($model->insertUpdate($records)) {
                    $msg = "Insert successfull";
                } else {
                    $msg = "No insert";
                }
            } else {
                $msg = 'Input data is not null';
            }
        }
        $link = "index.php?apps=testsection&msg={$msg}";
        $app->redirect($link);
    }

}

?>
