<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
include_once (PATH_APPS . 'models' . DS . 'config.model.php');

class configController {

    function __construct() {

    }

    function save() {
        $records = $_POST;
        $records['paypal_testmode'] = (isset($records['paypal_testmode']) && $records['paypal_testmode'] == 'on') ? 1 : 0;
        $records['end_time'] = $records['end_time'] . ":" . $records['time_unit'];
        if (!empty($records)) {
            $count = 0;
            foreach ($records as $key => $record) {
                $name = trim($key);
                $value = trim($record);
                if ($name != '' && $value != '') {
                    $model = new configModel();
                    if ($name != 'time_unit') {
                        if ($model->setConfig($name, $value)) {
                            $msg = "Update successfull";
                        }
                    }
                } else {
                    $msg = "No update";
                }
            }
        } else {
            $msg = "No update";
        }
        $app = App::getInstance();
        $request = $app->getRequest();
        $url = "index.php?apps=config&msg={$msg}";
        $app->redirect($url);
    }

    function save_config() {
        $app = App::getInstance();
        include_once (PATH_APPS . 'models' . DS . 'config.model.php');
        $records = $_POST;
        $model = new configModel();
        foreach ($records as $key => $value) {
            if ($value == '')
                $value = 0;
            if ($model->setConfig($key, intval($value))) {
                $msg = "Update successfull";
            } else {

                $msg = "No Update";
            }
        }
        $link = "index.php?apps=slalomtest&msg={$msg}";
        $app->redirect($link);
    }
    function save_config_slalom() {
        $app = App::getInstance();
        include_once (PATH_APPS . 'models' . DS . 'config.model.php');
        $model = new configModel();
            $string = $_POST['time_round_slalom1'].",".$_POST['time_round_slalom2'].",".$_POST['time_round_slalom3'];
        if ($string == '')
                $string = "0,0,0";
            if ($model->setConfig("time_round_slalom", $string)) {
                $msg = "Update successfull";
            } else {

                $msg = "No Update";
            }
        $link = "index.php?apps=slalomtest&msg={$msg}";
        $app->redirect($link);
    }

}

?>
