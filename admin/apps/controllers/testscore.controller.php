<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of questionsgame
 *
 * @author AdminFGC
 */
if (!class_exists('helper')) {
    include_once (PATH_APPS . "helpers" . DS . 'helper.php');
}
include_once (PATH_APPS . 'models' . DS . 'testscore.model.php');

class testscoreController {

    //put your code here
    function __construct() {
        
    }

    //delete content question
    function delete() {
        $app = App::getInstance();
        $request = $app->getRequest();
        $model = new testscoreModel();
        $cid = $request->getVar('cid', array(0));
        $count = 0;
        for ($i = 0; $i < count($cid); $i++) {
            if ($model->delete($cid[$i])) {
                $count++;
            }
        }
        $msg = "Deleted ($count) successfull!";

        $link = "index.php?apps=testscore&msg={$msg}";

        $app->redirect($link);
    }

    //delete content answer
    function deleteanswer() {
        $app = App::getInstance();
        $request = $app->getRequest();
        $model = new testscoreModel();
        $cid = $request->getVar('cid', array(0));
        $id = $request->getVar('id', '');
        $count = 0;
        for ($i = 0; $i < count($cid); $i++) {
            if ($model->deleteanswer($cid[$i])) {
                $count++;
            }
        }
        $msg = "Deleted ($count) successfull!";
        $link = "index.php?apps=testscore&task=viewanswers&id={$id}&msg={$msg}";

        $app->redirect($link);
    }

    function published() {
        $app = App::getInstance();
        $request = $app->getRequest();
        $cid = $request->getVar('cid', 0);
        $publish = $request->getVar('valuepublish', 0);
        if ($publish == 0) {
            $published = 1;
        } else {
            $published = 0;
        }
        $model = new testscoreModel();
        if ($model->published($cid, $published)) {
            if ($published == 1) {
                echo "1";
            } else {
                echo "0";
            }
        } else {
            if ($published == 0) {
                echo "0";
            } else {
                echo "1";
            }
        }
    }

    //save contetnt question trong truong hop update and add new
    function saveq() {
        $app = App::getInstance();
        $helper = new helper();
        $pathFile = dirname(dirname(dirname(dirname(__FILE__)))) . DS . 'testScore/uploadfiles';
        $records = $_POST;
        $db = $app->getDBO();

        if (isset($records['question_id']) && intval($records['question_id']) > 0) {
            //Thu hien update question
            $records['id'] = $records['question_id'];
            $image1 = $helper->uploadImage($pathFile, 'image1');
            $image2 = $helper->uploadImage($pathFile, 'image2');
            if (@$image1['image'] != "") {
                $records['image1'] = $image1['image'];
            }
            if (@$image2['image'] != "") {
                $records['image2'] = $image2['image'];
            }
            $table = new Testscore_questionsTable($db);
            if ($table->save($records)) {
                $msg = "Update question successfull";
            } else {
                $msg = "Update question unsuccessfull";
            }
        } else {
            //echo $pathFile;exit();
            $image1 = $helper->uploadImage($pathFile, 'image1');
            $image2 = $helper->uploadImage($pathFile, 'image2');
            if ($image1['image'] != "" && $image2['image'] != "") {
                $records['image1'] = $image1['image'];
                $records['image2'] = $image2['image'];

                $table = new Testscore_questionsTable($db);
                if (!empty($records)) {
                    if (!$table->save($records)) {
                        $msg = "Error insert question";
                    } else {
                        $msg = "Insert successfull";
                    }
                } else {
                    $msg = "Waring! Plase input all data before submit form";
                }
            } else {
                $msg = "Waring! You must upload enough 2 photos";//$image1['error'] . ". " . $image2['error'];
            }
        }
        $link = "index.php?apps=testscore&msg={$msg}";

        $app->redirect($link);
    }

    //update content answer
    function saveanswer() {
        $records = $_POST;
        $postions = array();
        $app = App::getInstance();
        $db = $app->getDBO();
        global $arrayPostions;

        foreach ($arrayPostions as $postion) {
            if (isset($records['postion' . $postion['position']]) && $records['postion' . $postion['position']] > 0) {
                $postions[] = $records['postion' . $postion['position']];
            }
        }
        if (!empty($postions)) {
            $records['postions'] = implode(";", $postions);
            if (in_array($records['right_postion'], $postions)) {
                if ($records['question_id'] > 0 && isset($records['right_postion']) && $records['right_postion'] > 0 && @$records['namepostion'] != "") {
                    $update = false;
                    if (isset($records['id']) && intval($records['id']) > 0) {
                        $update = true;
                    } else {
                        unset($records['id']);
                    }
                    $table = new Testscore_answersTable($db);
                    if (!$table->save($records)) {
                        if (!$update) {
                            $msg = "Error insert answer";
                        } else {
                            $msg = "Error update answer";
                        }
                    } else {
                        if (!$update) {
                            $msg = "Insert answer successfull";
                        } else {
                            $msg = "Update answer successfull";
                        }
                    }
                } else {
                    $msg = "Plase input all data";
                }
            }else{
                $msg = "Waring! Right position is selected must be positions which are selected in 'Choose position'";
            }
        } else {
            $msg = "Waring! You must select Positions and Right position";
        }
        echo $msg;

        $link = "index.php?apps=testscore&task=viewanswers&id={$records['question_id']}&msg={$msg}";

        $app->redirect($link);
    }

    function save_config() {
        $app = App::getInstance();
        include_once (PATH_APPS . 'models' . DS . 'config.model.php');
        $records = $_POST;
        $model = new configModel();
        foreach ($records as $key => $value) {
            if ($value == '')
                $value = 0;
            if ($model->setConfig($key, intval($value))) {
                $msg = "Update successfull";
            } else {

                $msg = "Update unsuccessfull";
            }
        }
        $link = "index.php?apps=testscore&task=configtime&msg={$msg}";
        $app->redirect($link);
    }

    function deletereport() {
        $app = App::getInstance();
        $request = $app->getRequest();
        $model = new testscoreModel();
        $cid = $request->getVar('cid', array(0));
        $count = 0;
        for ($i = 0; $i < count($cid); $i++) {
            if ($model->deletereport($cid[$i])) {
                $count++;
            }
        }
        $msg = "Deleted ($count) successfull!";

        $link = "index.php?apps=testscore&task=reports&msg={$msg}";

        $app->redirect($link);
    }

}

?>