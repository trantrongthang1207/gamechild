<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
include_once (PATH_APPS . 'models' . DS . 'login.model.php');
$app = App::getInstance();
$request = $app->getRequest();
$task = $request->getVar('task', '');
switch ($task) {
    case 'login':
        $model = new loginModel();
        $model->login();
        break;
    case 'logout':
        $model = new loginModel();
        $model->logout();
        break;
    default :
        $msg = $request->getVar('msg', '');
        include_once (PATH_APPS . 'views' . DS . 'logins' . DS . 'edit_login_default.php');
        break;
}
?>
