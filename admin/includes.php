<?php

if (!defined('FGC'))
    define('FGC', 1);
include_once PATH_ADMIN . 'defines.php';
include_once PATH_SITE . 'configs.php';
include_once( PATH_LIBRARY_CORE . 'functions.php');
include_once( PATH_LIBRARY_CORE . 'object.class.php');

include_once (PATH_ADMIN . 'template.php');
include_once (PATH_ROOT . 'languages' . DS . 'en_languages.php');
include_once (PATH_APPS . 'helpers' . DS . 'helper.php');