<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class TemplateLoader {

    var $path = null;
    var $template = 'default';
    var $pathadmin = null;

    function __construct($path, $pathadmin, $template='default') {
        $this->path = $path;
        $this->pathadmin = $pathadmin;
        $this->template = $template;
    }

    function display() {

    }

    function loadApp($appName='') {
        if ($appName == '') {
            $app = App::getInstance();
            $request = $app->getRequest();
            $application = $request->getVar('apps', '');
        } else {
            $application = $appName;
        }
        $file = $this->getPathAdmin() . DS . 'apps' . DS . $application . '.php';
        if (file_exists($file))
            include_once ($file);
        else {
            $file = $this->getPathAdmin() . DS . 'apps' . DS . 'user' . '.php';
            include_once ($file);
        }
    }

    function loadHeader() {
        include_once ($this->getPathTemplate() . 'header.tpl.php');
    }

    function loadFooter() {
        include_once ($this->getPathTemplate() . 'footer.tpl.php');
    }

    function getPathTemplate() {
        return $this->path . DS . $this->template . DS;
    }

    function getPathAdmin() {
        return $this->pathadmin;
    }

    function loadTemplate() {
        $application = App::getInstance();
        if ($application->isAjax()) {
            $this->loadApp();
        } else {
            include_once ($this->getPathTemplate() . 'index.tpl.php');
        }
    }

}

?>
