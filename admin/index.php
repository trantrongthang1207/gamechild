<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
error_reporting(0);
if (!defined('FGC'))  define('FGC', 1);

if (!defined('DS')) define('DS', DIRECTORY_SEPARATOR);

define('SITE_TITLE', '');

define('SITE_NAME', '');

define('PATH_ROOT', dirname(dirname(__FILE__)) . DS);
if(!defined('PATH_ADMIN')) define ('PATH_ADMIN', dirname (__FILE__).DS);

include_once PATH_ADMIN.'includes.php';

/* @var $application Application */
class App {

    static $instance;

    function __construct($configs) {

        self::$instance = new Application('admin', $configs);
    }

    /**
     *
     * @global <type> $application
     * @return Application
     */
    function &getInstance() {

        return self::$instance;
    }

    function halt($message=null) {
        if (isset(self::$instance))
            self::$instance->halt($message);
        else
            exit($message);
    }

    function  __call($method_name, $arguments) {
        if (!method_exists($this, $method_name)) {
            return self::$instance->$method_name();
        }
    }

}
$app = new App($configs);
$application = $app->getInstance();
$application->initialize();
$db = $application->getDBO();
$array_type_questions = helper::getAllTypeQuestion($db);
$templateloader = new TemplateLoader(PATH_ADMIN_TEMPLATES, PATH_ADMIN,'latestpilotjobs');
$templateloader->loadTemplate();
$application->close();
?>
