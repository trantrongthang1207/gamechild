<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
include_once (PATH_APPS . 'models' . DS . 'user.model.php');

class LoginConditions {

    function __construct() {

    }

    function check() {
        $app = App::getInstance();
        $session = $app->getSession();
        $username = $session->get('username', 0, 'adminlogin');
        $userType = $session->get('usertype', '', 'adminlogin');
        if ($username != '') {
            $model = new userModel();
            $user = $model->getuserbyusername($username);
            if (!empty($user) && $userType == $user->user_type && $userType == 'admin') {
                return true;
            } else {
                $session->clear('username', 'adminlogin');
                $session->clear('usertype', 'adminlogin');
                return false;
            }
        } else {
            $session->clear('username', 'adminlogin');
            $session->clear('usertype', 'adminlogin');
            return false;
        }
    }

}

?>
